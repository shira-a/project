package com.igam.shira.myapplication;

public class Upload {
    private String mName;
    private String mImageUrl;

    public Upload()
    {

    }
    public Upload(String ImageUrl)
    {

        this.mImageUrl=ImageUrl;
    }
    public Upload(String ImageUrl, String mName)
    {

        this.mImageUrl=ImageUrl;
        this.mName="rate:"+""+mName;
    }

    public String getmName() {
        return mName;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }
}
