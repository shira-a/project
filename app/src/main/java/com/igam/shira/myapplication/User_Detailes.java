package com.igam.shira.myapplication;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import java.util.List;
import java.util.Vector;

public class User_Detailes extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
private  String gUid=null;
    private String user_guidf=null;
private String newUser=null;
    private List<Fragment> mFragments = new Vector<Fragment>();
    private String weekNum = "EE";

private boolean new_user=true;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    public void setmViewPager(ViewPager mViewPager) {
        this.mViewPager = mViewPager;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //getSupportFragmentManager().addOnBackStackChangedListener();
        mFragments.add(new Frag1());
        mFragments.add(new Frag2());
        mFragments.add(new NewUser());
        mFragments.add(new Frag3());
        mFragments.add(new Frag7());

       gUid= getIntent().getStringExtra("Uid");
        user_guidf=getIntent().getStringExtra("user_Uid");
       newUser=getIntent().getStringExtra("new_user");
       if(newUser==null)
           newUser ="false";
       //else
          // new_user=true;

       // Log.d("new user?",newUser);

        if(user_guidf!=null)

         Log.d("detailes",user_guidf);
        setTitle("פרטי משתמש");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));



    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            rootView.setClickable(true);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);

            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }



    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {

            super(fm);

        }
        @Override
        public Fragment getItem(int position) {
            Fragment fragment=new Frag3();

            switch (position)
            {
                case 0:

                    if(newUser.equals("true")&&new_user) {
                        Log.d("new","new");
                        fragment = new NewUser();


                    }
                    else  {
                        fragment = new Frag1();
                        Log.d("old","old");
                        Bundle bundle = new Bundle();
                        bundle.putString("Uid", gUid);
                        bundle.putString("user_gUid", user_guidf);
                        // bundle.putString("new_user","true" );
                        fragment.setArguments(bundle);
                    }
                    break;
                case 1:fragment=new Frag2();
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("Uid", gUid);
                    bundle1.putString("user_gUid", user_guidf);
                    fragment.setArguments(bundle1);
                break;
                case 2:fragment=new Frag3();
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("Uid", gUid);
                    bundle2.putString("user_gUid", user_guidf);
                    fragment.setArguments(bundle2);
                break;
                case 3:fragment=new Frag7();
                Bundle bundle3=new Bundle();
                bundle3.putString("Uid",gUid);
                bundle3.putString("user_gUid", user_guidf);
                fragment.setArguments(bundle3);
                break;

            }

            return fragment;
        }










        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }



    }

    @Override
    public void onBackPressed() {
           super.onBackPressed();


    }






}
