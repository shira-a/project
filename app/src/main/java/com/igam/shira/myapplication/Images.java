package com.igam.shira.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

public class Images extends AppCompatActivity implements ImageAdapter.OnItemClickListener {
    private RecyclerView recyclerView;
    private FirebaseStorage firebaseStorage;
    private StorageReference mStorageRef;
    private FirebaseFirestore db;
    private ImageAdapter imageAdapter;
    private List<Upload> mUplaods;
    private String gUid,weeknum,pathweek;
    private ProgressDialog mProgress;
    private String user_guidf;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);
        firebaseStorage = FirebaseStorage.getInstance();
        mStorageRef = firebaseStorage.getReference();
        recyclerView=(RecyclerView)findViewById(R.id.recycle_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mProgress = new ProgressDialog(this);
        mUplaods=new ArrayList<>();
        user_guidf=getIntent().getStringExtra("user_gUid");
        gUid= getIntent().getStringExtra("Uid");
        weeknum=getIntent().getStringExtra("weekNum");
        pathweek="mouth"+weeknum;
        db=FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("users-data").document(user_guidf);


        Log.d("gUid of user in images",user_guidf);

        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {

                Map<String, Object> forms = (Map<String, Object>) documentSnapshot.get(weeknum);
                for (Map.Entry<String, Object> s: forms.entrySet()) {
                    // String key = (String) form.getKey();
                    String value = (String) s.getValue();
                    Upload upload=new Upload(value);
                    Log.d("string",value);

                    mUplaods.add(upload);
                }

                imageAdapter=new ImageAdapter(Images.this,mUplaods);
                recyclerView.setAdapter(imageAdapter);
                imageAdapter.setOnItemClickListener(Images.this);
              //  mProgress.dismiss();
            }
        });


        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if(documentSnapshot.exists())
                {


                }

            }
        });


    }

    @Override
    public void onItemClicked(int position) {
        Toast.makeText(Images.this, "position"+position, Toast.LENGTH_LONG).show();
        String s=mUplaods.get(position).getmImageUrl();
        Intent intent = new Intent(Images.this, Scribbler.class);
        // Create a URI from url
        Uri uri=Uri.parse(s);
        intent.putExtra("gUid",gUid);
        intent.putExtra("user_gUid", user_guidf);
        intent.putExtra("Url",s);
        intent.putExtra("weeknum",weeknum);
Log.d("url print",s);
        startActivity(intent);
        Log.d("pictureee",s);

    }


}
