package com.igam.shira.myapplication;

import android.app.ActionBar;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;

public class Tutorials extends AppCompatActivity {
    private String gUid;
    private String role;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorials);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        ((TextView)getSupportActionBar().getCustomView().findViewById(R.id.tvTitle)).setText("הדרכות");
        gUid = getIntent().getStringExtra("Uid");

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        DocumentReference docRef = db.collection("users-data").document(gUid);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document .exists()) {
                        //     Log.i("LOGGER","First "+document.getString("first_entry"));
                        Map<String, Object> map = document.getData();
                        role = (String) document.get("role");

                        //if the field is Boolean
                        if(document.get("role")!=null&&role.equals("user"))
                        {
                            getMenuInflater().inflate(R.menu.menu, menu);

                        }
                        else if(document.get("role")!=null&&role.equals("admin"))
                        {
                            getMenuInflater().inflate(R.menu.admin, menu);

                        }

                        // Log.i("LOGGER","second "+document.getString("role"));

                    } else {
                        Log.d("LOGGER", "No such document");
                    }
                } else {
                    Log.d("LOGGER", "get failed with ", task.getException());
                }
            }
        });







        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.item1:
                Intent intent = new Intent(Tutorials.this, Treatment_progress.class);
                intent.putExtra("Uid", gUid);
                startActivity(intent);
                break;
            case R.id.item2:
                Intent intent1 = new Intent(Tutorials.this, Tutorials.class);
                intent1.putExtra("Uid", gUid);
                startActivity(intent1);
                break;
            case R.id.item4:
                Intent intent2 = new Intent(Tutorials.this, Users.class);
                intent2.putExtra("Uid", gUid);
                startActivity(intent2);
                break;
            case R.id.item5:
                Intent intent3 = new Intent(Tutorials.this, Checkpic.class);
                intent3.putExtra("Uid", gUid);
                startActivity(intent3);
                break;
            case R.id.item3:
                FirebaseAuth.getInstance().signOut();
                Intent intent4 = new Intent(Tutorials.this, login.class);
                //intent4.putExtra("Uid", gUid);
                startActivity(intent4);
                break;


            default:
                return super.onOptionsItemSelected(item);
        }





        return true;
    }


    public void gumsProblems(View view)
    {
        Intent intent = new Intent(Tutorials.this, InstructionActivity2.class);
        intent.putExtra("Uid", gUid);
        startActivity(intent);
    }

    public void brushTeeth(View view)
    {
        Intent intent = new Intent(Tutorials.this, InstructionsActivity.class);
        intent.putExtra("Uid", gUid);
        startActivity(intent);
    }

    public void howToPic(View view)
    {
        Intent intent = new Intent(Tutorials.this, InstructionsActivity3.class);
        intent.putExtra("Uid", gUid);
        startActivity(intent);

    }
    public void gumsProblems2(View view)
    {
        Intent intent = new Intent(Tutorials.this, gumsProblem2.class);
        intent.putExtra("Uid", gUid);
        startActivity(intent);
    }

    public void gumsProblems3(View view)
    {
        Intent intent = new Intent(Tutorials.this, Gumsproblem3.class);
        intent.putExtra("Uid", gUid);
        startActivity(intent);
    }

    public void gumsProblems4(View view)
    {
        Intent intent = new Intent(Tutorials.this, GumsProblem4.class);
        intent.putExtra("Uid", gUid);
        startActivity(intent);
    }


}
