package com.igam.shira.myapplication;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;

public class Frag3 extends Fragment {
    private  LinearLayout view;
    private  LinearLayout theLayout;
    private FirebaseFirestore db =FirebaseFirestore.getInstance();
    private String gender;
    private String birthDate;
    private String country;
    private String race;
    private String spiritualStatus,tbreastfeed;
    private String familyStatus,pregnantWeek;
    private boolean isPregnant=true,isbreastfeed;
    private String work,isPregnanttext;
    private String gUid,user_guidf;
    private User user;
    boolean flag=false,firstEntry=false;
    private Spinner genderSpinner,countrySpinner,DateOfBirthSpinner,countryBirthSpinner,LiamSpinner,spiritualStatusSpinner,famelyStatusSpinner,bissnesSpinner;
    private CheckBox prignent,breastfeed;
    private EditText weekOfPregnent;
    int sp_position;

    TextView txt1,txt2,txt3;
    Button  btn;

    @Nullable
    @Override


    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
      //  ActionBar actionBar = getActivity().getActionBar();
      //  actionBar.setTitle("פרטים");
        gUid= getArguments().getString("Uid");
        user_guidf=getArguments().getString("user_gUid");
        if(user_guidf!=null)
        Log.d("gUid",user_guidf);
        else
            Log.d("gUid","new user");


        view = (LinearLayout) inflater.inflate(R.layout.activity_details, container, false);



        FragmentManager fm = getFragmentManager();
        int count = fm.getBackStackEntryCount();
        for(int i = 0; i < count; ++i) {
            fm.popBackStackImmediate();
        }


          btn = (Button) view.findViewById(R.id.next);
        genderSpinner = (Spinner) view.findViewById(R.id.gender);
        countrySpinner=(Spinner)view. findViewById(R.id.country);
        LiamSpinner=(Spinner)view. findViewById(R.id.race);
        spiritualStatusSpinner=(Spinner)view. findViewById(R.id.spiritualStatus);
        famelyStatusSpinner=(Spinner)view.findViewById(R.id.familyStatus);
        bissnesSpinner=(Spinner)view.findViewById(R.id.work);
        prignent=(CheckBox) view.findViewById(R.id.checkboxPregnant);
        weekOfPregnent=(EditText)view.findViewById(R.id.weeksofPregnancy) ;
        breastfeed=(CheckBox) view.findViewById(R.id.checkboxBreastfeeding);
        txt1=(TextView)view.findViewById(R.id.textView5) ;
        txt2=(TextView)view.findViewById(R.id.textView7);
        txt1.setText(" ");
        txt2.setText(" ");




       if(user_guidf!=null) {
    Log.d("user_guidf","user_guidf!=null");
           DocumentReference docRef = db.collection("users-data").document(user_guidf);


           docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
               @Override
               public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                   if (task.isSuccessful()) {
                       DocumentSnapshot document = task.getResult();
                       if (document.exists()) {
                             //  Log.i("LOGGER","First "+document.getString("first_entry"));

                        Map<String, Object> map = (Map) document.get("GeneralForm");
                           Map<String, Object> map1 = (Map) map.get("GeneralForm");
                             for(Map.Entry<String, Object> entry : map1.entrySet())
                             {
                                 if (entry.getKey().equals("gender")) {
                                     gender =entry.getValue().toString();
                                    // genderSpinner.setAdapter(gender);
                                     Log.d("gender",gender);
                                     //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                     for(int i = 0; i <genderSpinner.getAdapter().getCount(); i++)

                                     {
                                         if (genderSpinner.getItemAtPosition(i).equals(gender) )
                                         {
                                             genderSpinner.setSelection(i);
                                             genderSpinner.setEnabled(false);
                                             break;
                                         }
                                     }
                                 }






                                 if (entry.getKey().equals("country")) {
                                     country =entry.getValue().toString();
                                     // genderSpinner.setAdapter(gender);
                                     Log.d("country",country);
                                     //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                     for(int i = 0; i <countrySpinner.getAdapter().getCount(); i++)

                                     {
                                         if (countrySpinner.getItemAtPosition(i).equals(country) )
                                         {
                                             countrySpinner.setSelection(i);
                                             countrySpinner.setEnabled(false);
                                             break;
                                         }
                                     }
                                 }



                                 if (entry.getKey().equals("race")) {
                                     race =entry.getValue().toString();
                                     // genderSpinner.setAdapter(gender);
                                     Log.d("race",race);
                                     //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                     for(int i = 0; i <LiamSpinner.getAdapter().getCount(); i++)

                                     {
                                         if (LiamSpinner.getItemAtPosition(i).equals(race) )
                                         {
                                             LiamSpinner.setSelection(i);
                                             LiamSpinner.setEnabled(false);
                                             break;
                                         }
                                     }
                                 }



                                 if (entry.getKey().equals("work")) {
                                     work =entry.getValue().toString();
                                     // genderSpinner.setAdapter(gender);
                                     Log.d("work",work);
                                     //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                     for(int i = 0; i <bissnesSpinner.getAdapter().getCount(); i++)

                                     {
                                         if (bissnesSpinner.getItemAtPosition(i).equals(work) )
                                         {
                                             bissnesSpinner.setSelection(i);
                                             bissnesSpinner.setEnabled(false);
                                             break;
                                         }
                                     }
                                 }




                                 if (entry.getKey().equals("spiritualStatus")) {
                                     spiritualStatus =entry.getValue().toString();
                                     // genderSpinner.setAdapter(gender);
                                     Log.d("spiritualStatus",spiritualStatus);
                                     //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                     for(int i = 0; i <spiritualStatusSpinner.getAdapter().getCount(); i++)

                                     {
                                         if (spiritualStatusSpinner.getItemAtPosition(i).equals(spiritualStatus) )
                                         {
                                             spiritualStatusSpinner.setSelection(i);
                                             spiritualStatusSpinner.setEnabled(false);
                                             break;
                                         }
                                     }
                                 }




                                 if (entry.getKey().equals("familyStatus")) {
                                     familyStatus =entry.getValue().toString();
                                     // genderSpinner.setAdapter(gender);
                                     Log.d("familyStatus",familyStatus);
                                     //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                     for(int i = 0; i <famelyStatusSpinner.getAdapter().getCount(); i++)

                                     {
                                         if (famelyStatusSpinner.getItemAtPosition(i).equals(familyStatus) )
                                         {
                                             famelyStatusSpinner.setSelection(i);
                                             famelyStatusSpinner.setEnabled(false);
                                             break;
                                         }
                                     }
                                 }




                                 if (entry.getKey().equals("isPregnant")) {
                                     isPregnanttext = entry.getValue().toString();
                                     // genderSpinner.setAdapter(gender);
                                     Log.d("isPregnant", String.valueOf(isPregnant));
                                     //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                     if(isPregnanttext.equals("false"))
                                         isPregnant=false;
                                     else
                                         isPregnant=true;
                                       prignent.setChecked(isPregnant);
                                         prignent.setEnabled(false);
                                 }

                                 if (entry.getKey().equals("pregnantWeek")) {
                                    pregnantWeek=entry.getValue().toString();
                                     weekOfPregnent.setText(pregnantWeek);
                                     weekOfPregnent.setEnabled(false);
                                 }



                                 if (entry.getKey().equals("isBreastfeeding")) {
                                     tbreastfeed=entry.getValue().toString();
                                     if(tbreastfeed.equals("false"))
                                         isbreastfeed=false;
                                     else
                                         isbreastfeed=true;
                                     breastfeed.setChecked(isbreastfeed);
                                     breastfeed.setEnabled(false);
                                 }






                             }
                         firstEntry=(Boolean)document.get("isCompleted");
                       } else {
                           Log.d("LOGGER", "No such document");
                       }
                   } else {
                       Log.d("LOGGER", "get failed with ", task.getException());
                   }
                   if(!firstEntry||user_guidf==null) {
                       Log.d("user_guidf","user_guidf=null");

                       view.removeAllViewsInLayout();
                       TextView txt = new TextView(getActivity());
                       txt.setText("הטופס עדיין לא הוגש");
                       txt.setTextSize(24);
                       txt.setTextColor(Color.BLACK);
                       txt.setGravity(Gravity.RIGHT);
                       txt.setLayoutParams(new LinearLayout.LayoutParams(
                               DrawerLayout.LayoutParams.FILL_PARENT,
                               DrawerLayout.LayoutParams.WRAP_CONTENT));

                       view.addView(txt);
                   }

               }


           });


       }




        // view=inflater.inflate(R.layout.activity_details, container, false);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("clicked","button clicked");
               Frag5 fragment = new Frag5();
              if(fragment!=null) {
                    Bundle arguments = new Bundle();
                    arguments.putString("Uid", gUid);
                  arguments.putString("user_gUid", user_guidf);
                    fragment.setArguments(arguments);


                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.addToBackStack(null);
                    Log.d("container", String.valueOf(((ViewGroup) getView().getParent()).getId()));
                    transaction.replace(R.id.root_frame3, fragment).commit();
                    getChildFragmentManager().executePendingTransactions();


                    //make your toast here

                    //theLayout.removeAllViewsInLayout();
                    // view=(LinearLayout)inflater.inflate(R.layout.activity_oral_hygiene, container, false);
                }

            }
        });



        return view;
    }






}
