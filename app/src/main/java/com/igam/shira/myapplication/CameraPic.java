package com.igam.shira.myapplication;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.app.Activity;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class CameraPic extends Activity {
    private Camera mCamera;
    private CameraPreview mCameraPreview;
    private StorageReference mStorageRef;
    private ProgressDialog mProgress;
    private String weekNum = "EE";
    private String gUid;
    private List<File> pictures=new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_pic);
        gUid= getIntent().getStringExtra("Uid");
        weekNum=getIntent().getStringExtra("submission");
        mCamera = getCameraInstance();
        mCameraPreview = new CameraPreview(this, mCamera);
        mProgress = new ProgressDialog(this);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mCameraPreview);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        Button captureButton = (Button) findViewById(R.id.button_capture);
        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CountDownTimer(5000,2000){

                    @Override
                    public void onFinish() {

                    }

                    @Override
                    public void onTick(long millisUntilFinished) {
                        mCamera.startPreview();
                        mCamera.takePicture(null, null, mPicture);

                    }

                }.start();


            }
        });
    }

    /**
     * Helper method to access the camera returns null if it cannot get the
     * camera or does not exist
     *
     * @return
     */
    private Camera getCameraInstance() {
        Camera camera = null;
        try {
            camera = Camera.open();
        } catch (Exception e) {
            // cannot get camera or does not exist
        }
        return camera;
    }

    PictureCallback mPicture = new PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = getOutputMediaFile();
            if (pictureFile == null) {
                return;
            }
            try {

                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);

                fos.close();
                pictures.add(pictureFile);
                Uri uri=Uri.fromFile(pictureFile);
                uploadMethod(uri);
            } catch (FileNotFoundException e) {

            } catch (IOException e) {
            }
        }

    };

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "MyCameraApp");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }





    // ============================================================================================
    //  upload image to Firebase .
    // ============================================================================================
    public void upload(int requestCode, int resultCode, Intent data)


    {
        if (requestCode == 111 && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] dataBAOS = baos.toByteArray();

            mProgress.setMessage("Uploading Image...");
            mProgress.show();

//            StorageReference imagesRef = mStorageRef.child("images").child("IMG_" + userName + "_" +  new Date().getTime());
            StorageReference imagesRef = mStorageRef.child("images").child("IMG__userID_" + gUid + "_week_"+ weekNum +"_" + new Date().getTime());

            imagesRef.putBytes(dataBAOS).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(CameraPic.this, "Successfully uploading the image file!", Toast.LENGTH_LONG).show();

                    mProgress.dismiss();

                }

            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    mProgress.dismiss();
                    Toast.makeText(CameraPic.this, "Error uploading file... ", Toast.LENGTH_LONG).show();

                }
            });
        }
    }




//////////////////////////////////////////////////////////////////////
private void uploadMethod(Uri imageUri) {
   // progressDialog();
    FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
    StorageReference storageReferenceProfilePic = firebaseStorage.getReference();
    StorageReference imageRef = storageReferenceProfilePic.child("images").child(gUid).child(weekNum).child("IMG__userID_" + gUid + "_week_"+ weekNum +"_" + new Date().getTime());

    imageRef.putFile(imageUri)
            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //if the upload is successful
                    //hiding the progress dialog
                    //and displaying a success toast
                   // mProgress.setMessage("Uploading Image...");
                  //  mProgress.show();
                    Toast.makeText(CameraPic.this, "succed uploading file... ", Toast.LENGTH_LONG).show();

                    //String profilePicUrl = taskSnapshot.getDownloadUrl().toString();
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    //if the upload is not successful
                    //hiding the progress dialog
                    mProgress.dismiss();
                    Toast.makeText(CameraPic.this, "Error uploading file... ", Toast.LENGTH_LONG).show();
                    //and displaying error message
                }
            })
            .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    //calculating progress percentage
//                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
//                        //displaying percentage in progress dialog
//                        progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                }
            });

}





}