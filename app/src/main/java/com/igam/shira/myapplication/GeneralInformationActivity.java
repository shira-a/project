package com.igam.shira.myapplication;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class GeneralInformationActivity extends AppCompatActivity {
    private String gUid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_information);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        ((TextView)getSupportActionBar().getCustomView().findViewById(R.id.tvTitle)).setText("מידע כללי");

        gUid= getIntent().getStringExtra("Uid");

    }
    public void next(View view)
    {
        Intent intent = new Intent(GeneralInformationActivity.this, DetailsActivity.class);
        intent.putExtra("Uid",gUid);
        startActivity(intent);
    }
}
