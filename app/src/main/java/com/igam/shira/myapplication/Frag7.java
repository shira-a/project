package com.igam.shira.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import com.twilio.http.TwilioRestClient;
import com.twilio.rest.api.v2010.account.MessageCreator;

import retrofit2.http.HEAD;
import spark.Spark;

import static spark.Spark.get;
import static spark.Spark.post;


public class Frag7 extends Fragment {
    private LinearLayout view;
    private DocumentReference docRef;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private String gUid, user_guidf;
    private EditText message;
    private String phoneNumber;
    private Button send;
    private String Sms_message;
    private OkHttpClient mClient = new OkHttpClient();
    private Context mContext;
    private Map<String, Object> smsHistory = new HashMap<>();
    private Map<String, Object> data = new HashMap<>();
    private Map<String, Object> counter= new HashMap<>();
    private Map<String, Object> map1 =new HashMap<>();
    private Map<String, Object> map =new HashMap<>();
    private int count;
    //   public static final String ACCOUNT_SID = "";
    // public static final String AUTH_TOKEN = "";


    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        //  ActionBar actionBar = getActivity().getActionBar();
        //  actionBar.setTitle("פרטים");
        gUid = getArguments().getString("Uid");
        user_guidf = getArguments().getString("user_gUid");
        if (user_guidf != null)
            Log.d("user_guidf", user_guidf);
        else
            Log.d("user_guidf", "new user");

        mContext = getActivity().getApplicationContext();
        view = (LinearLayout) inflater.inflate(R.layout.messages, container, false);
        message = (EditText) view.findViewById(R.id.sms_message);
        send = (Button) view.findViewById(R.id.send_sms);


        if (gUid != null) {

            docRef = db.collection("users-data").document(gUid);

            Log.d("clicked", "button clicked");
            // Log.d("phone", phoneNumber);
            // Log.d("Sms_message",Sms_message);


            if (user_guidf != null) {


                docRef = db.collection("users-data").document(user_guidf);

                docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                //     Log.i("LOGGER","First "+document.getString("first_entry"));

                                phoneNumber = (String) document.get("phone");
                                phoneNumber=phoneNumber.substring(1);
                                Log.d("phonenumber444",phoneNumber);
                                phoneNumber="+972"+phoneNumber;
                            } else {
                                Log.d("LOGGER", "No such document");
                            }
                        } else {
                            Log.d("LOGGER", "get failed with ", task.getException());
                        }

                    }
                });
                DocumentReference docRef1 = db.collection("users-data").document(user_guidf);
                docRef1.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();

                            if (document.exists()) {
                                //     Log.i("LOGGER","First "+document.getString("first_entry"));
                                map = (Map) document.get("smsHistory");
                                Log.d("map1", String.valueOf(map));
                                count=0;
                                if(map!=null) {
                                    smsHistory=map;
                                    Log.d("smsHistory", String.valueOf(smsHistory));
                                     map1 = (Map) map.get(String.valueOf(count));
                                    Log.d("map", String.valueOf(map1));
                                    while (map1 != null) {
                                        count++;
                                        map1 = (Map) map.get(String.valueOf(count));

                                    }

                                    Log.d("map", String.valueOf(map1));
                                }
                                else
                                    map=new HashMap<>();


                            } else {
                                Log.d("LOGGER", "No such document");
                            }
                        } else {
                            Log.d("LOGGER", "get failed with ", task.getException());
                        }

                    }
                });
            }

            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("count", String.valueOf(count));
                    Sms_message = message.getText().toString();
                    Date date= Calendar.getInstance().getTime();
                    data.put("date",date);
                    data.put("content",Sms_message);
                    counter.put(String.valueOf(count),data);
                    map.put("smsHistory",counter);
                    count++;

                    db.collection("users-data").document(user_guidf).update(map)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.e(" successfully written!", "o");
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.e("Error writing document", "o");
                                }
                            });
                    try {
                        post("https://smsbackend12.herokuapp.com/sms", new Callback() {

                            @Override
                            public void onFailure(Call call, IOException e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d("Message sent", Sms_message);
                                        Toast.makeText(getActivity().getApplicationContext(), "SMS Sent!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
        }

        return view;

    }

    Call post(String url, Callback callback) throws IOException {
        RequestBody formBody = new FormBody.Builder()
                .add("To", phoneNumber)
                .add("Body", Sms_message)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        Call response = mClient.newCall(request);
        response.enqueue(callback);
        return response;
    }
}




