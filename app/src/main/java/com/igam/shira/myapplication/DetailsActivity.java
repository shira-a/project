package com.igam.shira.myapplication;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class DetailsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private String gender,country,race,spiritualStatus,familyStatus,work,gUid;
    static private String birthDate;
    private boolean isPregnant=false,isBreastFeeding=false;
    TextView error=null;
    boolean gender_flag=false;
    String Weeks;
    private Spinner genderSpinner,countrySpinner,raceSpinner,spiritualStatusSpinner,familyStatusSpinner,workSpinner;
    TextView date;
    static TextView birthDate1;
    EditText WeeksOfPregnancy;
    private boolean flagPregnancy=false;
    private View divider1,divider2;
    private LinearLayout linear1,Pregnant,changeSub11;
    private Button next;
    private Map<String, Object> GeneralForm = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        ((TextView)getSupportActionBar().getCustomView().findViewById(R.id.tvTitle)).setText("שאלון הרשמה");
        WeeksOfPregnancy=(EditText) findViewById(R.id.weeksofPregnancy) ;
        Pregnant=(LinearLayout)findViewById(R.id.checkboxPregnant1) ;
        changeSub11=(LinearLayout) findViewById(R.id.changeSub);
        divider1=(View)findViewById(R.id.divider009);
        divider2=(View)findViewById(R.id.divider7);
        linear1=(LinearLayout)findViewById(R.id.LinearBreast) ;
        next=(Button)findViewById(R.id.next);
        Pregnant.setVisibility(View.GONE);
        changeSub11.setVisibility(View.GONE);
        divider1.setVisibility(View.GONE);
        divider2.setVisibility(View.GONE);
        linear1.setVisibility(View.GONE);
        birthDate1=(TextView)findViewById(R.id.birthDate);
        genderSpinner = (Spinner) findViewById(R.id.gender);
        countrySpinner=(Spinner) findViewById(R.id.country);
        raceSpinner=(Spinner) findViewById(R.id.race);
        spiritualStatusSpinner=(Spinner) findViewById(R.id.spiritualStatus);
        familyStatusSpinner=(Spinner) findViewById(R.id.familyStatus);
        workSpinner=(Spinner) findViewById(R.id.work);
        genderSpinner.setOnItemSelectedListener(this);
        gUid= getIntent().getStringExtra("Uid");
        Log.d("user id",gUid);
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();

        Button btn = (Button) findViewById(R.id.next);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gender= genderSpinner.getSelectedItem().toString();
                country=countrySpinner.getSelectedItem().toString();
                race= raceSpinner.getSelectedItem().toString();
                spiritualStatus= spiritualStatusSpinner.getSelectedItem().toString();
                familyStatus= familyStatusSpinner.getSelectedItem().toString();
                work= workSpinner.getSelectedItem().toString();
                final CheckBox pregnant=(CheckBox)findViewById(R.id.checkboxPregnant);
                if(pregnant.isChecked()) {
                    isPregnant=true;

                }
                Weeks = WeeksOfPregnancy.getText().toString();
                final CheckBox breastFeeding=(CheckBox)findViewById(R.id.checkboxBreastfeeding);
                if(breastFeeding.isChecked()){
                    isBreastFeeding=true;
                }
                GeneralForm.put("gId_", gUid);
                GeneralForm.put("birthDate",birthDate);
                GeneralForm.put("gender", gender);
                GeneralForm.put("country",country);
                GeneralForm.put("race",race);
                GeneralForm.put("spiritualStatus_",spiritualStatus);
                GeneralForm.put("familyStatus",familyStatus);
                GeneralForm.put("work",work);
                GeneralForm.put("isPregnant",isPregnant);
                GeneralForm.put("pregnantWeek",Weeks);
                GeneralForm.put("isBreastfeeding",isBreastFeeding);
                if(validation()) {

                    Intent intent = new Intent(DetailsActivity.this, HealthQuestionnaireActivity.class);
                    intent.putExtra("generalForm", (Serializable) GeneralForm);
                    intent.putExtra("Uid", gUid);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(DetailsActivity.this, "נא הכנס פרטים",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {//birth date selection

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            birthDate= year + "-" + month + "-" + day;
            Log.e("weeks",birthDate );
            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            int month8=month+1;
            birthDate= day + "/" + month8 + "/" + year;
            birthDate1.setText(birthDate);
            Log.e("weeks",birthDate );
        }
    }

    public void showDatePickerDialog(View v) {//show the date selected
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Object item = parent.getItemAtPosition(position);
        gender_flag=false;
        if (position == 2) {//if gender female was selected the option of pregnant and breastfeeding appears
            Pregnant.setVisibility(View.VISIBLE);
            divider1.setVisibility(View.VISIBLE);
            divider2.setVisibility(View.VISIBLE);
            linear1.setVisibility(View.VISIBLE);
        }
        else
        {
            Pregnant.setVisibility(View.GONE);
            divider1.setVisibility(View.GONE);
            divider2.setVisibility(View.GONE);
            linear1.setVisibility(View.GONE);
            changeSub11.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }
    public void ShowWeeks(View v)//if pregnant was selected the weeks of pregnancy appears
    {
        if(flagPregnancy==false)
        {
            changeSub11.setVisibility(View.VISIBLE);
            flagPregnancy=true;
        }
        else
        {
            changeSub11.setVisibility(View.GONE);
            flagPregnancy=false;
        }
    }
    private boolean validation() {//checks if all fields are filled

        if(gender==null||gender.isEmpty())
        {
            return false;
        }
        if(country==null||country.isEmpty())
        {

            return false;
        }
        if(race==null||race.isEmpty())
        {
            return false;
        }
        if(spiritualStatus==null||spiritualStatus.isEmpty())
        {
            return false;
        }
        if(familyStatus==null||familyStatus.isEmpty())
        {
            return false;
        }
        if(work==null||work.isEmpty())
        {

            return false;
        }
        if(birthDate==null||birthDate.isEmpty())
        {

            return false;
        }
        next.setBackgroundColor(Color.parseColor("#ff0099cc"));
        return true;
    }





}
