package com.igam.shira.myapplication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;

public class Frag5 extends Fragment {
    private  LinearLayout view;
    private FirebaseFirestore db =FirebaseFirestore.getInstance();
private EditText numOfCigarettesADay,yearsOfSmoking;
private String num_of_cigarettes=null,years_Of_Smoking=null;
    private String gUid,smoketext,user_guidf;
    private CheckBox smoke;
   private boolean isSmoking;
    @Nullable
    @Override


    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        //  ActionBar actionBar = getActivity().getActionBar();
        //  actionBar.setTitle("פרטים");
        gUid= getArguments().getString("Uid");
        user_guidf=getArguments().getString("user_gUid");
        if(user_guidf!=null)
            Log.d("user_guidf",user_guidf);
        else
            Log.d("gUid","new user");


        view = (LinearLayout) inflater.inflate(R.layout.activity_health_questionnaire, container, false);

        numOfCigarettesADay=(EditText)view.findViewById(R.id.numofcigarettes);
        yearsOfSmoking=(EditText)view.findViewById(R.id.yearsofsmoking);
        smoke=(CheckBox)view.findViewById(R.id.isSmoking);

        if(user_guidf!=null) {

            DocumentReference docRef = db.collection("users-data").document(user_guidf);


            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            //     Log.i("LOGGER","First "+document.getString("first_entry"));
                            Map<String, Object> map =(Map) document.get("healthForm");





                            for(Map.Entry<String, Object> entry : map.entrySet()) {
                                if (entry.getKey().equals("numOfCigarettesADay")) {
                                    num_of_cigarettes=entry.getValue().toString();
                                    Log.d("num_of_cigarettes",num_of_cigarettes);
                                    Log.d("kkk","hhh"+num_of_cigarettes);

                                    numOfCigarettesADay.setText(num_of_cigarettes);
                                    numOfCigarettesADay.setEnabled(false);
                                }

                                if (entry.getKey().equals("yearsOfSmoking")) {

                                    years_Of_Smoking=entry.getValue().toString();
                                    yearsOfSmoking.setText(years_Of_Smoking);
                                    yearsOfSmoking.setEnabled(false);
                                }






                                if (entry.getKey().equals("isSmoking")) {
                                    smoketext = entry.getValue().toString();
                                    // genderSpinner.setAdapter(gender);
                                    Log.d("smoketext", String.valueOf(smoketext));
                                    //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                    if(smoketext.equals("false"))
                                        isSmoking=false;
                                    else
                                        isSmoking=true;

                                    smoke.setChecked(isSmoking);
                                    smoke.setEnabled(false);
                                }


                            }


                        } else {
                            Log.d("LOGGER", "No such document");
                        }
                    } else {
                        Log.d("LOGGER", "get failed with ", task.getException());
                    }
                }
            });


        }




        // view=inflater.inflate(R.layout.activity_details, container, false);
        Button  btn = (Button) view.findViewById(R.id.next);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("clicked","button clicked");
                 Frag6 fragment = new Frag6();
                if(fragment!=null) {
                    Bundle arguments = new Bundle();
                    arguments.putString("Uid", gUid);
                    arguments.putString("user_gUid", user_guidf);
                    fragment.setArguments(arguments);


                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.addToBackStack(null);
                    Log.d("container", String.valueOf(((ViewGroup) getView().getParent()).getId()));
                    //transaction.hide(NewUser.this);
                    transaction.replace(R.id.root_frame5, fragment).commit();
                    getChildFragmentManager().executePendingTransactions();


                    //make your toast here

                    //theLayout.removeAllViewsInLayout();
                    // view=(LinearLayout)inflater.inflate(R.layout.activity_oral_hygiene, container, false);
                }

            }
        });


        return view;
    }
}
