package com.igam.shira.myapplication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class NewUser extends Fragment  implements View.OnClickListener, NewUserAdapterSpinner {
    EditText user_code, user_phone, user_password, verification_code;
    private FirebaseFirestore db =FirebaseFirestore.getInstance();

    Spinner role;
    int code,random_number;
    LinearLayout view;
    Button random,save_btn,phone_button;
    String gUid,userRole,userPhone,userCode,userPassworord;
    boolean exsist_user=false;
    private FirebaseAuth mAuth;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
      //  ActionBar actionBar = getActivity().getActionBar();
//        actionBar.setTitle("טופס הרשמה");
       // super.onCreate(savedInstanceState);

        view = (LinearLayout)inflater.inflate(R.layout.edit_new_user, container, false);
        mAuth = FirebaseAuth.getInstance();

        // view.setOnClickListener(this);
        user_code = (EditText) view.findViewById(R.id.user_code);
        user_phone = (EditText) view.findViewById(R.id.user_phone);
        user_password = (EditText) view.findViewById(R.id.user_password);
        user_password.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        role = (Spinner) view.findViewById(R.id.role);
        verification_code = (EditText) view.findViewById(R.id.verification_code);
         save_btn = (Button) view.findViewById(R.id.save);
         random=(Button)view.findViewById(R.id.random_number);
        phone_button=(Button)view.findViewById(R.id.phone_password) ;
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("clicked", "button clicked");

                if (validation()) {
                    userCode=user_code.getText().toString();
                    userPhone=user_phone.getText().toString();
                    gUid=userCode+"@igam.com";
                    userRole= role.getSelectedItem().toString();
                    userPhone=user_phone.getText().toString();
                    userPassworord=user_password.getText().toString();
                    Map<String, Object> data = new HashMap<>();
                    data.put("role", userRole);
                    data.put("phone",userPhone);
                    data.put("id",gUid);
                    data.put("first_entry",false);




                    mAuth.createUserWithEmailAndPassword(gUid, userPassworord);





                    db.collection("users-data").document(gUid).set(data)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.e(" successfully written!","o");
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.e("Error writing document","o");
                                }
                            });


                     Frag1 fragment = new Frag1();


                 if (fragment != null) {

                        Bundle arguments = new Bundle();
                        arguments.putString("Uid", gUid);
                        fragment.setArguments(arguments);
                     /* final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, fragment , "NewFragmentTag");
                    ft.commit();*/


                     android.support.v4.app.FragmentTransaction transaction = getChildFragmentManager().beginTransaction();


                     transaction.addToBackStack("New user");
                   //  getSupportFragmentManager().popBackStackImmediate("New user", 0);

                     getActivity().getSupportFragmentManager().popBackStack();

                     Log.d("container", String.valueOf(((ViewGroup)getView().getParent()).getId()));
                        //transaction.hide(NewUser.this);
                        transaction.replace(R.id.root_frame, fragment).show(fragment).commit();
                       getChildFragmentManager().executePendingTransactions();


                        // getFragmentManager().beginTransaction().add(fragment,null);
                        // getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        //  transaction.addToBackStack("null");
                        //transaction.addToBackStack("tagname");


                        //transaction.commit();
                    }


                }
                //make your toast here


            }
        });
        random.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("clicked", "button clicked");

                 int number =randomNumber();
                     userCode = String.valueOf(number);
                    user_code.setText(userCode);
                //make your toast here


            }
        });

        phone_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userPhone=user_phone.getText().toString();
                user_password.setText(userPhone);

                //make your toast here


            }
        });



        return view;

    }

    private int randomNumber() {
        boolean flag=false;
        int number = 0;
        while(!flag) {
            Random r = new Random(System.currentTimeMillis());
             number = (10000 + r.nextInt(20000));
            if (!checkIfUserExsist(number))
                flag=true;

        }

        return number;
    }

    private boolean checkIfUserExsist(int number) {
        final String user=String.valueOf(number)+"@igam.com";
        exsist_user=false;

        db.collection("users-data").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        if (document.exists()) {
                            String s = (String) document.get("id");
                            if(s!=null)
                            if(s.equals(user)) {
                                Log.d("user",document.toString());
                                exsist_user=true;
                                break;
                            }
                        }
                    }
                }
            }
        });
        Log.d("exsist_user", String.valueOf(exsist_user));
        return exsist_user;
    }


    private boolean validation() {
        int yourDesiredLength = 1;
        int user_phone_number=10;
        if (user_code.getText().length() < yourDesiredLength) {
            user_code.setError("Your Input is Invalid");
            return false;
        } else if (user_phone.getText().length() < yourDesiredLength) {
            user_phone.setError("Your Input is Invalid");
            return false;
        }
        else {

            return true;
        }
    }


    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.random_number)
            Log.d("vvvvv","vvvvvvvv");
    }



    public class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;
            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }
            public char charAt(int index) {
                return '*'; // This is the important part
            }
            public int length() {
                return mSource.length(); // Return default
            }
            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    };

    @Override
    public void onDetach() {
        super.onDetach();
    }



}