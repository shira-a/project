package com.igam.shira.myapplication;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class HealthQuestionnaireActivity extends AppCompatActivity {
    private Map<String, Object> GeneralForm = new HashMap<>();
    private Map<String, Object> healthForm = new HashMap<>();
    public boolean isSmoking = false,ismoking=false;
    private Button prev,nextBtn;
    private EditText cigarettesAday,yearsSmoking;
    private TextView cigarettesAday1,yearsSmoking1;
    private String numOfCigarettes,yearsofsmoking,gUid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_questionnaire);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        ((TextView)getSupportActionBar().getCustomView().findViewById(R.id.tvTitle)).setText("שאלון הרשמה");
        gUid = getIntent().getStringExtra("Uid");
        GeneralForm = (Map<String, Object>) getIntent().getSerializableExtra("generalForm");
        cigarettesAday=findViewById(R.id.numofcigarettes);
        yearsSmoking=findViewById(R.id.yearsofsmoking);
        cigarettesAday1=findViewById(R.id.changeSub);
        yearsSmoking1=findViewById(R.id.textView17);
        cigarettesAday1.setVisibility(View.GONE);
        cigarettesAday.setVisibility(View.GONE);
        yearsSmoking1.setVisibility(View.GONE);
        yearsSmoking.setVisibility(View.GONE);
        prev=(Button)findViewById(R.id.prevID);
        nextBtn=(Button)findViewById(R.id.next);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveAndNext();
            }
        });
    }

    public void isChecked(View v) {
        final CheckBox c1 = (CheckBox) findViewById(R.id.isSmoking);
        if (c1.isChecked()) {
            isSmoking = true;
        }

        if(ismoking==false)//if he smokes the questions about smoking appears
        {
            cigarettesAday1.setVisibility(View.VISIBLE);
            cigarettesAday.setVisibility(View.VISIBLE);
            yearsSmoking1.setVisibility(View.VISIBLE);
            yearsSmoking.setVisibility(View.VISIBLE);
            ismoking=true;
        }
        else
        {
            cigarettesAday1.setVisibility(View.GONE);
            cigarettesAday.setVisibility(View.GONE);
            yearsSmoking1.setVisibility(View.GONE);
            yearsSmoking.setVisibility(View.GONE);
            ismoking=false;
        }
    }


    public void saveAndNext() {
        numOfCigarettes=cigarettesAday.getText().toString();
        yearsofsmoking=yearsSmoking.getText().toString();

        healthForm.put("isSmoking", isSmoking);
        healthForm.put("numOfCigarettesADay", numOfCigarettes);
        healthForm.put("yearsOfSmoking", yearsofsmoking);

        Map<String, Object> postValues = healthForm;

        if(validation()){

            Intent intent = new Intent(HealthQuestionnaireActivity.this, OralHygieneActivity.class);
            intent.putExtra("generalForm", (Serializable) GeneralForm);
            intent.putExtra("healthForm", (Serializable) healthForm);
            intent.putExtra("Uid", gUid);
            startActivity(intent);
        }
        else
            Toast.makeText(HealthQuestionnaireActivity.this, "נא הכנס פרטים",
                    Toast.LENGTH_SHORT).show();
    }
    public boolean validation()//checks if all fields are filled
    {
        if(ismoking==true&&numOfCigarettes.isEmpty())
        {
            return false;
        }
        if(ismoking==true&&yearsofsmoking.isEmpty())
        {
            return false;
        }
        return true;

    }


}
