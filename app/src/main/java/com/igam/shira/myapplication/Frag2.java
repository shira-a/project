package com.igam.shira.myapplication;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Frag2 extends Fragment {
    private LinearLayout view;
    private String gUid,user_guidf;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    private Map<String, Object> mouthReviewSubmissions1 = new HashMap<>();
    private Map<String, Object> mouthReviewSubmissions = new HashMap<>();
    private Map<String, Object> mouth1 = new HashMap<>();
    private Map<String, Object> mouth2 = new HashMap<>();
    private Map<String, Object> mouth3 = new HashMap<>();
    private Map<String, Object> mouth4 = new HashMap<>();
    private Map<String, Object> mouth5 = new HashMap<>();

    private Date unlockDate;
    private Date updateDate;
    private String timeLeft;
    private String weekNum = "EE";
    private ImageButton week0,week1,week2,week3,week4,week5,week6,week7;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        gUid = getArguments().getString("Uid");
        user_guidf=getArguments().getString("user_gUid");
        if (user_guidf != null)
            Log.d("gUid", user_guidf);
        else
            Log.d("gUid", "new user");


        ActionBar actionBar = getActivity().getActionBar();

        //  actionBar.setTitle("לוח מעקב");
        view = (LinearLayout) inflater.inflate(R.layout.activity_treatment_progress, container, false);

        week0=  (ImageButton)view.findViewById(R.id.imageButtonWeek0);
        week1=  (ImageButton)view.findViewById(R.id.imageButtonWeek1);
        week2=  (ImageButton)view.findViewById(R.id.imageButtonWeek2);
        week3=  (ImageButton)view.findViewById(R.id.imageButtonWeek3);
        week4=  (ImageButton)view.findViewById(R.id.imageButtonWeek4);
        week5=  (ImageButton)view.findViewById(R.id.imageButtonWeek5);
        week6=  (ImageButton)view.findViewById(R.id.imageButtonWeek6);
        week7=  (ImageButton)view.findViewById(R.id.imageButtonWeek7);







        week0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_guidf!=null)
                {
                    Intent intent = new Intent(getActivity(), SubmissionDetails.class);
                    intent.putExtra("Uid", gUid);
                    intent.putExtra("user_Uid", user_guidf);
                    intent.putExtra("submission","0");
                    startActivity(intent);
                }


                }

        });
        week1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_guidf!=null)
                {
                    Intent intent = new Intent(getActivity(), SubmissionDetails.class);
                    intent.putExtra("Uid", gUid);
                    intent.putExtra("user_Uid", user_guidf);
                    intent.putExtra("submission","1");
                    startActivity(intent);
                }


            }

        });
        week2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_guidf!=null)
                {
                    Intent intent = new Intent(getActivity(), SubmissionDetails.class);
                    intent.putExtra("Uid", gUid);
                    intent.putExtra("user_Uid", user_guidf);
                    intent.putExtra("submission","2");
                    startActivity(intent);
                }


            }

        });




        week3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_guidf!=null)
                {
                    Intent intent = new Intent(getActivity(), SubmissionDetails.class);
                    intent.putExtra("Uid", gUid);
                    intent.putExtra("user_Uid", user_guidf);
                    intent.putExtra("submission","3");
                    startActivity(intent);
                }


            }

        });




        week4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_guidf!=null)
                {
                    Intent intent = new Intent(getActivity(), SubmissionDetails.class);
                    intent.putExtra("Uid", gUid);
                    intent.putExtra("user_Uid", user_guidf);
                    intent.putExtra("submission","4");
                    startActivity(intent);
                }


            }

        });

        week5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_guidf!=null)
                {
                    Intent intent = new Intent(getActivity(), SubmissionDetails.class);
                    intent.putExtra("Uid", gUid);
                    intent.putExtra("user_Uid", user_guidf);
                    intent.putExtra("submission","5");
                    startActivity(intent);
                }


            }

        });
        week6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_guidf!=null)
                {
                    Intent intent = new Intent(getActivity(), SubmissionDetails.class);
                    intent.putExtra("Uid", gUid);
                    intent.putExtra("user_Uid", user_guidf);
                    intent.putExtra("submission","6");
                    startActivity(intent);
                }


            }

        });
        week7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_guidf!=null)
                {
                    Intent intent = new Intent(getActivity(), SubmissionDetails.class);
                    intent.putExtra("Uid", gUid);
                    intent.putExtra("user_Uid", user_guidf);
                    intent.putExtra("submission","7");
                    startActivity(intent);
                }


            }

        });







        if(user_guidf!=null) {
    final DocumentReference docRef = db.collection("users-data").document(user_guidf);
    docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
        @Override
        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    //     Log.i("LOGGER","First "+document.getString("first_entry"));
                    Boolean b;
                    b = (Boolean) document.get("isCompleted");
                    if (b == null)
                        b = false;
                    //if the field is Boolean

                    if (b) {
                        updateDate = Calendar.getInstance().getTime();
                        final DocumentReference docRef1 = db.collection("users-data").document(user_guidf);
                        docRef1.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if (task.isSuccessful()) {
                                    DocumentSnapshot document1 = task.getResult();
                                    if (document1.exists()) {
                                        Log.d("LOGGER", " such document");
                                        Map<String, Object> map = (Map) document1.get("mouthReviewSubmissions");
                                        Map<String, Object> map1 = (Map) map.get("mouth0");
                                        Map<String, Object> map2 = (Map) map.get("mouth1");
                                        Map<String, Object> map3 = (Map) map.get("mouth2");
                                        Map<String, Object> map4 = (Map) map.get("mouth3");
                                        Map<String, Object> map5 = (Map) map.get("mouth4");
                                        Date d1, d2, d3, d4, d5;
                                        String y1, y2, y3, y4, y5;
                                        d1 = (Date) map1.get("unlockDate");
                                        timeLeft = ReturnTimeLeft(d1, updateDate);
                                        TextView tx = (TextView) view.findViewById(R.id.timeLeft1);

                                        if (timeLeft != "לא הוגש") {

                                            tx.setText(timeLeft);
                                            ImageButton b1;
                                            b1 = (ImageButton) view.findViewById(R.id.imageButtonWeek0);
                                            b1.setEnabled(false);
                                        } else {

                                            y1 = (String) map1.get("status");
                                            tx.setText(y1);

                                        }

                                        d2 = (Date) map2.get("unlockDate");
                                        timeLeft = ReturnTimeLeft(d2, updateDate);
                                        TextView to = (TextView)view.findViewById(R.id.timeLeft2);

                                        if (timeLeft != "לא הוגש") {
                                            to.setText(timeLeft);
                                            ImageButton b2;
                                            b2 = (ImageButton)view.findViewById(R.id.imageButtonWeek1);
                                            b2.setEnabled(false);
                                        } else {
                                            y2 = (String) map2.get("status");
                                            to.setText(y2);

                                        }

                                        d3 = (Date) map3.get("unlockDate");
                                        timeLeft = ReturnTimeLeft(d3, updateDate);
                                        TextView tt = (TextView) view.findViewById(R.id.timeLeft3);

                                        if (timeLeft != "לא הוגש") {
                                            tt.setText(timeLeft);
                                            ImageButton b3;
                                            b3 = (ImageButton) view.findViewById(R.id.imageButtonWeek2);
                                            b3.setEnabled(false);
                                        } else {
                                            y3 = (String) map3.get("status");
                                            tt.setText(y3);

                                        }

                                        d4 = (Date) map4.get("unlockDate");
                                        timeLeft = ReturnTimeLeft(d4, updateDate);
                                        TextView tr = (TextView)view.findViewById(R.id.timeLeft4);

                                        if (timeLeft != "לא הוגש") {
                                            tr.setText(timeLeft);
                                            ImageButton b4;
                                            b4 = (ImageButton)view.findViewById(R.id.imageButtonWeek3);
                                            b4.setEnabled(false);
                                        } else {
                                            y4 = (String) map4.get("status");
                                            tr.setText(y4);
                                        }

                                        d5 = (Date) map5.get("unlockDate");
                                        timeLeft = ReturnTimeLeft(d5, updateDate);
                                        TextView th = (TextView)view.findViewById(R.id.timeLeft5);

                                        if (timeLeft != "לא הוגש") {
                                            th.setText(timeLeft);
                                            ImageButton b5;
                                            b5 = (ImageButton) view.findViewById(R.id.imageButtonWeek4);
                                            b5.setEnabled(false);
                                        } else {
                                            y5 = (String) map5.get("status");
                                            th.setText(y5);

                                        }


                                    } else {
                                        Log.d("LOGGER", "No such document");
                                    }
                                } else {
                                    Log.d("LOGGER", "get failed with ", task.getException());
                                }
                            }
                        });

                    } else if (!b) {

                        db.collection("users-data").document(user_guidf).update("isCompleted", true)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.e(" successfully written!", "o");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.e("Error writing document", "o");
                                    }
                                });


                        updateDate = Calendar.getInstance().getTime();

                        // unlockDate=updateDate;
                        timeLeft = ReturnTimeLeft(updateDate, updateDate);
                        TextView tx = (TextView) view.findViewById(R.id.timeLeft1);
                        tx.setText(timeLeft);
                        if (timeLeft != "לא הוגש") {
                            ImageButton b1;
                            b1 = (ImageButton)view.findViewById(R.id.imageButtonWeek0);
                            b1.setEnabled(false);
                        }


                        mouth1.put("updateDate", updateDate);
                        mouth1.put("unlockDate", updateDate);
                        mouth1.put("status", "לא הוגש");
                        mouth1.put("sequenceNumber", 1);
                        unlockDate = new Date(updateDate.getYear(), updateDate.getMonth(), updateDate.getDate() + 7, updateDate.getHours(), updateDate.getMinutes(), updateDate.getSeconds());
                        timeLeft = ReturnTimeLeft(unlockDate, updateDate);
                        TextView tv = (TextView)view.findViewById(R.id.timeLeft2);
                        tv.setText(timeLeft);
                        if (timeLeft != "לא הוגש") {
                            ImageButton b2;
                            b2 = (ImageButton) view.findViewById(R.id.imageButtonWeek1);
                            b2.setEnabled(false);
                        }

                        //daysLeft=unlockDate.getDate()-updateDate.getDate();


                        mouth2.put("updateDate", updateDate);
                        mouth2.put("unlockDate", unlockDate);
                        mouth2.put("status", "לא הוגש");
                        mouth2.put("sequenceNumber", 2);
                        unlockDate = new Date(updateDate.getYear(), updateDate.getMonth(), updateDate.getDate() + 14, updateDate.getHours(), updateDate.getMinutes(), updateDate.getSeconds());
                        timeLeft = ReturnTimeLeft(unlockDate, updateDate);
                        TextView tz = (TextView) view.findViewById(R.id.timeLeft3);
                        tz.setText(timeLeft);
                        if (timeLeft != "לא הוגש") {
                            ImageButton b3;
                            b3 = (ImageButton)view.findViewById(R.id.imageButtonWeek2);
                            b3.setEnabled(false);
                        }
                        mouth3.put("updateDate", updateDate);
                        mouth3.put("unlockDate", unlockDate);
                        mouth3.put("status", "לא הוגש");
                        mouth3.put("sequenceNumber", 3);
                        unlockDate = new Date(updateDate.getYear(), updateDate.getMonth(), updateDate.getDate() + 21, updateDate.getHours(), updateDate.getMinutes(), updateDate.getSeconds());
                        timeLeft = ReturnTimeLeft(unlockDate, updateDate);
                        TextView ts = (TextView) view.findViewById(R.id.timeLeft4);
                        ts.setText(timeLeft);
                        if (timeLeft != "לא הוגש") {
                            ImageButton b4;
                            b4 = (ImageButton) view.findViewById(R.id.imageButtonWeek3);
                            b4.setEnabled(false);
                        }

                        Log.e(" daysleft", timeLeft);


                        mouth4.put("updateDate", updateDate);
                        mouth4.put("unlockDate", unlockDate);
                        mouth4.put("status", "לא הוגש");
                        mouth4.put("sequenceNumber", 4);
                        unlockDate = new Date(updateDate.getYear(), updateDate.getMonth(), updateDate.getDate() + 28, updateDate.getHours(), updateDate.getMinutes(), updateDate.getSeconds());
                        timeLeft = ReturnTimeLeft(unlockDate, updateDate);
                        TextView tg = (TextView) view.findViewById(R.id.timeLeft5);
                        tg.setText(timeLeft);
                        if (timeLeft != "לא הוגש") {
                            ImageButton b5;
                            b5 = (ImageButton)view.findViewById(R.id.imageButtonWeek4);
                            b5.setEnabled(false);
                        }


                        mouth5.put("updateDate", updateDate);
                        mouth5.put("unlockDate", unlockDate);
                        mouth5.put("status", "לא הוגש");
                        mouth5.put("sequenceNumber", 5);


                        Log.e(" update date", String.valueOf(updateDate.getDate()));
                        mouthReviewSubmissions1.put("mouth0", mouth1);
                        mouthReviewSubmissions1.put("mouth1", mouth2);
                        mouthReviewSubmissions1.put("mouth2", mouth3);
                        mouthReviewSubmissions1.put("mouth3", mouth4);
                        mouthReviewSubmissions1.put("mouth4", mouth5);

                        mouthReviewSubmissions.put("mouthReviewSubmissions", mouthReviewSubmissions1);


                        db.collection("users-data").document(user_guidf).update(mouthReviewSubmissions)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.e(" successfully written!", "o");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.e("Error writing document", "o");
                                    }
                                });


                    }


                    // Log.i("LOGGER","second "+document.getString("role"));

                } else {
                    Log.d("LOGGER", "No such document");
                }
            } else {
                Log.d("LOGGER", "get failed with ", task.getException());
            }
        }
    });
}
        return view;


    }


    public String ReturnTimeLeft(Date finalDate, Date firstDate) {

        long different = finalDate.getTime() - firstDate.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;
        if (elapsedDays >= 7) {
            long elapsedWeeks = elapsedDays / 7;
            long elaspedDays2 = elapsedDays - (elapsedWeeks * 7);
            return ("נותר," + elapsedWeeks + " שבועות, " + elaspedDays2 + " ימים, " + elapsedHours + " שעות, " + elapsedMinutes + " דקות");
        }
        if (elapsedDays <= 0 && elapsedHours <= 0 && elapsedMinutes <= 0) {
            return ("לא הוגש");

        } else if (elapsedDays == 0 && elapsedHours == 0) {
            return ("נותר," + elapsedMinutes + " דקות");

        } else if (elapsedDays == 0) {
            return ("נותר," + elapsedHours + " שעות, " + elapsedMinutes + " דקות");


        }


        return ("נותר," + elapsedDays + " ימים " + elapsedHours + " שעות, " + elapsedMinutes + " דקות");

        //Toast.makeText(Treatment_progress.this, elapsedDays + " " + elapsedHours + " " + elapsedMinutes + " " + elapsedSeconds, Toast.LENGTH_SHORT).show();

    }




}