package com.igam.shira.myapplication;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class Scribbler extends Activity {
    private static final int PICK_FROM_FILE = 1;
    private static final int CAMERA_CAPTURE = 2;
    private EditText checkResult;
    private String check_result;

    private  Map<String, Object> binaryImageUpload = new HashMap<>();
    private  Map<String, Object> binaryImageUploadForWeek = new HashMap<>();
    private  Map<String, Object> rateMap = new HashMap<>();
    private  Map<String, Object> urlMap = new HashMap<>();
    private  Map<String, Object> sumery = new HashMap<>();
    private  Map<String, Object> polygon0 = new HashMap<>();
    private  Map<String, Object> polygon1 = new HashMap<>();
    private  Map<String, Object> polygon2 = new HashMap<>();
    private  Map<String, Object> polygon3 = new HashMap<>();
    private  Map<String, Object> polygon4 = new HashMap<>();
    private  Map<String, Object> polygon5 = new HashMap<>();
    private List<Upload> MapList=new ArrayList<>();
    private Uri mImageCaptureUri;
    private boolean newPolygon=false;
    private LinearLayout rl;
    private FirebaseFirestore mDatabase;
    private Bitmap bmp1;
    private ScrollView s;
    private Button submit_btn;
    Canvas canvas;
    int j=0;
    Bitmap scaledBitmap;
    int count_of_images=0;
    int i;
    Bitmap image1,tmp;
    DrawImage pic;
    private   List<Paths> paths1=new ArrayList<Paths>();
    private ImageView drawImageView;
    int leftX, rightX, topY, bottomY;
    private List<File> binary_pictures=new ArrayList<>();
    private List<String> rate_number=new ArrayList<>();
    private List<String> checkResults=new ArrayList<>();
    Bitmap image, caniImage;
    List<Point> points = new ArrayList<Point>();
    private Map<String, Object> [] listMap;
    Paint paint = new Paint();
    // private Uri mImageCaptureUri;
//    private ImageView  drawImageView1=(ImageView)findViewById(R.id.DrawImageView);
    private String path;
    private Bitmap sourceBitmap;
    private Path clipPath;
    boolean rateb;
    String rate_string;
    Bitmap myRoundedImage,myRoundedImage1;
    // LinearLayout rl;
    private Bitmap bitmap;
    boolean clear=false;

    // private static final int PICK_FROM_FILE = 1;
    //private static final int CAMERA_CAPTURE = 2;

    //  Polygon.Builder polygon;
    Region r;
    Bitmap  imageScaled;
    Polygon p;
    Polygon polygon;
    Bitmap bmp;
    Bitmap alteredBitmap;
    android.graphics.Point point;
    // Canvas canvas;
    private Spinner rating;
    private String gUid,weekNum;
    private String user_guidf;
    Matrix matrix;
    float downx = 0;
    private Mat mIntermediateMat;

    Point p1=null, p2=null;
    float downy = 0;
    float upx = 0;
    int drag_point=-1;
    float upy = 0;
    boolean valid=true;
    public static int num_of_path=0;
    public static int crop_num=7;
    // public static Paths []paths=new Paths[crop_num];
    // private List<Paths> pathes1 = new ArrayList<Paths>();
    boolean flgPathDraw = true,touch_in_point=false,f=false,add_polygon_flag=false,touch_in_first_point=false;

    // Bitmap  bitmap1 = BitmapFactory.decodeReso urce(getResources(),
    //  R.drawable.website);



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        drawView.setBackgroundColor(Color.WHITE);

        //setContentView(drawView,lp);
        setContentView(R.layout.activity_scribbler);
        rl = (LinearLayout) findViewById(R.id.lll);
        s=(ScrollView)findViewById(R.id.sss);
        // myview.layout(50,50,50,50);

        OpenCVLoader.initDebug();


        Paths p=new Paths();
        paths1.add(p);
        drawImageView=(ImageView)findViewById(R.id.DrawImageView) ;
        checkResult=(EditText)findViewById(R.id.check_result);
        gUid= getIntent().getStringExtra("gUid");
        weekNum=getIntent().getStringExtra("weeknum");
        binaryImageUploadForWeek.put("binary_images_for week"+weekNum,binaryImageUpload);
        user_guidf=getIntent().getStringExtra("user_gUid");
        path=getIntent().getStringExtra("Url");
        Log.d("path print",path);
        pic = new DrawImage(this);
        //   myRoundedImage = pic.getDrawingCache();

        // if(bmp1!=null) {
        rating = (Spinner) findViewById(R.id.rate);
        mDatabase=FirebaseFirestore.getInstance();
        pic.setAdjustViewBounds(true);
        // submit_btn=(Button)findViewById(R.id.submmitbtn) ;
        Button caniButton 	= (Button) findViewById(R.id.cani);
        Button secButton 	= (Button) findViewById(R.id.section);

        Button saveButton 	= (Button) findViewById(R.id.btn_save);
        saveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                rate_string = rating.getSelectedItem().toString();
                check_result = checkResult.getText().toString();
                // pic.setImageBitmap(bmp1);


                myRoundedImage = pic.getDrawingCache();
                myRoundedImage1 = myRoundedImage.copy(Bitmap.Config.ARGB_8888, true);
                if (paths1.get(num_of_path).getPoints().size() > 0) {
                    Toast.makeText(Scribbler.this, "press on add button for add polygon",
                            Toast.LENGTH_SHORT).show();
                }
                // pic.setDrawingCacheEnabled(false);
                else
                {
                    if (validation() && newPolygon) {
                        rate_number.add(rate_string);
                        checkResults.add(check_result);
                        checkResult.setText(" ");
                        rating.setSelection(0);
                        newPolygon = false;

                    } else
                        Toast.makeText(Scribbler.this, "נא הכנס פרטים",
                                Toast.LENGTH_SHORT).show();
                    if (!newPolygon) {


                        // clear();
                        pic.invalidate();
                        myRoundedImage = pic.getDrawingCache();
                        for (i = 0; i < num_of_path; i++) {
                            save_image(i);
                        }
                        Log.d("submit bottun", "submit press");
                        //submmit_images();
                        for (i = 0; i < binary_pictures.size(); i++) {
                            File pictureFile = binary_pictures.get(i);
                            String srate = rate_number.get(i);

                            String result = checkResults.get(i);

                            Uri uri = Uri.fromFile(pictureFile);
                            submmit_images(uri, srate, result);
                            if (i == binary_pictures.size() - 1) {
                                Log.d("firestore uplouding", "firestore");
                                uploadToFirestore();
                            }

                        }
                    }
                }
            }

        });
        caniButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {



                Mat srcMat = new Mat ( bmp1.getHeight(), bmp1.getWidth(), CvType.CV_8UC3);
                Bitmap myBitmap32 = bmp.copy(Bitmap.Config.ARGB_8888, true);

                Utils.bitmapToMat(myBitmap32, srcMat);

                Mat gray = new Mat(srcMat.size(), CvType.CV_8UC1);
                Imgproc.cvtColor(srcMat, gray, Imgproc.COLOR_RGB2GRAY);
                Mat edge = new Mat();
                Mat dst = new Mat();
                Imgproc.Canny(gray, edge, 80, 90);
                Imgproc.cvtColor(edge, dst, Imgproc.COLOR_GRAY2RGBA,4);
                Bitmap resultBitmap = Bitmap.createBitmap(dst.cols(), dst.rows(),Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(dst, resultBitmap);
                pic.setImageBitmap(resultBitmap);



            }

        });
        secButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                bmp1 = pic.getDrawingCache();
                Bitmap copy_bitmap  = bmp1.copy( Bitmap.Config.ARGB_8888 , true);
                for (int x = 0; x < bmp1.getWidth(); x++) {
                    for (int y = 0; y < bmp1.getHeight(); y++) {
                        copy_bitmap.setPixel(x, y, Color.rgb(255, 255, 255));

                    }
                    }

                double y1, x1,b,xMax, xMin,yMax, yMin;
                double a;
                if(p1.x>p2.x) {
                    xMax = p1.x;
                    xMin=p2.x;
                }
                else
                {
                    xMax = p2.x;
                    xMin=p1.x;
                }
                if(p1.y>p2.y) {
                    yMax = p1.y;
                    yMin=p2.y;
                }
                else
                {
                    yMax = p2.y;
                    yMin=p1.y;
                }
                Log.d("p1.x", String.valueOf(p1.x));
                Log.d("p1.y", String.valueOf(p1.y));
                Log.d("p2.x", String.valueOf(p2.x));
                Log.d("p2.y", String.valueOf(p2.y));
                double k=p2.y-p1.y;
                double l=p2.x-p1.x;
                Log.d("k", String.valueOf(k));
                Log.d("l", String.valueOf(l));
                a=k/l;

                Log.d("a", String.valueOf(a));
                b=p2.y-(a*p2.x);
                Log.d("b", String.valueOf(b));
                for (int x = 0; x < bmp1.getWidth(); x++) {
                    for (int y = 0; y < bmp1.getHeight(); y++) {
                        //com.igam.shira.myapplication.Point point=new com.igam.shira.myapplication.Point(x,y);

                        if ((y<=a*x+b+0.2)&&(y>=a*x+b-0.2)&&y<=yMax&&y>=yMin&&x<=xMax&&x>=xMin) {
                            // Log.d("draw in polygon","draw in polygon");

                             Log.d("point33", String.valueOf(x));
                            Log.d("point44", String.valueOf(y));

                            copy_bitmap.setPixel(x, y, Color.rgb(0, 0, 0));
                        }
                        else
                            copy_bitmap.setPixel(x, y, Color.rgb(255, 255, 255));

                      /* if((p1.x==x&&p1.y==y)||(p2.x==x&&p2.y==y))
                       {
                           copy_bitmap.setPixel(x, y, Color.rgb(0, 0, 0));
                           Log.d("pppp", "llll");
                           Log.d("p1.x", String.valueOf(p1.x));
                           Log.d("p1.y", String.valueOf(p1.y));
                           Log.d("p2.x", String.valueOf(p2.x));
                           Log.d("p2.y", String.valueOf(p2.y));
                           Log.d("x", String.valueOf(x));
                           Log.d("y", String.valueOf(y));
                       }
                       else
                           copy_bitmap.setPixel(x, y, Color.rgb(255, 255, 255));*/



                    }
                }
                MediaStore.Images.Media.insertImage(getContentResolver(), copy_bitmap, "image" , "none");
                pic.invalidate();
                pic.setImageBitmap(copy_bitmap);


            }

        });

        /*Display display = getWindowManager().getDefaultDisplay();
        int displayWidth = display.getWidth();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(), R.drawable.website, options);
        int width = options.outWidth;
        if (width > displayWidth) {
            int widthRatio = Math.round((float) width / (float) displayWidth);
            options.inSampleSize = widthRatio;
        }
        options.inJustDecodeBounds = false;
        Bitmap scaledBitmap =  BitmapFactory.decodeResource(getResources(),R.drawable.website, options);
        drawImageView.setImageBitmap(scaledBitmap);*/

        Button discardButton 	= (Button) findViewById(R.id.btn_discard);


        discardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  if (mImageCaptureUri != null) {
                    //Now deleting the temporary file created on camera capture
                    File f = new File(mImageCaptureUri.getPath());
                    if (f.exists()) {
                        f.delete();
                    }

                    getContentResolver().delete(mImageCaptureUri, null, null );
                    mImageCaptureUri = null;
                    //resetting the image view
                    ImageView iv = (ImageView) findViewById(R.id.DrawImageView);
                    iv.setImageDrawable(null);*/
                Log.d("discard","discard button");
                Log.d("before num of path", String.valueOf(num_of_path));

                // if (num_of_path>0)
                //  num_of_path=num_of_path-1;
                if( paths1.get(num_of_path).getPoints().size()!=0)
                {
                    paths1.get(num_of_path).getPoints().clear();
                    paths1.get(num_of_path).setPath(new Path());
                    paths1.get(num_of_path).setMfirstpoint(null);
                    paths1.get(num_of_path).setBfirstpoint(false);

                }
                else {
                    if (num_of_path > 0)
                        num_of_path = num_of_path - 1;
                    paths1.get(num_of_path).getPoints().clear();
                    Log.d("points size", String.valueOf(paths1.get(num_of_path).getPoints().size()));


                    paths1.get(num_of_path).setPath(new Path());
                    paths1.get(num_of_path).setMfirstpoint(null);
                    paths1.get(num_of_path).setBfirstpoint(false);


                }                Log.d("after num of path", String.valueOf(num_of_path));

                pic.invalidate();

            }
        });








       /* submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("submit bottun","submit press");
                //submmit_images();
                for(   i=0;i<binary_pictures.size();i++) {
                    File pictureFile = binary_pictures.get(i);
                    String srate=rate_number.get(i);

                    Uri uri = Uri.fromFile(pictureFile);
                    submmit_images(uri,srate);
                    if(i==binary_pictures.size()-1) {
                        Log.d("firestore uplouding","firestore");
                        uploadToFirestore();
                    }

                }

            }
        });*/
















        final String [] items = new String [] {"Take from camera", "Select from gallery"};
        ArrayAdapter<String> adapter= new ArrayAdapter<String> (this, android.R.layout.select_dialog_item,items);
        AlertDialog.Builder builder	= new AlertDialog.Builder(this);

        builder.setTitle("Select Image");
        builder.setAdapter( adapter, new DialogInterface.OnClickListener() {
            public void onClick( DialogInterface dialog, int item ) { //pick from camera
                if (item == 0) {
                    try {
                        //use standard intent to capture an image
                        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        //we will handle the returned data in onActivityResult
                        startActivityForResult(captureIntent, CAMERA_CAPTURE);
                    } catch(ActivityNotFoundException e){
                        e.printStackTrace();
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                } else { //pick from file
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);
                }
            }
        } );



        Button addPolygon=(Button) findViewById(R.id.add_polygon_id);


        addPolygon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rate_string= rating.getSelectedItem().toString();
                check_result=checkResult.getText().toString();

                // if(validation()) {
                //   rate_number.add(rate_string);
                //   checkResults.add(check_result);
                //   checkResult.setText(" ");
                //   rating.setSelection(0);
                if (paths1.get(num_of_path).getPoints().size() != 0) {
                    paths1.get(num_of_path).setMlastpoint(point);
                    build_path();
                    //  paths[num_of_path].getPath().close();
                    // rating();
                    // save_image();
                    // num_of_path++;
                    //  add_polygon_flag = true;
                    // myview.invalidate();
                }
                // }
                // else
                //Toast.makeText(Scribbler.this, "נא הכנס פרטים",
                //  Toast.LENGTH_SHORT).show();

            }

        });

        final AlertDialog dialog = builder.create();
        Button rotateButton=(Button)findViewById(R.id.btn_rotate) ;

        rotateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //drawImageView.setRotation(drawImageView.getRotation() + 90);
                pic.setRotation(pic.getRotation() + 90);
                //pic.invalidate();

            }

        });










        new Thread(new Runnable() {
            public void run() {
                mImageCaptureUri= loadImageFromNetwork();

                drawImageView.post(new Runnable() {
                    public void run() {

                        // alteredBitmap=functiomm();

                        //    drawImageView.setImageBitmap(bmp);

                        //  doCrop();

                        //drawImageView.invalidate();
                        //functiomm();


                        fun();



                    }
                });
            }
        }).start();




    }
    private void clear()
    {
        for (i = 0; i <= num_of_path; i++) {
            if( paths1.get(num_of_path).getPoints().size()!=0)
            {
                paths1.get(num_of_path).getPoints().clear();
                paths1.get(num_of_path).setPath(new Path());
                paths1.get(num_of_path).setMfirstpoint(null);
                paths1.get(num_of_path).setBfirstpoint(false);

            }        }




    }

    private void submmit_images(final Uri imageUri, final String srate,final String result) {
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        StorageReference storageReferenceProfilePic = firebaseStorage.getReference();
        StorageReference imageRef = storageReferenceProfilePic.child("binary_images").child(user_guidf).child(weekNum).child("IMG__userID_" + user_guidf + "_week_"+ weekNum +"_" + new Date().getTime()+".png");

        imageRef.putFile(imageUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        //if the upload is successful
                        //hiding the progress dialog
                        //and displaying a success toast
                        // mProgress.setMessage("Uploading Image...");
                        //mProgress.show();
                        if(taskSnapshot!=null) {

                            Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                            while (!urlTask.isSuccessful()) ;
                            Uri downloadUrl = urlTask.getResult();


                            final String sdownload_url = String.valueOf(downloadUrl);
                            Log.d("uriii", sdownload_url);
                            Upload upload1 = new Upload( sdownload_url,String.valueOf(srate) );
                            //rateMap.put("rate"+count_of_images,srate);
                            // urlMap.put("url"+count_of_images,sdownload_url);
                            // Paths []paths=new Paths[crop_num];
                            listMap=new HashMap[num_of_path];
                            sumery.putAll(rateMap);
                            sumery.putAll(urlMap);
                            listMap[count_of_images]=new  HashMap<>();
                            listMap[count_of_images].put("rate",srate);
                            listMap[count_of_images].put("url",sdownload_url);
                            listMap[count_of_images].put("check result:",result);

                            binaryImageUpload.put(String.valueOf(count_of_images+1),listMap[count_of_images]);


                            uploadToFirestore();


                            count_of_images++;
                        }





                        Toast.makeText(Scribbler.this, "succed uploading file... ", Toast.LENGTH_LONG).show();

                        //String profilePicUrl = taskSnapshot.getDownloadUrl().toString();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        //if the upload is not successful
                        //hiding the progress dialog
                        // mProgress.dismiss();
                        Toast.makeText(Scribbler.this, "Error uploading file... ", Toast.LENGTH_LONG).show();
                        //and displaying error message
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        //calculating progress percentage
//                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
//                        //displaying percentage in progress dialog
//                        progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                    }
                });

    }

    private void save_image(int numOfPolygon) {
        //rate_string= rating.getSelectedItem().toString();
        //   if (validation()) {

        try {
            String path = Environment.getExternalStorageDirectory().toString();
            OutputStream fOut = null;
            File file = new File(path, String.valueOf(Math.round(Math.random() * 100000)) + ".png");
            Log.d("vvvvvv", file.getName());
            fOut = new FileOutputStream(file);
            // cropImageByPath();
            //alertedbitmap=true;
            Bitmap copy_bitmap  = bmp1.copy( Bitmap.Config.ARGB_8888 , true);
            // clear();
            //   pic.invalidate();
            clear=true;
            pic.invalidate();
            myRoundedImage = pic.getDrawingCache();

            bmp1 = Binary(bmp1,numOfPolygon);
            // canvas.drawBitmap(bmp1, 0, 0, paint);
            bmp1.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();

            //MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            AlertDialog.Builder builder = new AlertDialog.Builder(Scribbler.this);
            builder.setMessage("Saved");
            AlertDialog dialog = builder.create();
            dialog.show();

            //Now deleting the temporary file created on camera capture
            File f = new File(mImageCaptureUri.getPath());
            //binaryImageUpload.put(rate_string, f);
            binary_pictures.add(file);
            // rate_number.add(rate_string);
             /*   if (f.exists()) {
                    f.delete();
                }*/


        } catch (Exception e) {
            e.printStackTrace();
        }
        //}
        //  else
        // Toast.makeText(Scribbler.this, "נא הכנס פרטים",
        // Toast.LENGTH_SHORT).show();
    }



    public void canifunc(){
        CameraBridgeViewBase.CvCameraViewFrame f = null;
        Mat rgbaInnerWindow;
        Mat rgba = f.rgba();
        Size sizeRgba = rgba.size();


        rgbaInnerWindow = rgba.submat(0, 1000, 0, 500);
        Imgproc.Canny(rgbaInnerWindow, mIntermediateMat, 80, 90);
        Imgproc.cvtColor(mIntermediateMat, rgbaInnerWindow,
                Imgproc.COLOR_GRAY2BGRA, 4);
        rgbaInnerWindow.release();

    }

    private boolean validation() {


        if( (rate_string==null||rate_string.isEmpty()))
        {
            return false;
        }
        return true;
    }

    private void rating() {


    }

    private void fun() {


        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        try {
            bmp1 = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                    mImageCaptureUri), null, bmpFactoryOptions);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        bmpFactoryOptions.inJustDecodeBounds = false;
        try {
            bmp1 = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                    mImageCaptureUri), null, bmpFactoryOptions);


            Display display = getWindowManager().getDefaultDisplay();
            int displayWidth = display.getWidth();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            // BitmapFactory.decodeResource(getResources(), R.drawable.website, options);
            int width = options.outWidth;
            if (width > displayWidth) {
                int widthRatio = Math.round((float) width / (float) displayWidth);
                options.inSampleSize = widthRatio;
            }
            options.inJustDecodeBounds = false;
            scaledBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                    mImageCaptureUri), null, options);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }



        //rl.addView(drawView);
        //  DrawImage pic = findViewById(R.id.draw);
        bmp1 = bmp1.copy( Bitmap.Config.ARGB_8888 , true);
        //  pic.setBackgroundColor(Color.YELLOW);
        // pic.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        drawImageView.setBackgroundColor(Color.BLACK);
        pic.setScaleType(ImageView.ScaleType.FIT_XY);
        pic.setDrawingCacheEnabled(true);

        //   myRoundedImage = Bitmap.createBitmap(
        //  bmp1.getWidth(), bmp1.getHeight(), Bitmap.Config.ARGB_8888 );
        //   canvas = new Canvas(  );
        //    canvas.drawBitmap(bmp1, 0, 0, paint);
        //  pic.draw( canvas );
        pic.setImageBitmap(bmp1);


        //pic.invalidate();
        pic.setScaleType(ImageView.ScaleType.FIT_XY);
        pic.setAdjustViewBounds(true);

        // measured();

        final LinearLayout.MarginLayoutParams params = (LinearLayout.MarginLayoutParams) pic.getLayoutParams();
        // params.bottomMargin=50;
        //  pic.setLayoutParams(params);
        // myview.setLeft(300);
        //  myview.setRight(300);
        // pic.setTop(300);
        //  pic.setY(0);
        //  params.topMargin += 600;
        //  params.bottomMargin+=600;
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        //  setPic(Uri.parse(path));
        // s.addView(pic);
        //  pic.setWidth(500);

        rl.addView(pic);
        //  s.addView(pic);







        /*Display display = getWindowManager().getDefaultDisplay();
        int displayWidth = display.getWidth();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(), R.drawable.website, options);
        int width = options.outWidth;
        if (width > displayWidth) {
            int widthRatio = Math.round((float) width / (float) displayWidth);
            options.inSampleSize = widthRatio;
        }
        options.inJustDecodeBounds = false;
        Bitmap scaledBitmap =  BitmapFactory.decodeResource(getResources(),R.drawable.website, options);
        drawImageView.setImageBitmap(scaledBitmap);*/




    }



    public void uploadToFirestore()

    {
        Log.d("firestore uplouding","firestore");
        //  binaryImageUploadForWeek.put("binary_images_for week"+weekNum,MapList);
        // binaryImageUploadForWeek.put("binary_images_for week"+weekNum,MapList);

        mDatabase.collection("users-data").document(user_guidf).update(binaryImageUploadForWeek)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e(" successfully written!", "o");



                        // mProgress.show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("Error writing document", "o");
                    }
                }).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });





    }



    private void add_point_to_path(android.graphics.Point point) {
        check_new_polygon();
        if (paths1.get(num_of_path).isBfirstpoint()) {

          /*  if (comparepoint(paths[num_of_path].getMfirstpoint(), point)) {
                /// points.add(point);
                paths[num_of_path].getPoints().add(paths[num_of_path].getMfirstpoint());
                //*flgPathDraw = false;
                //  drawImageView.invalidate();
                // / showcropdialog();
                // / cropflag=true;
            } else {
                paths[num_of_path].getPoints().add(point);
                //  drawImageView.invalidate();
            }*/
            paths1.get(num_of_path).getPoints().add(point);
            // paths[num_of_path].getPath().lineTo(point.x,point.y);
        }
        //else {
        //  paths[num_of_path].getPoints().add(point);
        //drawImageView.invalidate();
        // }

        if (!(paths1.get(num_of_path).isBfirstpoint())) {

            paths1.get(num_of_path).setMfirstpoint(point);
            // paths[num_of_path].getPath().moveTo(point.x,point.y);
            paths1.get(num_of_path).getPoints().add(point);
            paths1.get(num_of_path).setBfirstpoint(true);
            //drawImageView.invalidate();

        }
    }

    private void check_new_polygon() {



    }


    public void fun1(int numOfPolygon)
    {
        // polygon = Polygon.Builder();




      /*  for (int i = 0; i < paths[0].getPoints().size(); i++) {
            polygon = Polygon.Builder().
        addVertex(new com.shirael.project1.Point(paths[0].getPoints().get(i).x, MainActivity.paths[0].getPoints().get(i).y)).build();
        }*/




       /* Polygon.Builder poly2 = new Polygon.Builder();


        for (int i = 0; i < paths[0].getPoints().size(); i++) {
            poly2.addVertex(new com.shirael.project1.Point(paths[0].getPoints().get(i).x, MainActivity.paths[0].getPoints().get(i).y) );

            Polygon polygon2 = poly2.build();*/
        Polygon.Builder poly2=new Polygon.Builder();
        //   if(numOfPolygon>0) {
        for (int i = 0; i < paths1.get(numOfPolygon).getPoints().size(); i++) {
            Log.d("num_of_path", String.valueOf(numOfPolygon ));
            poly2.addVertex((new com.igam.shira.myapplication.Point(paths1.get(numOfPolygon).getPoints().get(i).x, paths1.get(numOfPolygon).getPoints().get(i).y)));
            Log.d("Binary function*~~~>>>>", "Binary function");
            Log.e("Hi  ==>", "Size: " + paths1.get(numOfPolygon).getPoints().get(i).x + " " +paths1.get(numOfPolygon).getPoints().get(i).y);

        }
        // }
        //  if(numOfPolygon>0) {
        polygon = poly2.build();
        //  }

    }


    public Bitmap Binary(Bitmap bmpOriginal,int numOfPolygon)
    {
        int width, height, threshold;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();
        threshold = 127;
        Bitmap bmpBinary = Bitmap.createBitmap(bmpOriginal);
        fun1(numOfPolygon);

        for (int x = 0; x < myRoundedImage.getWidth(); x++) {
            for (int y = 0; y < myRoundedImage.getHeight(); y++) {
                com.igam.shira.myapplication.Point point=new com.igam.shira.myapplication.Point(x,y);

              /*  if(r.contains((int)point.x,(int) point.y)) {
                    Log.d(TAG, "Touch IN");
                    bmpBinary.setPixel(x, y, Color.rgb(255, 255, 255));
                }
                else {
                    Log.d(TAG, "Touch OUT");
                    bmpBinary.setPixel(x, y, Color.rgb(0, 0, 0));
                }*/

                //if(numOfPolygon>0) {
                if (polygon.contains(point)) {
                    // Log.d("draw in polygon","draw in polygon");
                    myRoundedImage.setPixel(x, y, Color.rgb(255, 255, 255));
                }
                else
                    myRoundedImage.setPixel(x, y, Color.rgb(0, 0, 0));




            }
        }

        return myRoundedImage;
    }


    private boolean check_touch_in_point(android.graphics.Point point) {
        Log.d("check touch*~~~~~~~>>>>", "check touch");
        touch_in_point=false;


        for (int i = 0; i < paths1.get(num_of_path).getPoints().size(); i++) {
            Point p1 = new Point();
            p1 =paths1.get(num_of_path).getPoints().get(i);
            Log.e("point  ==>", "Size: " + point.x + " " + point.y);
            Log.e("p1  ==>", "Size: " + p1.x + " " + p1.y);


            double  distanceFromCenter = Math.sqrt(((p1.x - point.x) * (p1.x - point.x)) + ((p1.y - point.y) * (p1.y - point.y)));
            Log.e("distanceFromCenter  ==>", String.valueOf(distanceFromCenter));

            if (Math.abs(distanceFromCenter) < 30) {
                Log.e("exax  ==>", String.valueOf(Math.sqrt(3 ^ 2)));
                Log.e("exax -9 ==>", String.valueOf(Math.abs(-9)));

                Log.e("distanceFromCenter  ==>", String.valueOf(distanceFromCenter));

                //Touch within circle...
                Log.d(" touch in point", "touch in point");
                Log.e("point  ==>", "Size: " + point.x + " " + point.y);
                Log.e("p1  ==>", "Size: " + p1.x + " " + p1.y);


                touch_in_point = true;
                drag_point = i;
                break;


            }

        }

        // myview.invalidate();
        Log.d("check touch*~~~~~~~>>>>", String.valueOf(touch_in_point));

        return touch_in_point;
    }
    private void check_if_polygon() {


        if(paths1.get(num_of_path).getPoints().size()>2)
        {
            build_path();
        }


    }

    private void check_touch_in_first_point(android.graphics.Point point_d) {

        if(paths1.get(num_of_path).getMfirstpoint()!=null) {
            Log.e("first point  ==>", "Size: " + paths1.get(num_of_path).getMfirstpoint() + " " + paths1.get(num_of_path).getMfirstpoint());

            double distanceFromCenter = Math.sqrt(((paths1.get(num_of_path).getMfirstpoint().x - point_d.x) * (paths1.get(num_of_path).getMfirstpoint().x - point_d.x)) + ((paths1.get(num_of_path).getMfirstpoint().y - point_d.y) * (paths1.get(num_of_path).getMfirstpoint().y - point_d.y)));

            if (distanceFromCenter < 30) {
                Log.e("p1  ==>", "first point touch");
                touch_in_first_point=true;

                check_if_polygon();

            }
        }

    }



    private boolean drag(android.graphics.Point point) {
        if(touch_in_point&&drag_point!=-1)
        {
            Log.d("drag action","drag action");

            paths1.get(num_of_path).getPoints().set(drag_point,point);
            // paths[num_of_path].getPoints().remove(paths[num_of_path].getPoints().get(drag_point));
            // paths[num_of_path].getPoints().add(point);
          /*  for(int i=0;i<paths[num_of_path].getPoints().size();i++)
            {
                Point p=paths[num_of_path].getPoints().get(i);
                if(p==point)
                {
                    drag_point=i;
                    break;

                }
            }*/
            // onDraw(canvas);

        }

        return touch_in_point;

    }


    public void measured()

    {
        pic.getTop();
        int imageViewH=pic.getMeasuredHeight();//height of imageView
        int imageViewW =pic.getMeasuredWidth();//width of imageView
        int drawableH =pic.getDrawable().getIntrinsicHeight();//original height of underlying image
        int drawableW=pic.getDrawable().getIntrinsicWidth();//original width of underlying image
        int displayH, displayW;  // the shown height and width of the picture.
        ; // the shown edges of the picture.
        if (imageViewH/drawableH <= imageViewW/drawableW){
            displayW = drawableW*imageViewH/drawableH;//rescaled width of image within ImageView
            displayH = imageViewH;
            leftX = (imageViewW - displayW)/2; // left edge of the displayed image.
            rightX = leftX + displayW; // right edge of the displayed image.
            topY = 0; // top edg
            bottomY = displayH; // bottom edge.
        }else{
            displayH = drawableH*imageViewW/drawableH;//rescaled height of image within ImageView
            displayW = imageViewW;
            leftX = 0; // left edge of the displayed image.
            rightX = displayW; // right edge of the displayed image.
            topY = (imageViewH - displayH)/2; // top edg
            bottomY = topY + displayH; // bottom edge.
        }
        //TODO: clamp the eventX and eventY to the bound of leftX, rightX, topY, bottomY
    }





    private void setPic( Uri resultUri) {

        drawImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        drawImageView = (ImageView) findViewById(R.id.DrawImageView);
        Glide
                .with(getBaseContext())
                .load(resultUri)
                .into(drawImageView);
    }



    private boolean build_path()
    {
        rate_string= rating.getSelectedItem().toString();
        check_result=checkResult.getText().toString();


        if(paths1.get(num_of_path).getPoints().size()>2) {
            newPolygon=true;

            android.graphics.Point p = new android.graphics.Point();
            p = paths1.get(num_of_path).getPoints().get(0);
            paths1.get(num_of_path).getPath().moveTo(p.x, p.y);
            for (int i = 1; i < paths1.get(num_of_path).getPoints().size(); i++) {
                p = paths1.get(num_of_path).getPoints().get(i);
                paths1.get(num_of_path).getPath().lineTo(p.x, p.y);

            }
            paths1.get(num_of_path).getPath().close();
            num_of_path++;
            Paths paths=new Paths();
            paths1.add(paths);

            add_polygon_flag = true;
            pic.invalidate();
            // touch_in_first_point=false;
            if(validation())
            {
                rate_number.add(rate_string);
                checkResults.add(check_result);
                checkResult.setText(" ");
                rating.setSelection(0);
                newPolygon=false;

            }

            return true;
        }
        else
        {
            // paths[num_of_path].getPoints().clear();
            //pic.invalidate();
            Toast.makeText(Scribbler.this, "פוליגון חייב להכיל יותר משתי נקודות!",
                    Toast.LENGTH_SHORT).show();


            //   touch_  in_first_point=false;
          /*  if(validation())
            {
                rate_number.add(rate_string);
                checkResults.add(check_result);
                checkResult.setText(" ");
                rating.setSelection(0);
                newPolygon=false;

            }*/

            return false;
        }

    }


    private Uri loadImageFromNetwork() {

        try {

            Bitmap bitmap = null;
            InputStream iStream = null;
            URL url12 = new URL(path);
            HttpURLConnection conn = (HttpURLConnection) url12.openConnection();
            conn.setReadTimeout(5000 /* milliseconds */);
            conn.setConnectTimeout(7000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            iStream = conn.getInputStream();
            bitmap = BitmapFactory.decodeStream(conn.getInputStream());
            // drawImageView.setImageBitmap(bitmap);

            File f = new File(Environment.getExternalStorageDirectory(),System.currentTimeMillis() + ".png");
            if(f.exists())
                f.delete();
            f.createNewFile();
            Bitmap bitmap1 = bitmap;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap1.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);

            byte[] bitmapdata = bos.toByteArray();
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            mImageCaptureUri = Uri.fromFile(f);
            // image=Drawable.createFromStream(iStream, mImageCaptureUri.toString());
            // drawImageView.setBackground(image);

            fos.close();





            //  BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
            //   bmpFactoryOptions.inJustDecodeBounds = true;

            // bitmap = BitmapFactory.decodeStream(iStream,null,bmpFactoryOptions);
           /* int imageWidth = bmpFactoryOptions.outWidth;
            int imageHeight = bmpFactoryOptions.outHeight;
            URL url = new URL(path); //Some instantiated URL object


            mImageCaptureUri = Uri.parse(path );*/






            // bmp = bitmap;
         /*   if(bmp!=null) {
              //  alteredBitmap = Bitmap.createBitmap(imageWidth,
                   //     imageHeight, bmp.getConfig());


                BitmapFactory.Options bmpFactoryOptions1 = new BitmapFactory.Options();
                bmpFactoryOptions1.inJustDecodeBounds = true;
                bmp1 = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                        mImageCaptureUri), null, bmpFactoryOptions1);
                bmpFactoryOptions.inJustDecodeBounds = false;
                bmp1 = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                        mImageCaptureUri), null, bmpFactoryOptions1);
                alteredBitmap = Bitmap.createBitmap(bmp1.getWidth(), bmp1
                        .getHeight(), bmp1.getConfig());

                Log.d("alerted image","gggggggggggggggggggggggggggguu");


            }*/
            iStream.close();

//            bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(
            // mImageCaptureUri), null, bmpFactoryOptions);


            Log.d("alerted image","dddddddddddddddddddddddddddddddddddddddd");




        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mImageCaptureUri;

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;
        switch (requestCode) {

            case PICK_FROM_FILE:
                mImageCaptureUri = data.getData();
                //   Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(),
                //     R.drawable.website);
                //int width = bitmap1.getWidth();
                // int height = bitmap1.getHeight();
                //   android.view.ViewGroup.LayoutParams lp = new android.view.ViewGroup.LayoutParams(width,height);//100 is width and 200 is height
                // check_point.run();


                BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                bmpFactoryOptions.inJustDecodeBounds = true;
                try {
                    bmp1 = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                            mImageCaptureUri), null, bmpFactoryOptions);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bmpFactoryOptions.inJustDecodeBounds = false;
                try {
                    bmp1 = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                            mImageCaptureUri), null, bmpFactoryOptions);





                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                tmp = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
                pic.invalidate();

                //  Bitmap bmp = Bitmap.createBitmap(949, 300, Bitmap.Config.ARGB_8888);

                // drawImageView.setImageBitmap(bmp);
                // Canvas canvas=new Canvas(bmp);



                break;

            case CAMERA_CAPTURE:
                mImageCaptureUri = data.getData();

                break;
        }
    }




    private static final int PERMISSION_REQUEST_CODE = 200;
    private boolean checkPermission() {

        return ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ;
    }

    private void requestPermissionAndContinue() {
        if (ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, WRITE_EXTERNAL_STORAGE)
                    && ActivityCompat.shouldShowRequestPermissionRationale(this, READ_EXTERNAL_STORAGE)) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                alertBuilder.setCancelable(true);
                alertBuilder.setTitle("permission_necessary");
                alertBuilder.setMessage("storage_permission_is_encessary_to_wrote_event");
                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(Scribbler.this, new String[]{WRITE_EXTERNAL_STORAGE
                                , READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                    }
                });
                AlertDialog alert = alertBuilder.create();
                alert.show();
                Log.e("", "permission denied, show dialog");
            } else {
                ActivityCompat.requestPermissions(Scribbler.this, new String[]{WRITE_EXTERNAL_STORAGE,
                        READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
            }
        } else {
            openActivity();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (permissions.length > 0 && grantResults.length > 0) {

                boolean flag = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {
                    openActivity();
                } else {
                    finish();
                }

            } else {
                finish();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void openActivity() {
        //add your further process after giving permission or to dow
        //
        // nload images from remote server.
    }






































    public class DrawImage extends AppCompatImageView {

        private int color = Color.BLACK;
        private float width = 4f;
        private List<Holder> holderList = new ArrayList<Holder>();


        private class Holder {
            Path path;
            Paint paint;

            Holder(int color, float width) {
                path = new Path();
                paint = new Paint();
                paint.setAntiAlias(true);
                paint.setStrokeWidth(width);
                paint.setColor(color);
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeJoin(Paint.Join.ROUND);
                paint.setStrokeCap(Paint.Cap.ROUND);
            }
        }

        public DrawImage(Context context) {
            super(context);
            //paths[0]=new Paths();
            // paths[1]=new Paths();
            //paths[2]=new Paths();
            // paths[3]=new Paths();
            // paths[4]=new Paths();
            // paths[5]=new Paths();


            // init();
        }

        public DrawImage(Context context, AttributeSet attrs) {
            super(context, attrs);
            // init();
        }

        public DrawImage(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            // init();
        }

        private void init() {
            holderList.add(new Holder(color, width));
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
           /* for (Holder holder : holderList) {
                canvas.drawPath(holder.path, holder.paint);
            }*/

            Bitmap tmp = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
            float[] vertices = new float[8];
            vertices[0]=0; vertices[1]=0; vertices[2]=100; vertices[3]=0;
            vertices[4]=0;vertices[5]=200; vertices[6]=100; vertices[7]=200;

            // canvas = new Canvas(bmp1);

            //drawImageView.setImageBitmap(scaledBitmap);




            //   canvas.drawBitmap(scaledBitmap, 0, 0, paint);
            // canvas.drawBitmapMesh(bmp1, 1, 1,vertices, 0, null, 0,
            //  paint);
            //  pic.draw(canvas);
            //  }
            if(!clear) {
                boolean first = true;
                Point next = null;
                Point prev = null;

                for (int i = 0; i < paths1.get(num_of_path).getPoints().size(); i ++) {
                    Point point = paths1.get(num_of_path).getPoints().get(i);
                    if (first) {
                        first = false;
                        //    paths[num_of_path].getPath().moveTo(point.x, point.y);
                    } else if (i < paths1.get(num_of_path).getPoints().size() - 1) {
                        next =paths1.get(num_of_path).getPoints().get(i + 1);
                        //  paths[num_of_path].getPath().quadTo(point.x, point.y, next.x, next.y);
                    } else {
                        //  paths[num_of_path].setMlastpoint(paths[num_of_path].getPoints().get(i));
                        //   paths[num_of_path].getPath().lineTo(point.x, point.y);
                    }
                    canvas.drawCircle(point.x, point.y, 15, paint);

                    if (i > 0) {
                        prev =paths1.get(num_of_path).getPoints().get(i - 1);

                    }

                    if (prev != null) {


                        canvas.drawLine(prev.x, prev.y, point.x, point.y, paint);
                    }
                }


                // paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                //  paint.setStyle(Paint.Style.STROKE);
                //  paint.setStrokeWidth(5);
                //  paint.setColor(Color.GREEN);
                // paint.setPathEffect(new DashPathEffect(new float[] { 10, 20 }, 0));
                for (int k = 0; k < num_of_path; k++) {
                    paint.setStyle(Paint.Style.STROKE);
                    paint.setStrokeWidth(5);

                    canvas.drawPath(paths1.get(k).getPath(), paint);
                }
                if (paint != null)
                    paint.setStyle(Paint.Style.FILL);

                //  paint.setStyle(Paint.Style.FILL_AND_STROKE);
            }

        }



        @Override
        public boolean onTouchEvent(MotionEvent event) {
           /* float eventX = event.getX();
            float eventY = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    holderList.add(new Holder(color,width));
                    holderList.get(holderList.size() - 1).path.moveTo(eventX, eventY);
                    return true;
                case MotionEvent.ACTION_MOVE:
                    holderList.get(holderList.size() - 1).path.lineTo(eventX, eventY);
                    break;
                case MotionEvent.ACTION_UP:
                    break;
                default:
                    return false;
            }

            invalidate();*/















            point = new Point();
            Point point_d = new Point();


            point.x = (int) event.getX();
            point.y = (int) event.getY();

            switch (event.getAction())
            {

                case MotionEvent.ACTION_DOWN:
                    point_d.x = (int) event.getX();
                    point_d.y = (int) event.getY();
                    if(newPolygon)
                    {
                        rate_string= rating.getSelectedItem().toString();
                        check_result=checkResult.getText().toString();

                        if(validation())
                        {
                            rate_number.add(rate_string);
                            checkResults.add(check_result);
                            checkResult.setText(" ");
                            rating.setSelection(0);
                            newPolygon=false;

                        }
                        else
                            Toast.makeText(Scribbler.this, "נא הכנס פרטים",
                                    Toast.LENGTH_SHORT).show();


                    }

                    check_touch_in_first_point(point_d);
                    f=  check_touch_in_point(point_d);
                    if(!touch_in_point&&!touch_in_first_point&&!newPolygon)
                    {
                        Log.d("Action down*~~~~~~~>>>>", "not touched");
                        Log.e("Hi  ==>", "Size: " + point_d.x + " " + point_d.y);
                        section_function(point_d);

                        add_point_to_path(point_d);
                        invalidate();



                    }
                    // check_touch_in_point(point);

                    Log.d("Action down*~~~~~~~>>>>", String.valueOf(f));





                    break;

                case MotionEvent.ACTION_MOVE:
                    Log.d("view move action", "view move action");

                    android.graphics.Point move_point=new android.graphics.Point();

                    move_point.x=(int) event.getX();
                    move_point.y = (int) event.getY();
                    if(!touch_in_point)
                        check_touch_in_point(point_d);

                    if(touch_in_point) {
                        Log.d("view move action", "view move action");

                        drag(point);
                        invalidate();
                    }
                    break;

                case MotionEvent.ACTION_UP:
                    //touch_in_first_point=false;

                    touch_in_first_point=false;

                    Log.d("Action up*~~~~~~~>>>>", "called");
                    if(flgPathDraw) {

                        if (num_of_path == crop_num - 1) {
                            flgPathDraw = false;

                        }


                        else if(touch_in_point)
                        {
                            Log.d("Action up*~~~~~~~>>>>", "touched");

                            touch_in_point = false;
                            drag_point=-1;
                        }




                        invalidate();


                    }



                    // drag_point = -1;


                /*    if (flgPathDraw) {

                     //   if(add_polygon_flag) {

                          //  if (!comparepoint(paths[num_of_path-1].getMfirstpoint(), paths[num_of_path-1].getMlastpoint())) {
                                if (num_of_path == crop_num - 1) {
                                    flgPathDraw = false;

                                }
                               // paths[num_of_path-1].getPoints().add(paths[num_of_path-1].getMfirstpoint());
                              //  add_polygon_flag=false;
                         //   }
                       // }
                    }

                      invalidate();*/

                    break;


            }



            return true;
        }






        public void resetPaths() {
            for (Holder holder : holderList) {
                holder.path.reset();
            }
            invalidate();
        }

        public void setBrushColor(int color) {
            this.color = color;
        }

        public void setWidth(float width) {
            this.width = width;
        }


    }

    private void section_function(Point point_d) {

        p1=p2;
        p2=point_d;
        Log.d("first point", String.valueOf(p1));
        Log.d("second point", String.valueOf(p2));

    }


}