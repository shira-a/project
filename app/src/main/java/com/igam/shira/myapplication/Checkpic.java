package com.igam.shira.myapplication;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Checkpic extends AppCompatActivity {

    private FirebaseStorage firebaseStorage;
    private StorageReference mStorageRef;
    private FirebaseFirestore db =FirebaseFirestore.getInstance();
    private List<String> list;
    LinearLayout linearLayout;
    TextView countOfUseres;
    ArrayAdapter adapter;
    ListView listView;
    private String sequenceNumber;
    private static final String SUM= "סך הכל:";
    private String gUid;
    private String user_gUid;
    private ProgressDialog mProgress;
    private Date date;

    int count=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkpic);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        ((TextView)getSupportActionBar().getCustomView().findViewById(R.id.tvTitle)).setText("הגשות לבדיקה");
        gUid = getIntent().getStringExtra("Uid");
        mProgress = new ProgressDialog(this);
        firebaseStorage = FirebaseStorage.getInstance();
        mStorageRef = firebaseStorage.getReference();

        list = new ArrayList<>();

        linearLayout = new LinearLayout(this);
        // setContentView(linearLayout);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        countOfUseres = (TextView)findViewById(R.id.count_of_users);
        EditText filter=(EditText)findViewById(R.id.searchFilter);

        db.collection("submission-reviews").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    String size= String.valueOf(task.getResult().size());


                    countOfUseres.setText(SUM+""+size);
                    //  linearLayout.addView(countOfUseres);
                    countOfUseres.setTextSize(20);
                    countOfUseres.setTextColor(Color.BLACK);
                    countOfUseres.setGravity(Gravity.CENTER);



                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Map<String, Object> map2 = document.getData();
                        // String s=document.getId();
                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy  hh:mm a");
                        Date date5 = (Date) map2.get("submissionDate");
                        try {
                           date = format.parse(String.valueOf(date5));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                      //  String date = format.format(Date.parse(String.valueOf(date5)));
                        String s = "\n" + (String) map2.get("userId") + "\n" + "submission number: " + (String) map2.get("sequenceNumber") + "\n" + date5 + "\n";

                        // if ((String) map2.get("status") == "ממתין לבדיקה")
                        // {
                        list.add(s);
                        Log.d("id",document.getId());
                        // display(document.getId());
                        Log.d("size", String.valueOf(list.size()));

                        // }



                    }

                    Log.d("TAG", list.toString());
                } else {
                    Log.d("TAG", "Error getting documents: ", task.getException());
                }
            }
        });
        listView=(ListView)findViewById(R.id.theList);
        adapter=new ArrayAdapter(Checkpic.this,android.R.layout.simple_list_item_1,list);
        listView.setAdapter(adapter);



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                            Log.d("the 111 user", String.valueOf(((TextView) view).getText()));
                            String path=String.valueOf(((TextView) view).getText());
                            int count = 1;
                            String[] lines = path.split("\\r?\\n");
                            String lastChar = String.valueOf(lines[2]).substring(String.valueOf(lines[2]).length() - 1);
                            user_gUid=String.valueOf(lines[1]);
                            sequenceNumber=lastChar;

                            Log.d("idnumber111", String.valueOf(lines[1]));
                            Log.d("idnumber222", String.valueOf(lines[2]));
                            Log.d("idnumber333", lastChar);


                            Intent intent = new Intent(Checkpic.this, SubmissionDetails.class);
                            intent.putExtra("user_Uid", user_gUid);
                            intent.putExtra("submission", sequenceNumber);
                            intent.putExtra("Uid", gUid);
                            Log.d("the user", String.valueOf(user_gUid));

                            startActivity(intent);


                        }
                    });




        // count_();

        filter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
}
