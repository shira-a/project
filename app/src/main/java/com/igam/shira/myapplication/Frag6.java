package com.igam.shira.myapplication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;

public class Frag6 extends Fragment {
    private  LinearLayout view;
    private FirebaseFirestore db =FirebaseFirestore.getInstance();
    private Spinner lastVisitToHygienistSpinner;
  private Spinner brushingTeethIntervalSpinner,suggestedBrushingIntervalInDaySpinner,medicalThreadUsageSpinner,mouthWashUsageSpinner,lastVisitToDentalSpinner;
    private String user_guidf,gUid,lastVisitToDentaltext,recommendedVisitIntervalToDentisttext,lastVisitToHygienisttext;
    private String brushingTeethIntervaltext,suggestedBrushingIntervalInDaytext,medicalThreadUsagetext,mouthWashUsagetext;
    private CheckBox smoke;
    private String recommendedVisitIntervalToHygienisttext,isBrushingTeethtext;
    private boolean isBrushingTeeth;
    private EditText recommendedVisitIntervalToDentist,recommendedVisitIntervalToHygienist;

    @Nullable
    @Override


    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        //  ActionBar actionBar = getActivity().getActionBar();
        //  actionBar.setTitle("פרטים");
        gUid= getArguments().getString("Uid");
        user_guidf=getArguments().getString("user_gUid");
        if(user_guidf!=null)
            Log.d("user_guidf",user_guidf);
        else
            Log.d("user_guidf","new user");


        view = (LinearLayout) inflater.inflate(R.layout.activity_oral_hygiene, container, false);

        brushingTeethIntervalSpinner=(Spinner)view.findViewById(R.id.brushingTeethInterval);
        suggestedBrushingIntervalInDaySpinner=(Spinner)view.findViewById(R.id.suggestedBrushingIntervalInDay);
        medicalThreadUsageSpinner=(Spinner)view.findViewById(R.id.medicalThreadUsage);
        mouthWashUsageSpinner=(Spinner)view.findViewById(R.id.mouthWashUsage);
        lastVisitToDentalSpinner=(Spinner)view.findViewById(R.id.lastVisitToDental);
        recommendedVisitIntervalToDentist=(EditText)view.findViewById(R.id.recommendedVisitIntervalToDentist);
        lastVisitToHygienistSpinner=(Spinner)view.findViewById(R.id.lastVisitToHygienist);
        recommendedVisitIntervalToHygienist=(EditText)view.findViewById(R.id.recommendedVisitIntervalToHygienist);
        smoke=(CheckBox)view.findViewById(R.id.isBrushingTeeth);
        if(user_guidf!=null) {

            DocumentReference docRef = db.collection("users-data").document(user_guidf);


            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            //     Log.i("LOGGER","First "+document.getString("first_entry"));
                            Map<String, Object> map =  (Map) document.get("mouthHealthForm");


                            for(Map.Entry<String, Object> entry : map.entrySet()) {
                                if (entry.getKey().equals("brushingTeethInterval")) {
                                    brushingTeethIntervaltext = entry.getValue().toString();
                                    // genderSpinner.setAdapter(gender);
                                    Log.d("brushingTeethInterval", brushingTeethIntervaltext);
                                    //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                    for (int i = 0; i < brushingTeethIntervalSpinner.getAdapter().getCount(); i++)

                                    {
                                        if (brushingTeethIntervalSpinner.getItemAtPosition(i).equals(brushingTeethIntervaltext)) {
                                            brushingTeethIntervalSpinner.setSelection(i);
                                            brushingTeethIntervalSpinner.setEnabled(false);
                                            break;
                                        }
                                    }
                                }





                                if (entry.getKey().equals("medicalThreadUsage")) {
                                    medicalThreadUsagetext = entry.getValue().toString();
                                    // genderSpinner.setAdapter(gender);
                                    Log.d("medicalThreadUsagetext", medicalThreadUsagetext);
                                    //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                    for (int i = 0; i < medicalThreadUsageSpinner.getAdapter().getCount(); i++)

                                    {
                                        if (medicalThreadUsageSpinner.getItemAtPosition(i).equals(medicalThreadUsagetext)) {
                                            medicalThreadUsageSpinner.setSelection(i);
                                            medicalThreadUsageSpinner.setEnabled(false);
                                            break;
                                        }
                                    }
                                }



                                if (entry.getKey().equals("suggestedBrushingIntervalInDay")) {
                                    suggestedBrushingIntervalInDaytext = entry.getValue().toString();
                                    // genderSpinner.setAdapter(gender);
                                    Log.d("suggestedBrushingInterv", suggestedBrushingIntervalInDaytext);
                                    //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                    for (int i = 0; i < suggestedBrushingIntervalInDaySpinner.getAdapter().getCount(); i++)

                                    {
                                        if (suggestedBrushingIntervalInDaySpinner.getItemAtPosition(i).equals(suggestedBrushingIntervalInDaytext)) {
                                            suggestedBrushingIntervalInDaySpinner.setSelection(i);
                                            suggestedBrushingIntervalInDaySpinner.setEnabled(false);
                                            break;
                                        }
                                    }
                                }






                                if (entry.getKey().equals("mouthWashUsage")) {
                                    mouthWashUsagetext = entry.getValue().toString();
                                    // genderSpinner.setAdapter(gender);
                                    Log.d("mouthWashUsagetext", mouthWashUsagetext);
                                    //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                    for (int i = 0; i < mouthWashUsageSpinner.getAdapter().getCount(); i++)

                                    {
                                        if (mouthWashUsageSpinner.getItemAtPosition(i).equals(mouthWashUsagetext)) {
                                            mouthWashUsageSpinner.setSelection(i);
                                            mouthWashUsageSpinner.setEnabled(false);
                                            break;
                                        }
                                    }
                                }





                                if (entry.getKey().equals("lastVisitToDental")) {
                                    lastVisitToDentaltext = entry.getValue().toString();
                                    // genderSpinner.setAdapter(gender);
                                    Log.d("lastVisitToDentaltext", lastVisitToDentaltext);
                                    //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                    for (int i = 0; i < lastVisitToDentalSpinner.getAdapter().getCount(); i++)

                                    {
                                        if (lastVisitToDentalSpinner.getItemAtPosition(i).equals(lastVisitToDentaltext)) {
                                            lastVisitToDentalSpinner.setSelection(i);
                                            lastVisitToDentalSpinner.setEnabled(false);
                                            break;
                                        }
                                    }
                                }





                                if (entry.getKey().equals("recommendedVisitIntervalToDentist")) {
                                    recommendedVisitIntervalToDentisttext = entry.getValue().toString();
                                    // genderSpinner.setAdapter(gender);
                                    Log.d("recommendedVisitInte", recommendedVisitIntervalToDentisttext);
                                    //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                    recommendedVisitIntervalToDentist.setText(recommendedVisitIntervalToDentisttext);
                                    recommendedVisitIntervalToDentist.setEnabled(false);
                                }


                                if (entry.getKey().equals("lastVisitToHygienist")) {
                                    lastVisitToHygienisttext = entry.getValue().toString();
                                    // genderSpinner.setAdapter(gender);
                                    Log.d("lastVisitToHygienist", lastVisitToHygienisttext);
                                    //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                    for (int i = 0; i < lastVisitToHygienistSpinner.getAdapter().getCount(); i++)

                                    {
                                        if (lastVisitToHygienistSpinner.getItemAtPosition(i).equals(lastVisitToHygienisttext)) {
                                            lastVisitToHygienistSpinner.setSelection(i);
                                            lastVisitToHygienistSpinner.setEnabled(false);
                                            break;
                                        }
                                    }
                                }







                                if (entry.getKey().equals("recommendedVisitIntervalToHygienist")) {
                                    recommendedVisitIntervalToHygienisttext = entry.getValue().toString();
                                    // genderSpinner.setAdapter(gender);
                                    Log.d("recommendedVisitInterva", recommendedVisitIntervalToHygienisttext);
                                    //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                    recommendedVisitIntervalToHygienist.setText(recommendedVisitIntervalToHygienisttext);
                                    recommendedVisitIntervalToHygienist.setEnabled(false);
                                }





                                if (entry.getKey().equals("isBrushingTeeth")) {
                                    isBrushingTeethtext = entry.getValue().toString();
                                    // genderSpinner.setAdapter(gender);
                                    Log.d("isBrushingTeethtext", String.valueOf(isBrushingTeethtext));
                                    //CursorAdapter myAdapter = (CursorAdapter) genderSpinner.getAdapter(); //cast
                                     if(isBrushingTeethtext.equals("false"))
                                         isBrushingTeeth=false;
                                     else
                                         isBrushingTeeth=true;

                                    smoke.setChecked(isBrushingTeeth);
                                    smoke.setEnabled(false);
                                }

                            }









                            } else {
                            Log.d("LOGGER", "No such document");
                        }
                    } else {
                        Log.d("LOGGER", "get failed with ", task.getException());
                    }
                }
            });


        }




        // view=inflater.inflate(R.layout.activity_details, container, false);
        Button  btn = (Button) view.findViewById(R.id.next);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("clicked","button clicked");
                //  Frag4 fragment = new Frag4();
              /*  if(fragment!=null) {
                    Bundle arguments = new Bundle();
                    arguments.putString("Uid", gUid);
                    fragment.setArguments(arguments);


                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.addToBackStack(null);
                    Log.d("container", String.valueOf(((ViewGroup) getView().getParent()).getId()));
                    //transaction.hide(NewUser.this);
                    transaction.replace(R.id.root_frame1, fragment).commit();
                    getChildFragmentManager().executePendingTransactions();


                    //make your toast here

                    //theLayout.removeAllViewsInLayout();
                    // view=(LinearLayout)inflater.inflate(R.layout.activity_oral_hygiene, container, false);
                }*/

            }
        });


        return view;
    }
}
