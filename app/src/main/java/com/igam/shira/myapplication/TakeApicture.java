package com.igam.shira.myapplication;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.CamcorderProfile;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TakeApicture extends AppCompatActivity {
    private TextView SubmissionNumber,counter,changeSub,option;
    private  Map<String, Object> imageUpload = new HashMap<>();
    private  Map<String, Object> weekNumber = new HashMap<>();
    private FirebaseFirestore mDatabase;
    private ProgressDialog mProgress;
    int i;
    int count_of_images=1;
    private static final String TAG = "AndroidCameraApi";
    public MediaPlayer mp1;
    private Canvas canvas;
    Paint paint=new Paint();
    String submissionNum;
    final static String SUBMISSION="הגשה מספר";
    final static String CAMERA_OPEN=" המצלמה תיפתח בעוד ";
    final static String SEC=" שניות";
    int count =5,delay=5;

    ImageView cameera_pic;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    //private CameraPreview mCameraPreview;
    private StorageReference mStorageRef;
    private TextView status1;
    private String status;

    private String weekNum = "EE";
    private Button submit;
    Spinner spinner;
    private String gUid;
    private List<File> pictures=new ArrayList<>();
    FrameLayout frameLayout;
    private TextView tentimer;
    private String pathweek;
    int image_counter=10;
    private Button back;


    private static final String TIMER="Timer:";




    private static final int REQUEST_CAMERA_PERMISSION_RESULT = 0;
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION_RESULT = 1;
    private static final int STATE_PREVIEW = 0;
    private static final int STATE_WAIT_LOCK = 1;
    public static final String CAMERA_FRONT = "1";
    public static final String CAMERA_BACK = "0";
    private String cameraId = CAMERA_BACK;
    private int mCaptureState = STATE_PREVIEW;
    private TextureView mTextureView;
    private ImageView cancle_btn;
    private TextView timer;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_apicture);
        timer=(TextView) findViewById(R.id.timer);
        timer.setVisibility(View.GONE);
        cancle_btn=(ImageView)findViewById(R.id.cancle);
        cancle_btn.setVisibility(View.GONE);
        cancle_btn.setEnabled(false);
        createImageFolder();


        frameLayout = (FrameLayout) findViewById(R.id.camera_preview);
        frameLayout.setVisibility(View.GONE);
        mChronometer = (Chronometer) findViewById(R.id.chronometer);
        mTextureView = (TextureView) findViewById(R.id.textureView);

        mp1 = MediaPlayer.create(TakeApicture.this, R.raw.shuter);


        gUid= getIntent().getStringExtra("Uid");
        weekNum=getIntent().getStringExtra("submission");

        weekNumber.put(weekNum,imageUpload);
        pathweek="mouth"+weekNum;

        DocumentReference docRef = db.collection("users-data").document(gUid);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        //     Log.i("LOGGER","First "+document.getString("first_entry"));

                        Map<String, Object> map = (Map) document.get("mouthReviewSubmissions");
                        Map<String, Object> map1 = (Map) map.get(pathweek);

                        status = (String) map1.get("status");
                        status1.setText(status);
                        Log.e(" status00", String.valueOf(status));

                    } else {
                        Log.d("LOGGER", "No such document");
                    }
                }
                else{
                    Log.d("LOGGER", "get failed with ", task.getException());
                }
            }


        });





        status1=(TextView)findViewById(R.id.statusID);
        cameera_pic=(ImageView)findViewById( R.id.camera) ;
        counter=(TextView)findViewById(R.id.camera_txt);
        submit=(Button)findViewById(R.id.apply);
        back=(Button)findViewById(R.id.backButton) ;
        back.setVisibility(View.GONE);

//spinner=findViewById(R.id.gender);


        //  Log.e(" weekNum", String.valueOf(weekNum));
        //status= getIntent().getStringExtra("status");
        //  Log.e("status", String.valueOf(status));

        // mCameraPreview = new CameraPreview(this, mCamera);
        mProgress = new ProgressDialog(this);
        mProgress.setMessage("uploading...");
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        //  preview.addView(mCameraPreview);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mDatabase=FirebaseFirestore.getInstance();



        Bitmap bitmapBob;
        bitmapBob = BitmapFactory.decodeResource(this.getResources(), R.drawable.camera);

        Rect dest = new Rect(0, 0, 60, 60);
        canvas=new Canvas();
        // Draw bob as background with dest size
        canvas.drawBitmap(bitmapBob, null, dest, paint);
        submit.setEnabled(false);
        submissionNum= getIntent().getStringExtra("submission");
        SubmissionNumber=(TextView) findViewById(R.id.cameraSecID);

        changeSub=(TextView) findViewById(R.id.changeSub);
        tentimer=(TextView) findViewById(R.id.textView15);
        //option=(TextView) findViewById(R.id.option);
        int number= Integer.parseInt(submissionNum)+1;

        SubmissionNumber.setTextColor(Color.BLACK);
        SubmissionNumber.setTextSize(30);
        SubmissionNumber.setText(SUBMISSION+" "+number);
        Log.e(" status88", String.valueOf(status));

        counter.setTextSize(20);
        counter.setTextColor(Color.BLACK);

       /* cal.add(Calendar.SECOND, 15);
        //TAKE PHOTO EVERY 15 SECONDS
        PendingIntent pintent = PendingIntent.getService(this, 0, service, 0);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                60*60*1000, pintent);
        startService(service);*/

        cameera_pic .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   cameera_pic.setImageBitmap(null);
                cameera_pic.setVisibility(View.GONE);
                tentimer.setVisibility(View.VISIBLE);
                counter.setVisibility(View.VISIBLE);
                //frameLayout.setVisibility(View.VISIBLE);


                new CountDownTimer(5000,1000){

                    @Override
                    public void onFinish() {


                        onResume();

                        cameera_pic.setEnabled(false);
                        //  SubmissionNumber.setVisibility(View.GONE);
                        counter.setVisibility(View.GONE);

//                            spinner.setVisibility(View.GONE);
                        submit.setVisibility(View.GONE);
                        tentimer.setVisibility(View.GONE);
//                            changeSub.setVisibility(View.GONE);
                        //     option.setVisibility(View.GONE);
                        frameLayout.getLayoutParams().height=FrameLayout.LayoutParams.WRAP_CONTENT;
                        //frameLayout.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,FrameLayout.LayoutParams.WRAP_CONTENT));
                        frameLayout.requestLayout();
                        frameLayout.setVisibility(View.VISIBLE);
                        timer.setVisibility(View.VISIBLE);
                        timer.setText(TIMER+10);

                        delay=5;
                        image_counter=10;


                        //  take_pictures();



                        new CountDownTimer(2000,1000){

                            @Override
                            public void onFinish() {

                                image_counter=10;
                                function();

                                //  take_pictures();


                            }

                            @Override
                            public void onTick(long millisUntilFinished) {

                            }


                        }.start();








                    }

                    @Override
                    public void onTick(long millisUntilFinished) {
                        counter.setText(CAMERA_OPEN+delay+""+SEC);
                        delay--;


                    }


                }.start();

            }
        });







        submit .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(   i=0;i<pictures.size();i++) {
                    File pictureFile = pictures.get(i);


                    Uri uri = Uri.fromFile(pictureFile);
                    uploadMethod(uri);
                    if(i==pictures.size()-1) {
                        Log.d("firestore uplouding","firestore");
                        uploadToFirestore();
                    }

                }


            }

        });

        cancle_btn .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameera_pic.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.GONE);
                cancle_btn.setVisibility(View.GONE);
                submit.setEnabled(false);

                cameera_pic.bringToFront();
                cameera_pic.setEnabled(true);
                for(int i=0;i<pictures.size();i++)
                {
                    pictures.remove(i);
                }
                Log.d("cancle button","cancle");




            }

        });

        back .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(TakeApicture.this, Treatment_progress.class);
                intent.putExtra("Uid", gUid);
                startActivity(intent);

            }

        });





    }


















    private TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            setupCamera(width, height);
            connectCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    };
    private CameraDevice mCameraDevice;
    private CameraDevice.StateCallback mCameraDeviceStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            mCameraDevice = camera;
            mMediaRecorder = new MediaRecorder();
            if(mIsRecording) {
                try {
                    createVideoFileName();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                startRecord();
                mMediaRecorder.start();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mChronometer.setBase(SystemClock.elapsedRealtime());
                        mChronometer.setVisibility(View.VISIBLE);
                        mChronometer.start();
                    }
                });
            } else {
                startPreview();
            }
            // Toast.makeText(getApplicationContext(),
            //         "Camera connection made!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onDisconnected(CameraDevice camera) {

            mCameraDevice = camera;
            camera.close();
        }

        @Override
        public void onError(CameraDevice camera, int error) {
            camera.close();
            mCameraDevice = null;
        }
    };
    private HandlerThread mBackgroundHandlerThread;
    private Handler mBackgroundHandler;
    private String mCameraId;
    private Size mPreviewSize;
    private Size mVideoSize;
    private Size mImageSize;
    private ImageReader mImageReader;
    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener = new
            ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader reader) {
                    mBackgroundHandler.post(new ImageSaver(reader.acquireNextImage()));
                    //   reader.close();
                }
            };
    private class ImageSaver implements Runnable {

        private final Image mImage;

        public ImageSaver(Image image) {
            mImage = image;
            //  mImage.close();
        }

        @Override
        public void run() {
            ByteBuffer byteBuffer = mImage.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[byteBuffer.remaining()];
            byteBuffer.get(bytes);

            FileOutputStream fileOutputStream = null;
            try {
                fileOutputStream = new FileOutputStream(mImageFileName);
                fileOutputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                mImage.close();


                // Intent mediaStoreUpdateIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                // mediaStoreUpdateIntent.setData(Uri.fromFile(new File(mImageFileName)));
                // sendBroadcast(mediaStoreUpdateIntent);

                if(fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }


        }
    }
    private MediaRecorder mMediaRecorder;
    private Chronometer mChronometer;
    private int mTotalRotation;
    private CameraCaptureSession mPreviewCaptureSession;
    private CameraCaptureSession.CaptureCallback mPreviewCaptureCallback = new
            CameraCaptureSession.CaptureCallback() {

                private void process(CaptureResult captureResult) {
                    switch (mCaptureState) {
                        case STATE_PREVIEW:
                            // Do nothing
                            break;
                        case STATE_WAIT_LOCK:
                            mCaptureState = STATE_PREVIEW;
                            Integer afState = captureResult.get(CaptureResult.CONTROL_AF_STATE);
                            if(afState == CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED ||
                                    afState == CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED) {
                                Toast.makeText(getApplicationContext(), "AF Locked!", Toast.LENGTH_SHORT).show();



                                new CountDownTimer(20000,2000){

                                    @Override
                                    public void onFinish() {
                                        image_counter=10;
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                frameLayout.setVisibility(View.GONE);

//                    spinner.setVisibility(View.VISIBLE);
                                                //    submit.setVisibility(View.VISIBLE);
                                                //       changeSub.setVisibility(View.VISIBLE);
                                                //       option.setVisibility(View.VISIBLE);

                                                SubmissionNumber.setVisibility(View.VISIBLE);

//                    spinner.setVisibility(View.VISIBLE);
                                                submit.setVisibility(View.VISIBLE);
                                                submit.setEnabled(true);
                                                //       changeSub.setVisibility(View.VISIBLE);
                                                //       option.setVisibility(View.VISIBLE);

                                               // frameLayout.getLayoutParams().height=700;
                                                frameLayout.getLayoutParams().height =FrameLayout.LayoutParams.WRAP_CONTENT;
                                                //  frameLayout.getLayoutParams().width=450;
                                                frameLayout.requestLayout();

                                                timer.setVisibility(View.GONE);
                                                //cancle_button.bringToFront();
                                                cancle_btn.setVisibility(View.VISIBLE);
                                                cancle_btn.setEnabled(true);
                                                frameLayout.setVisibility(View.VISIBLE);
                                                submit.setEnabled(true);

                                                frameLayout.setVisibility(View.VISIBLE);


                                                mCaptureRequestBuilder.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_OFF);
                                                try {
                                                    mPreviewCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(), null, null);
                                                } catch (CameraAccessException e) {
                                                    e.printStackTrace();
                                                }

                                                new CountDownTimer(5000,1000){

                                                    @Override
                                                    public void onFinish() {

                                                        try {
                                                            mPreviewCaptureSession.stopRepeating();
                                                        } catch (CameraAccessException e) {
                                                            e.printStackTrace();
                                                        }



                                                    }

                                                    @Override
                                                    public void onTick(long millisUntilFinished) {


                                                    }


                                                }.start();


                                                //stuff that updates ui

                                            }
                                        });

                                    }

                                    @Override
                                    public void onTick(long millisUntilFinished) {
                                        Log.d("pic","taken picture");

                                        mp1.start();
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                image_counter--;

                                                //stuff that updates ui
                                                timer.setText(TIMER+image_counter);

                                            }
                                        });
                                        startStillCaptureRequest();

                                        //unlockFocus();



                                    }

                                }.start();





                            }
                            break;
                    }
                }

                @Override
                public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);



                    process(result);
                }
            };
    private CameraCaptureSession mRecordCaptureSession;
    private CameraCaptureSession.CaptureCallback mRecordCaptureCallback = new
            CameraCaptureSession.CaptureCallback() {

                private void process(CaptureResult captureResult) {
                    switch (mCaptureState) {
                        case STATE_PREVIEW:
                            // Do nothing
                            break;
                        case STATE_WAIT_LOCK:
                            mCaptureState = STATE_PREVIEW;
                            Integer afState = captureResult.get(CaptureResult.CONTROL_AF_STATE);
                            if(afState == CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED ||
                                    afState == CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED) {
                                Toast.makeText(getApplicationContext(), "AF Locked!", Toast.LENGTH_SHORT).show();
                                startStillCaptureRequest();
                            }
                            break;
                    }
                }

                @Override
                public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);

                    process(result);
                }
            };
    private CaptureRequest.Builder mCaptureRequestBuilder;

    private ImageButton mStillImageButton;
    private boolean mIsRecording = false;
    private boolean mIsTimelapse = false;

    private File mVideoFolder;
    private String mVideoFileName;
    private File mImageFolder;
    private String mImageFileName;

    private static SparseIntArray ORIENTATIONS = new SparseIntArray();
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 0);
        ORIENTATIONS.append(Surface.ROTATION_90, 90);
        ORIENTATIONS.append(Surface.ROTATION_180, 180);
        ORIENTATIONS.append(Surface.ROTATION_270, 270);
    }

    private static class CompareSizeByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            return Long.signum( (long)(lhs.getWidth() * lhs.getHeight()) -
                    (long)(rhs.getWidth() * rhs.getHeight()));
        }
    }





















    //////////////////////////////////////////////////////////////////////////////////








    private void function() {
        if(!(mIsTimelapse || mIsRecording)) {
            timer.setText(TIMER+10);
            frameLayout.setVisibility(View.VISIBLE);

            checkWriteStoragePermission();
        }

        lockFocus();
        startPreview();
    }

    @Override
    protected void onResume() {
        super.onResume();

        startBackgroundThread();

        if(mTextureView.isAvailable()) {
            setupCamera(mTextureView.getWidth(), mTextureView.getHeight());
            connectCamera();
        } else {
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_CAMERA_PERMISSION_RESULT) {
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(),
                        "Application will not run without camera services", Toast.LENGTH_SHORT).show();
            }
            if(grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(),
                        "Application will not have audio on record", Toast.LENGTH_SHORT).show();
            }
        }
        if(requestCode == REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION_RESULT) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if(mIsRecording || mIsTimelapse) {
                    mIsRecording = true;
                    //  mRecordImageButton.setImageResource(R.mipmap.btn_video_busy);
                }
                Toast.makeText(this,
                        "Permission successfully granted!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this,
                        "App needs to save video to run", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onPause() {
        closeCamera();

        stopBackgroundThread();

        super.onPause();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocas) {
        super.onWindowFocusChanged(hasFocas);
        View decorView = getWindow().getDecorView();
        if(hasFocas) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    private void setupCamera(int width, int height) {
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            for(String cameraId : cameraManager.getCameraIdList()){
                CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);
                if(cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) ==
                        CameraCharacteristics.LENS_FACING_FRONT){
                    continue;
                }
                StreamConfigurationMap map = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                int deviceOrientation = getWindowManager().getDefaultDisplay().getRotation();
                mTotalRotation = sensorToDeviceRotation(cameraCharacteristics, deviceOrientation);
                boolean swapRotation = mTotalRotation == 90 || mTotalRotation == 270;
                int rotatedWidth = width;
                int rotatedHeight = height;
                if(swapRotation) {
                    rotatedWidth = height;
                    rotatedHeight = width;
                }
                mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class), rotatedWidth, rotatedHeight);
                mVideoSize = chooseOptimalSize(map.getOutputSizes(MediaRecorder.class), rotatedWidth, rotatedHeight);
                mImageSize = chooseOptimalSize(map.getOutputSizes(ImageFormat.JPEG), rotatedWidth, rotatedHeight);
                mImageReader = ImageReader.newInstance(mImageSize.getWidth(), mImageSize.getHeight(), ImageFormat.JPEG, 1);
                mImageReader.setOnImageAvailableListener(mOnImageAvailableListener, mBackgroundHandler);
                mCameraId = cameraId;
                return;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void connectCamera() {
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) ==
                        PackageManager.PERMISSION_GRANTED) {
                    cameraManager.openCamera(mCameraId, mCameraDeviceStateCallback, mBackgroundHandler);
                } else {
                    if(shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA)) {
                        Toast.makeText(this,
                                "Video app required access to camera", Toast.LENGTH_SHORT).show();
                    }
                    requestPermissions(new String[] {android.Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO
                    }, REQUEST_CAMERA_PERMISSION_RESULT);
                }

            } else {
                cameraManager.openCamera(mCameraId, mCameraDeviceStateCallback, mBackgroundHandler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void startRecord() {

        try {
            if(mIsRecording) {
                setupMediaRecorder();
            } else if(mIsTimelapse) {
                setupTimelapse();
            }
            SurfaceTexture surfaceTexture = mTextureView.getSurfaceTexture();
            surfaceTexture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            Surface previewSurface = new Surface(surfaceTexture);
            Surface recordSurface = mMediaRecorder.getSurface();
            mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);

            mCaptureRequestBuilder.addTarget(previewSurface);
            mCaptureRequestBuilder.addTarget(recordSurface);

            mCameraDevice.createCaptureSession(Arrays.asList(previewSurface, recordSurface, mImageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(CameraCaptureSession session) {
                            mRecordCaptureSession = session;
                            try {
                                mRecordCaptureSession.setRepeatingRequest(
                                        mCaptureRequestBuilder.build(), null, null
                                );
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(CameraCaptureSession session) {
                            Log.d(TAG, "onConfigureFailed: startRecord");
                        }
                    }, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void startPreview() {
        SurfaceTexture surfaceTexture = mTextureView.getSurfaceTexture();
        surfaceTexture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
        Surface previewSurface = new Surface(surfaceTexture);

        try {

            mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mCaptureRequestBuilder.set(CaptureRequest.CONTROL_MODE,
                    CameraMetadata.CONTROL_MODE_AUTO);
            mCaptureRequestBuilder.set(CaptureRequest.FLASH_MODE,
                    CameraMetadata.FLASH_MODE_TORCH);
            mCaptureRequestBuilder.addTarget(previewSurface);

            mCameraDevice.createCaptureSession(Arrays.asList(previewSurface, mImageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(CameraCaptureSession session) {
                            Log.d(TAG, "onConfigured: startPreview");
                            mPreviewCaptureSession = session;
                            try {
                                mPreviewCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(),
                                        null, mBackgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(CameraCaptureSession session) {
                            Log.d(TAG, "onConfigureFailed: startPreview");

                        }
                    }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void startStillCaptureRequest() {
        try {

            mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);

            mCaptureRequestBuilder.addTarget(mImageReader.getSurface());
            mCaptureRequestBuilder.set(CaptureRequest.JPEG_ORIENTATION, mTotalRotation);

            mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
            mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AWB_MODE, CaptureRequest.CONTROL_AWB_MODE_AUTO);
            // mCaptureRequestBuilder.set(CaptureRequest.CONTROL_MODE,
            //CameraMetadata.CONTROL_MODE_AUTO);
            mCaptureRequestBuilder.set(CaptureRequest.FLASH_MODE,
                    CameraMetadata.FLASH_MODE_TORCH);


            CameraCaptureSession.CaptureCallback stillCaptureCallback = new
                    CameraCaptureSession.CaptureCallback() {
                        @Override
                        public void onCaptureStarted(CameraCaptureSession session, CaptureRequest request, long timestamp, long frameNumber) {
                            super.onCaptureStarted(session, request, timestamp, frameNumber);

                            try {
                                File f= createImageFileName();
                                pictures.add(f);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };

            if(mIsRecording) {
                mRecordCaptureSession.capture(mCaptureRequestBuilder.build(), stillCaptureCallback, null);
            } else {
                mPreviewCaptureSession.capture(mCaptureRequestBuilder.build(), stillCaptureCallback, null);
            }


            //  mPreviewCaptureSession.stopRepeating();

            // mPreviewCaptureSession.capture(mCaptureRequestBuilder.build(), stillCaptureCallback, null);

            // Reset the autofucos trigger
            //  mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
            // mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);

            // After this, the camera will go back to the normal state of preview.
            //  mCaptureState = STATE_PREVIEW;
            //  mPreviewCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(), stillCaptureCallback, mBackgroundHandler);




        } catch (CameraAccessException e) {
            e.printStackTrace();
        }


    }

    private void closeCamera() {
        if(mCameraDevice != null) {
            mCameraDevice.close();
            mCameraDevice = null;
        }
        if(mMediaRecorder != null) {
            mMediaRecorder.release();
            mMediaRecorder = null;
        }
    }

    private void startBackgroundThread() {
        mBackgroundHandlerThread = new HandlerThread("Camera2VideoImage");
        mBackgroundHandlerThread.start();
        mBackgroundHandler = new Handler(mBackgroundHandlerThread.getLooper());
    }

    private void stopBackgroundThread() {
        mBackgroundHandlerThread.quitSafely();
        try {
            mBackgroundHandlerThread.join();
            mBackgroundHandlerThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static int sensorToDeviceRotation(CameraCharacteristics cameraCharacteristics, int deviceOrientation) {
        int sensorOrienatation = cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
        deviceOrientation = ORIENTATIONS.get(deviceOrientation);
        return (sensorOrienatation + deviceOrientation + 360) % 360;
    }

    private static Size chooseOptimalSize(Size[] choices, int width, int height) {
        List<Size> bigEnough = new ArrayList<Size>();
        for(Size option : choices) {
            if(option.getHeight() == option.getWidth() * height / width &&
                    option.getWidth() >= width && option.getHeight() >= height) {
                bigEnough.add(option);
            }
        }
        if(bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizeByArea());
        } else {
            return choices[0];
        }
    }

    private void createVideoFolder() {
        File movieFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        mVideoFolder = new File(movieFile, "camera2VideoImage");
        if(!mVideoFolder.exists()) {
            mVideoFolder.mkdirs();
        }
    }

    private File createVideoFileName() throws IOException {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String prepend = "VIDEO_" + timestamp + "_";
        File videoFile = File.createTempFile(prepend, ".mp4", mVideoFolder);
        mVideoFileName = videoFile.getAbsolutePath();
        return videoFile;
    }

    private void createImageFolder() {
        File imageFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        mImageFolder = new File(imageFile, "camera2VideoImage");
        if(!mImageFolder.exists()) {
            mImageFolder.mkdirs();
        }
    }

    private File createImageFileName() throws IOException {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String prepend = "IMAGE_" + timestamp + "_";
        File imageFile = File.createTempFile(prepend, ".png", mImageFolder);
        mImageFileName = imageFile.getAbsolutePath();

        return imageFile;
    }

    private void checkWriteStoragePermission() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                try {
                    createVideoFileName();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if(mIsTimelapse || mIsRecording) {
                    startRecord();
                    mMediaRecorder.start();
                    mChronometer.setBase(SystemClock.elapsedRealtime());
                    mChronometer.setVisibility(View.VISIBLE);
                    mChronometer.start();
                }
            } else {
                if(shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "app needs to be able to save videos", Toast.LENGTH_SHORT).show();
                }
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION_RESULT);
            }
        } else {
            try {
                createVideoFileName();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(mIsRecording || mIsTimelapse) {
                startRecord();
                mMediaRecorder.start();
                mChronometer.setBase(SystemClock.elapsedRealtime());
                mChronometer.setVisibility(View.VISIBLE);
                mChronometer.start();
            }
        }
    }

    private void setupMediaRecorder() throws IOException {
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setOutputFile(mVideoFileName);
        mMediaRecorder.setVideoEncodingBitRate(1000000);
        mMediaRecorder.setVideoFrameRate(30);
        mMediaRecorder.setVideoSize(mVideoSize.getWidth(), mVideoSize.getHeight());
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mMediaRecorder.setOrientationHint(mTotalRotation);
        mMediaRecorder.prepare();
    }

    private void setupTimelapse() throws IOException {
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
        mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_TIME_LAPSE_HIGH));
        mMediaRecorder.setOutputFile(mVideoFileName);
        mMediaRecorder.setCaptureRate(2);
        mMediaRecorder.setOrientationHint(mTotalRotation);
        mMediaRecorder.prepare();
    }

    private void lockFocus() {
        mCaptureState = STATE_WAIT_LOCK;

        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CaptureRequest.CONTROL_AF_TRIGGER_START);
        try {
            if(mIsRecording) {
                mRecordCaptureSession.capture(mCaptureRequestBuilder.build(), mRecordCaptureCallback, mBackgroundHandler);
            } else {
                mPreviewCaptureSession.capture(mCaptureRequestBuilder.build(), mPreviewCaptureCallback, mBackgroundHandler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }





    }


   /* private void unlockFocus()
    {
        try {
            // Reset the autofucos trigger
            mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
            mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);

            // After this, the camera will go back to the normal state of preview.
            mCaptureState = STATE_PREVIEW;
            mPreviewCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(), startStillCaptureRequest(), mBackgroundHandler);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }*/









    public Bitmap getImageBitmap(String imgName){
        AssetManager mgr = getAssets();
        InputStream inputStream = null;

        try {
            inputStream = mgr.open(imgName + ".png");
            return BitmapFactory.decodeStream(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }














    private void uploadMethod(final Uri imageUri) {
        // progressDialog();
        cancle_btn.setVisibility(View.GONE);
        cancle_btn.setEnabled(false);
        submit.setEnabled(false);
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        StorageReference storageReferenceProfilePic = firebaseStorage.getReference();
        StorageReference imageRef = storageReferenceProfilePic.child("images").child(gUid).child(weekNum).child("IMG__userID_" + gUid + "_week_"+ weekNum +"_" + new Date().getTime()+".png");

        imageRef.putFile(imageUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        //if the upload is successful
                        //hiding the progress dialog
                        //and displaying a success toast
                        // mProgress.setMessage("Uploading Image...");
                        //mProgress.show();
                        if(taskSnapshot!=null) {

                            Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                            while (!urlTask.isSuccessful()) ;
                            Uri downloadUrl = urlTask.getResult();


                            final String sdownload_url = String.valueOf(downloadUrl);
                            Log.d("uriii", sdownload_url);
                            Upload upload = new Upload( sdownload_url);
                            imageUpload.put(String.valueOf(count_of_images), sdownload_url);
                            uploadToFirestore();
                            count_of_images++;
                        }





                        Toast.makeText(TakeApicture.this, "succed uploading file... ", Toast.LENGTH_LONG).show();

                        //String profilePicUrl = taskSnapshot.getDownloadUrl().toString();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        //if the upload is not successful
                        //hiding the progress dialog
                        mProgress.dismiss();
                        Toast.makeText(TakeApicture.this, "Error uploading file... ", Toast.LENGTH_LONG).show();
                        //and displaying error message
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        //calculating progress percentage
//                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
//                        //displaying percentage in progress dialog
//                        progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                    }
                });


    }


    public void uploadToFirestore()

    {
        Log.d("firestore uplouding","firestore");

        mDatabase.collection("users-data").document(gUid).update(weekNumber)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e(" successfully written!", "o");
                        back.setVisibility(View.VISIBLE);


                        // mProgress.show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("Error writing document", "o");
                    }
                }).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });

        DocumentReference docRef = db.collection("users-data").document(gUid);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document .exists()) {
                        //     Log.i("LOGGER","First "+document.getString("first_entry"));
                        Map<String, Object> map = (Map) document.get("mouthReviewSubmissions");
                        Map<String, Object> map1 = (Map) map.get(pathweek);
                        Map<String,Object>map2=new HashMap<>();
                        map1.put("status","ממתין לבדיקה");
                        map.put(pathweek,map1);
                        map2.put("mouthReviewSubmissions",map);
                        db.collection("users-data").document(gUid).update(map2);
                        status1.setText("ממתין לבדיקה");

                    } else {
                        Log.d("LOGGER", "No such document");
                    }
                } else {
                    Log.d("LOGGER", "get failed with ", task.getException());
                }
            }
        });

        final String path1=gUid+"#"+weekNum;
        Map<String, Object> data = new HashMap<>();
        Date submissionDate= Calendar.getInstance().getTime();
        data.put("id",path1);
        data.put("sequenceNumber",weekNum);
        data.put("submissionDate",submissionDate);
        data.put("userId",gUid);
        db.collection("submission-reviews").document(path1).set(data)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e(" successfully written!", "o");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("Error writing document", "o");
                    }
                });



    }

    @Override
    protected void onStop(){
        super.onStop();
        mp1.stop();
        //your code for stopping the sound
    }






}












