package com.igam.shira.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;

public class login extends AppCompatActivity {
    private EditText username,password;
    private FirebaseAuth mAuth;
    private ProgressDialog mProgress;
    private FirebaseFirestore db =FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login2);
        username=findViewById(R.id.username);
        password=findViewById(R.id.password);
        mAuth = FirebaseAuth.getInstance();
        mProgress = new ProgressDialog(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void updateUI(FirebaseUser currentUser) {
    }

    public void entry(View v)
    {
        final String username_str=username.getText().toString()+"@igam.com";
        final String password_str=password.getText().toString();
        Log.d("user",username_str);
        if(username_str!=null&&!username_str.isEmpty()&&password_str!=null&&!password_str.isEmpty()) {
            mProgress.setMessage("Loading...");
            mProgress.show();

            mAuth.signInWithEmailAndPassword(username_str, password_str)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d("user name", "signInWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                updateUI(user);
                                Log.d("id", user.getUid());

                                DocumentReference docRef = db.collection("users-data").document(username_str);
                                docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful()) {
                                            DocumentSnapshot document = task.getResult();
                                            if (document.exists()) {
                                                //     Log.i("LOGGER","First "+document.getString("first_entry"));
                                                Boolean b;
                                                Map<String, Object> map = document.getData();
                                                String s = (String) document.get("role");
                                                if (document.get("role") != null)
                                                    Log.d("role", s);
                                                b = (Boolean) document.get("first_entry");
                                                if (b == null)
                                                    b = false;
                                                //if the field is Boolean
                                                if (b) {//if user exists enters to treatment progress
                                                    mProgress.dismiss();
                                                    Intent intent = new Intent(login.this, Treatment_progress.class);
                                                    intent.putExtra("Uid", username_str);
                                                    startActivity(intent);
                                                } else if (!b) {//is its new user goes to fill form
                                                    db.collection("users-data").document(username_str).update("first_entry", true)
                                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                @Override
                                                                public void onSuccess(Void aVoid) {
                                                                    Log.e(" successfully written!", "o");
                                                                }
                                                            })
                                                            .addOnFailureListener(new OnFailureListener() {
                                                                @Override
                                                                public void onFailure(@NonNull Exception e) {
                                                                    Log.e("Error writing document", "o");
                                                                }
                                                            });
                                                    mProgress.dismiss();
                                                    Intent intent = new Intent(login.this, GeneralInformationActivity.class);
                                                    intent.putExtra("Uid", username_str);
                                                    startActivity(intent);
                                                }
                                            } else {
                                                Log.d("LOGGER", "No such document");
                                            }
                                        } else {
                                            Log.d("LOGGER", "get failed with ", task.getException());
                                        }
                                    }
                                });
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("user name", "signInWithEmail:failure", task.getException());
                                Toast.makeText(login.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                                updateUI(null);
                            }
                        }
                    });
        }
        else
            Toast.makeText(login.this, "missing details.",
                    Toast.LENGTH_SHORT).show();

    }
}
