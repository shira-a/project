package com.igam.shira.myapplication;

public class UploadBinaryImage {

    private String mName;
    private String mImageUrl;

    public UploadBinaryImage()
    {

    }
    public UploadBinaryImage(String ImageUrl)
    {

        this.mImageUrl=ImageUrl;
    }

    public String getmName() {
        return mName;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }
}
