package com.igam.shira.myapplication;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;

public class Details extends Activity {
    String code,phone,role,password;
    private FirebaseFirestore db =FirebaseFirestore.getInstance();
    private String gUid;
    EditText user_code;
    EditText user_role;
    Spinner role_u;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_user_detailes);
        gUid= getIntent().getStringExtra("Uid");
        Log.d("detailes",gUid);
        setTitle("פרטי משתמש");
         user_code=(EditText)findViewById(R.id.user_code);
        EditText password=(EditText)findViewById(R.id.password);
        EditText user_phone=(EditText)findViewById(R.id.user_phone);
         user_role=(EditText)findViewById(R.id.user_role);
     if(gUid!=null) {
         DocumentReference docRef = db.collection("users-data").document(gUid);


         docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
             @Override
             public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                 if (task.isSuccessful()) {
                     DocumentSnapshot document = task.getResult();
                     if (document.exists()) {
                         //     Log.i("LOGGER","First "+document.getString("first_entry"));
                         Map<String, Object> map = document.getData();
                         role = (String) document.get("role");
                         user_role.setText(role);

                         //if the field is Boolean


                         // Log.i("LOGGER","second "+document.getString("role"));

                     } else {
                         Log.d("LOGGER", "No such document");
                     }
                 } else {
                     Log.d("LOGGER", "get failed with ", task.getException());
                 }
             }
         });

         user_role.setTextColor(Color.BLACK);
         user_code.setEnabled(false);
//        password.setEnabled(false);
         user_role.setEnabled(false);
         user_phone.setEnabled(false);
     }

else if(gUid==null)
     {

          edit();


     }

    }


    public void edit()
    {

        String colors[] = {"admin","user"};

// Selection of the spinner
        Spinner spinner = new Spinner(this);

// Application of the Array to the Spinner
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, colors);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner.setAdapter(spinnerArrayAdapter);


    }
}
