package com.igam.shira.myapplication;




import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 @author : Sumit Ranjan <sumit.nitt@gmail.com>
 @description : Main Activity
 **/



public class MarkingImageActivity extends Activity implements OnTouchListener {
    private Uri mImageCaptureUri;
    private ImageView drawImageView;
    private Bitmap sourceBitmap;
    private Drawable image;
    private Path clipPath;
    myview myview;
    LinearLayout rl;

    private static final int PICK_FROM_FILE = 1;
    private static final int CAMERA_CAPTURE = 2;
private String path;
    //  Polygon.Builder polygon;
    Region r;
    Bitmap  imageScaled;
    Polygon p;
    Polygon polygon;
    Bitmap bmp;
    Bitmap alteredBitmap;
    Point point;
    // Canvas canvas;
    Paint paint;
    Matrix matrix;
    float downx = 0;
    float downy = 0;
    float upx = 0;
    int drag_point=-1;
    float upy = 0;
    public static int num_of_path=0;
    public static int crop_num=7;
    public static Paths []paths=new Paths[crop_num];
    boolean flgPathDraw = true,touch_in_point=false,f=false,add_polygon_flag=false,touch_in_first_point=false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marking_image);
        //setContentView(new myview(this));
        LinearLayout rl = (LinearLayout) findViewById(R.id.layoutID);
        // myview.layout(50,50,50,50);

        // check_point.run();
        myview=new myview(this);
      //  myview.setPadding(100,100,100,100);
        //myview.setBackgroundColor(Color.BLACK);


        //myview.setForegroundGravity(Gravity.CENTER);

        //myview.setBottom(700);
        //  myview.setBottom(500);
        //myview.setRight(500);
        // myview.setLeft(500);
       // myview.setX(-400);
       // myview.setY(0);

       // myview.setZ(0);


        path=getIntent().getStringExtra("Url");
       //myview.setLayoutParams(new LinearLayout.LayoutParams(10, 10));


       // final LinearLayout.MarginLayoutParams params = (LinearLayout.MarginLayoutParams) myview.getLayoutParams();

        //myview.setLayoutParams(params);
        // myview.setLeft(300);
        //  myview.setRight(300);
        //  myview.setBottom(300);
        //  params.topMargin += 600;
        //  params.bottomMargin+=600;
        // params.setMarginStart(100);
        // params.setMarginEnd(100);
        //  params.leftMargin+=50;
        //  params.rightMargin+=100;
        //   params.setMargins(100,100,100,500);
        // params.resolveLayoutDirection(-params.getMarginStart());
        //   params.setMarginStart(-params.height);




        myview.requestFocus();

        rl.addView(myview);
        //   rl.addView(myview,50,50);



        if (!checkPermission()) {
            openActivity();
        } else {
            if (checkPermission()) {
                requestPermissionAndContinue();
            } else {
                openActivity();
            }
        }



        paths[0]=new Paths();
        paths[1]=new Paths();
        paths[2]=new Paths();
        paths[3]=new Paths();
        paths[4]=new Paths();
        paths[5]=new Paths();


        final String [] items			= new String [] {"Take from camera", "Select from gallery"};
        ArrayAdapter<String> adapter	= new ArrayAdapter<String> (this, android.R.layout.select_dialog_item,items);
        AlertDialog.Builder builder		= new AlertDialog.Builder(this);

        builder.setTitle("Select Image");
        builder.setAdapter( adapter, new DialogInterface.OnClickListener() {
            public void onClick( DialogInterface dialog, int item ) { //pick from camera
                if (item == 0) {
                    try {
                        //use standard intent to capture an image
                        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        //we will handle the returned data in onActivityResult
                        startActivityForResult(captureIntent, CAMERA_CAPTURE);
                    } catch(ActivityNotFoundException e){
                        e.printStackTrace();
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                } else { //pick from file
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);
                }
            }
        } );

        final AlertDialog dialog = builder.create();

        Button button 	= (Button) findViewById(R.id.btn_crop);
        Button saveButton 	= (Button) findViewById(R.id.btn_save);
        Button discardButton 	= (Button) findViewById(R.id.btn_discard);
        Button rotateButton=(Button)findViewById(R.id.btn_rotate) ;
        Button addPolygon=(Button) findViewById(R.id.add_polygon_id);

        drawImageView = (ImageView) findViewById(R.id.DrawImageView);
        drawImageView.setOnTouchListener(this);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                try {
                    String path = Environment.getExternalStorageDirectory().toString();
                    OutputStream fOut = null;
                    File file = new File(path, String.valueOf(Math.round(Math.random()*100000))+".jpg");
                    Log.d("vvvvvv",file.getName());
                    fOut = new FileOutputStream(file);
                    // cropImageByPath();
                    alteredBitmap= Binary(alteredBitmap);

                    alteredBitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
                    fOut.flush();
                    fOut.close();

                    MediaStore.Images.Media.insertImage(getContentResolver(),file.getAbsolutePath(),file.getName(),file.getName());
                    AlertDialog.Builder builder		= new AlertDialog.Builder(MarkingImageActivity.this);
                    builder.setMessage("Saved");
                    AlertDialog dialog = builder.create();
                    dialog.show();

                    //Now deleting the temporary file created on camera capture
                    File f = new File(mImageCaptureUri.getPath());
                    if (f.exists()) {
                        f.delete();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        discardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  if (mImageCaptureUri != null) {
                    //Now deleting the temporary file created on camera capture
                    File f = new File(mImageCaptureUri.getPath());
                    if (f.exists()) {
                        f.delete();
                    }

                    getContentResolver().delete(mImageCaptureUri, null, null );
                    mImageCaptureUri = null;
                    //resetting the image view
                    ImageView iv = (ImageView) findViewById(R.id.DrawImageView);
                    iv.setImageDrawable(null);*/
                Log.d("discard","discard button");
                Log.d("before num of path", String.valueOf(num_of_path));

                // if (num_of_path>0)
                //  num_of_path=num_of_path-1;
                if( paths[num_of_path].getPoints().size()!=0)
                {
                    paths[num_of_path].getPoints().clear();
                    paths[num_of_path].setPath(new Path());
                    paths[num_of_path].setMfirstpoint(null);
                    paths[num_of_path].setBfirstpoint(false);

                }
                else {
                    if (num_of_path > 0)
                        num_of_path = num_of_path - 1;
                    paths[num_of_path].getPoints().clear();
                    Log.d("points size", String.valueOf(paths[num_of_path].getPoints().size()));


                    paths[num_of_path].setPath(new Path());
                    paths[num_of_path].setMfirstpoint(null);
                    paths[num_of_path].setBfirstpoint(false);


                }
                Log.d("after num of path", String.valueOf(num_of_path));

                myview.invalidate();

            }
        });










        rotateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawImageView.setRotation(drawImageView.getRotation() + 90);
                myview.invalidate();

            }

        });



        addPolygon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(paths[num_of_path].getPoints().size()!=0) {
                    paths[num_of_path].setMlastpoint(point);
                    build_path();
                    //  paths[num_of_path].getPath().close();

                    // num_of_path++;
                    //  add_polygon_flag = true;
                    //  myview.invalidate();
                }

            }

        });
        new Thread(new Runnable() {
            public void run() {
                mImageCaptureUri= loadImageFromNetwork();

                drawImageView.post(new Runnable() {
                    public void run() {

                        // alteredBitmap=functiomm();

                        //    drawImageView.setImageBitmap(bmp);

                      //  doCrop();

                        myview.invalidate();
                        //functiomm();

                    }
                });
            }
        }).start();




        myview.invalidate();





    }
    private Uri loadImageFromNetwork() {


        try {


            Bitmap bitmap = null;
            InputStream iStream = null;
            URL url12 = new URL(path);
            HttpURLConnection conn = (HttpURLConnection) url12.openConnection();
            conn.setReadTimeout(5000 /* milliseconds */);
            conn.setConnectTimeout(7000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            iStream = conn.getInputStream();
            bitmap = BitmapFactory.decodeStream(conn.getInputStream());
           // drawImageView.setImageBitmap(bitmap);

            File f = new File(Environment.getExternalStorageDirectory(),System.currentTimeMillis() + ".png");
            if(f.exists())
                f.delete();
            f.createNewFile();
            Bitmap bitmap1 = bitmap;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
           bitmap1.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);

            byte[] bitmapdata = bos.toByteArray();
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            mImageCaptureUri = Uri.fromFile(f);
            image=Drawable.createFromStream(iStream, mImageCaptureUri.toString());
            drawImageView.setBackground(image);

            fos.close();





            //  BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
            //   bmpFactoryOptions.inJustDecodeBounds = true;

            // bitmap = BitmapFactory.decodeStream(iStream,null,bmpFactoryOptions);
           /* int imageWidth = bmpFactoryOptions.outWidth;
            int imageHeight = bmpFactoryOptions.outHeight;
            URL url = new URL(path); //Some instantiated URL object


            mImageCaptureUri = Uri.parse(path );*/






            // bmp = bitmap;
         /*   if(bmp!=null) {
              //  alteredBitmap = Bitmap.createBitmap(imageWidth,
                   //     imageHeight, bmp.getConfig());


                BitmapFactory.Options bmpFactoryOptions1 = new BitmapFactory.Options();
                bmpFactoryOptions1.inJustDecodeBounds = true;
                bmp1 = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                        mImageCaptureUri), null, bmpFactoryOptions1);
                bmpFactoryOptions.inJustDecodeBounds = false;
                bmp1 = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                        mImageCaptureUri), null, bmpFactoryOptions1);
                alteredBitmap = Bitmap.createBitmap(bmp1.getWidth(), bmp1
                        .getHeight(), bmp1.getConfig());

                Log.d("alerted image","gggggggggggggggggggggggggggguu");


            }*/
            iStream.close();

//            bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(
            // mImageCaptureUri), null, bmpFactoryOptions);


            Log.d("alerted image","dddddddddddddddddddddddddddddddddddddddd");




        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mImageCaptureUri;

    }

    /////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;
        switch (requestCode) {

            case PICK_FROM_FILE:
                mImageCaptureUri = data.getData();
                doCrop();
                break;

            case CAMERA_CAPTURE:
                mImageCaptureUri = data.getData();
                doCrop();
                break;
        }
    }




    public void fun()
    {
        // polygon = Polygon.Builder();




      /*  for (int i = 0; i < paths[0].getPoints().size(); i++) {
            polygon = Polygon.Builder().
        addVertex(new com.shirael.project1.Point(paths[0].getPoints().get(i).x, MainActivity.paths[0].getPoints().get(i).y)).build();
        }*/




       /* Polygon.Builder poly2 = new Polygon.Builder();


        for (int i = 0; i < paths[0].getPoints().size(); i++) {
            poly2.addVertex(new com.shirael.project1.Point(paths[0].getPoints().get(i).x, MainActivity.paths[0].getPoints().get(i).y) );

            Polygon polygon2 = poly2.build();*/
        Polygon.Builder poly2=new Polygon.Builder();

        for (int i = 0; i < paths[0].getPoints().size(); i++)
        {
            poly2.addVertex((new com.igam.shira.myapplication.Point(paths[0].getPoints().get(i).x,paths[0].getPoints().get(i).y)));
        }

        polygon=poly2.build();

    }





    public Bitmap Binary(Bitmap bmpOriginal)
    {
        int width, height, threshold;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();
        threshold = 127;
        Bitmap bmpBinary = Bitmap.createBitmap(bmpOriginal);
        fun();

        for (int x = 0; x < bmpOriginal.getWidth(); x++) {
            for (int y = 0; y < bmpOriginal.getHeight(); y++) {
                com.igam.shira.myapplication.Point point=new com.igam.shira.myapplication.Point(x,y);
              /*  if(r.contains((int)point.x,(int) point.y)) {
                    Log.d(TAG, "Touch IN");
                    bmpBinary.setPixel(x, y, Color.rgb(255, 255, 255));
                }
                else {
                    Log.d(TAG, "Touch OUT");
                    bmpBinary.setPixel(x, y, Color.rgb(0, 0, 0));
                }*/

                if(polygon.contains(point))
                {
                    bmpBinary.setPixel(x, y, Color.rgb(255, 255, 255));
                }
                //    else
                //   bmpBinary.setPixel(x, y, Color.rgb(0, 0, 0));




            }
        }

        return bmpBinary;
    }




   /* public Bitmap toBinary(Bitmap bmpOriginal) {
        int width, height, threshold;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();
        threshold = 127;
        Bitmap bmpBinary = Bitmap.createBitmap(bmpOriginal);



        for (int x = 0; x < bmpOriginal.getWidth(); x++) {
            for (int y = 0; y < bmpOriginal.getHeight(); y++) {


                for(int i=0;i<MainActivity.paths[0].getPoints().size();i++)
                {
                    if(MainActivity.paths[1].getPoints().get(i).x)
                }


                myBitmap.setPixel(x, y, Color.rgb(255, 255, 255));
            }
        }*/



      /*  for(int x = 0; x < width; ++x) {
            for(int y = 0; y < height; ++y) {
                // get one pixel color
                int pixel = bmpOriginal.getPixel(x, y);
                int red = Color.red(pixel);

                //get binary value
                if(red < threshold){
                    bmpBinary.setPixel(x, y, 0xFF000000);
                } else{
                    bmpBinary.setPixel(x, y, 0xFFFFFFFF);
                }

            }
        }
        return bmpBinary;
    }



   /* public boolean onTouch(View v, MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                downx = event.getX();
                downy = event.getY();
                clipPath = new Path();
                clipPath.moveTo(downx, downy);
                break;
            case MotionEvent.ACTION_MOVE:
                upx = event.getX();
                upy = event.getY();
                canvas.drawLine(downx, downy, upx, upy, paint);
                clipPath.lineTo(upx, upy);
                drawImageView.invalidate();
                downx = upx;
                downy = upy;
                break;
            case MotionEvent.ACTION_UP:
                upx = event.getX();
                upy = event.getY();
                canvas.drawLine(downx, downy, upx, upy, paint);
                clipPath.lineTo(upx, upy);
                drawImageView.invalidate();
                //cropImageByPath();
                break;
            case MotionEvent.ACTION_CANCEL:
                break;
            default:
                break;
        }
        return true;
    }*/








    private static final int PERMISSION_REQUEST_CODE = 200;
    private boolean checkPermission() {

        return ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ;
    }

    private void requestPermissionAndContinue() {
        if (ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, WRITE_EXTERNAL_STORAGE)
                    && ActivityCompat.shouldShowRequestPermissionRationale(this, READ_EXTERNAL_STORAGE)) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                alertBuilder.setCancelable(true);
                alertBuilder.setTitle("permission_necessary");
                alertBuilder.setMessage("storage_permission_is_encessary_to_wrote_event");
                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MarkingImageActivity.this, new String[]{WRITE_EXTERNAL_STORAGE
                                , READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                    }
                });
                AlertDialog alert = alertBuilder.create();
                alert.show();
                Log.e("", "permission denied, show dialog");
            } else {
                ActivityCompat.requestPermissions(MarkingImageActivity.this, new String[]{WRITE_EXTERNAL_STORAGE,
                        READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
            }
        } else {
            openActivity();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (permissions.length > 0 && grantResults.length > 0) {

                boolean flag = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {
                    openActivity();
                } else {
                    finish();
                }

            } else {
                finish();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void openActivity() {
        //add your further process after giving permission or to dow
        //
        // nload images from remote server.
    }

    public Runnable check_point = new Runnable()
    {
        @Override
        public void run()
        {
            while (true)
            {
                // TODO add code to refresh in background
                try
                {

                    Thread.sleep(1000);// sleeps 1 second
                    //Do Your process here.
                    if (point!=null)
                        check_touch_in_point(point);
                } catch (InterruptedException e){
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        }
    };




    public Bitmap convertBitmap(Bitmap bitmap) {
        Bitmap output =  Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        Paint colorFilterMatrixPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        colorFilterMatrixPaint.setColorFilter(new ColorMatrixColorFilter(new float[] {
                0, 0, 0, 255, 0,
                0, 0, 0, 255, 0,
                0, 0, 0, 255, 0,
                0, 0, 0, -255, 255
        }));
        canvas.drawBitmap(bitmap, 0, 0, colorFilterMatrixPaint);
        return output;
    }





   /* public void onDraw(Canvas canvas) {

        //  if (b != null)
        //   canvas.drawBitmap(b, 0, 0, null);

      //  canvas.drawBitmap(bmp, matrix, paint);
        //canvas=new Canvas();
       // canvas.drawBitmap(bmp, matrix, paint);

      // canvas = new Canvas(alteredBitmap);
   //  drawImageView.invalidate();
        // Path path = new Path();
        canvas.drawCircle(150, 150, 30, paint);

        for (int j = 0; j <= num_of_path; j++) {
            boolean first = true;
           /* if(paths[j].getPoints().size()==1)
            {
                Point point = paths[j].getPoints().get(0);
                canvas.drawCircle(point.x,point.y,15,paint);

            }
            Point next = null;
            Point prev = null;

            for (int i = 0; i < paths[j].getPoints().size() ; i += 1) {
                    Point point = paths[j].getPoints().get(i);
                    if (first) {
                        first = false;
                        paths[j].getPath().moveTo(point.x, point.y);
                    } else if (i < paths[j].getPoints().size() - 1) {
                        next = paths[j].getPoints().get(i + 1);
                        paths[j].getPath().quadTo(point.x, point.y, next.x, next.y);
                    } else {
                        paths[j].setMlastpoint(paths[j].getPoints().get(i));
                        paths[j].getPath().lineTo(point.x, point.y);
                    }
                canvas.drawCircle(point.x, point.y, 15, paint);
                    myview.draw(canvas);

                if(i>0) {
                        prev = paths[j].getPoints().get(i - 1);

                    }

                 if (prev != null) {
                        //canvas.drawCircle(point.x, point.y, 15, paint);
                        canvas.drawLine(prev.x, prev.y, point.x, point.y, paint);
                       // canvas.drawCircle(next.x, next.y, 15, paint);
                    }
               // drawImageView.invalidate();
                }

            // canvas.drawPath(paths[j].getPath(), paint);
        }
       // drawImageView.invalidate();

    }*/




    @Override
    public boolean onTouch(View view, MotionEvent event) {
        myview.onTouch(view,event);
        myview.invalidate();
       /* Point point = new Point();
        point.x = (int) event.getX();
        point.y = (int) event.getY();
        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:

                if(flgPathDraw&&!touch_in_point) {
                   check_touch_in_point(point);
                    Log.d("down action", "down action");

                    if (!touch_in_point) {
                        Log.d("down action", "down action");

                        add_point_to_path(point);
                      // onDraw(canvas);
                     //   myview.draw(canvas);
                      myview.invalidate();

                        //  myview.invalidate();
                        //drawImageView.invalidate();
                    // view.invalidate();
                        Log.e("Hi  ==>", "Size: " + point.x + " " + point.y);
                    }

                }
                break;
            case MotionEvent.ACTION_MOVE:
                Log.d("move action", "move action");

                //check_touch_in_point(point);

              drag(point);
                  //drawImageView.invalidate();

           // view.invalidate();

                break;

            case MotionEvent.ACTION_UP:
                // Log.d("Action up*******~~~~~~~>>>>", "called");
                paths[num_of_path].setMlastpoint(point);
                //paths[0].getPath().close();



                if (flgPathDraw) {
                    if(touch_in_point) {
                        touch_in_point = false;

                        drag_point=-1;
                    }

                    if (paths[num_of_path].getPoints().size() > 12) {
                        if (!comparepoint(paths[num_of_path].getMfirstpoint(), paths[num_of_path].getMlastpoint())) {
                            if(num_of_path==crop_num-2)
                            {
                                flgPathDraw = false;
                                // showcropdialog();

                            }
                            //cropImageByPath();
                            paths[num_of_path].getPoints().add(paths[num_of_path].getMfirstpoint());
                            //  num_of_path++;
                           // view.invalidate();
                            //drawImageView.invalidate();
                            onDraw(canvas);
                            // drawImageView.invalidate();
                            //cropflag=true;

                        }
                    }
                }
                break;
        }*/







        return true;
    }

    private boolean build_path()
    {


        if(paths[num_of_path].getPoints().size()>2) {
            Point p = new Point();
            p = paths[num_of_path].getPoints().get(0);
            paths[num_of_path].getPath().moveTo(p.x, p.y);
            for (int i = 1; i < paths[num_of_path].getPoints().size(); i++) {
                p = paths[num_of_path].getPoints().get(i);
                paths[num_of_path].getPath().lineTo(p.x, p.y);

            }
            paths[num_of_path].getPath().close();
            num_of_path++;
            add_polygon_flag = true;
            myview.invalidate();
            // touch_in_first_point=false;

            return true;
        }
        else
        {
            paths[num_of_path].getPoints().clear();
            myview.invalidate();
            //   touch_in_first_point=false;

            return false;
        }

    }

    private void add_point_to_path(Point point) {
        if (paths[num_of_path].isBfirstpoint()) {

          /*  if (comparepoint(paths[num_of_path].getMfirstpoint(), point)) {
                /// points.add(point);
                paths[num_of_path].getPoints().add(paths[num_of_path].getMfirstpoint());
                //*flgPathDraw = false;
                //  drawImageView.invalidate();
                // / showcropdialog();
                // / cropflag=true;
            } else {
                paths[num_of_path].getPoints().add(point);
                //  drawImageView.invalidate();
            }*/
            paths[num_of_path].getPoints().add(point);
            // paths[num_of_path].getPath().lineTo(point.x,point.y);
        }
        //else {
        //  paths[num_of_path].getPoints().add(point);
        //drawImageView.invalidate();
        // }

        if (!(paths[num_of_path].isBfirstpoint())) {

            paths[num_of_path].setMfirstpoint(point);
            // paths[num_of_path].getPath().moveTo(point.x,point.y);
            paths[num_of_path].getPoints().add(point);
            paths[num_of_path].setBfirstpoint(true);
            //drawImageView.invalidate();

        }
    }


    private boolean check_touch_in_point(Point point) {
        Log.d("check touch*~~~~~~~>>>>", "check touch");
        touch_in_point=false;


        for (int i = 0; i < paths[num_of_path].getPoints().size(); i++) {
            Point p1 = new Point();
            p1 = paths[num_of_path].getPoints().get(i);
            Log.e("point  ==>", "Size: " + point.x + " " + point.y);
            Log.e("p1  ==>", "Size: " + p1.x + " " + p1.y);


            double  distanceFromCenter = Math.sqrt(((p1.x - point.x) * (p1.x - point.x)) + ((p1.y - point.y) * (p1.y - point.y)));
            Log.e("distanceFromCenter  ==>", String.valueOf(distanceFromCenter));

            if (Math.abs(distanceFromCenter) < 30) {
                Log.e("exax  ==>", String.valueOf(Math.sqrt(3 ^ 2)));
                Log.e("exax -9 ==>", String.valueOf(Math.abs(-9)));

                Log.e("distanceFromCenter  ==>", String.valueOf(distanceFromCenter));

                //Touch within circle...
                Log.d(" touch in point", "touch in point");
                Log.e("point  ==>", "Size: " + point.x + " " + point.y);
                Log.e("p1  ==>", "Size: " + p1.x + " " + p1.y);


                touch_in_point = true;
                drag_point = i;
                break;


            }

        }

        // myview.invalidate();
        Log.d("check touch*~~~~~~~>>>>", String.valueOf(touch_in_point));

        return touch_in_point;
    }

    private void check_if_polygon() {


        if(paths[num_of_path].getPoints().size()>2)
        {
            build_path();
        }


    }


    private boolean drag(Point point) {
        if(touch_in_point&&drag_point!=-1)
        {
            Log.d("drag action","drag action");

            paths[num_of_path].getPoints().set(drag_point,point);
            // paths[num_of_path].getPoints().remove(paths[num_of_path].getPoints().get(drag_point));
            // paths[num_of_path].getPoints().add(point);
          /*  for(int i=0;i<paths[num_of_path].getPoints().size();i++)
            {
                Point p=paths[num_of_path].getPoints().get(i);
                if(p==point)
                {
                    drag_point=i;
                    break;

                }
            }*/
            myview.invalidate();
            // onDraw(canvas);

        }

        return touch_in_point;

    }



    private void showcropdialog() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent;
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        // Yes button clicked
                        // bfirstpoint = false;

                     //   intent = new Intent(getApplicationContext(),CropActivity.class);
                        //intent.putExtra("crop", true);
                      //  startActivity(intent);
                        break;

                   /* case DialogInterface.BUTTON_NEGATIVE:
                        // No button clicked

                        intent = new Intent(getApplicationContext(),CropActivity.class);
                        intent.putExtra("crop", false);
                        startActivity(intent);

                        paths[num_of_path].setBfirstpoint(false);
                        // resetView();

                        break;*/
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you Want to save Crop or Non-crop image?")
                .setPositiveButton("Crop", dialogClickListener).show()
                //.setNegativeButton("Non-crop", dialogClickListener).show()
                .setCancelable(false);
    }





    private boolean comparepoint(Point first, Point current) {
        int left_range_x = (int) (current.x - 3);
        int left_range_y = (int) (current.y - 3);

        int right_range_x = (int) (current.x + 3);
        int right_range_y = (int) (current.y + 3);

        if ((left_range_x < first.x && first.x < right_range_x)
                && (left_range_y < first.y && first.y < right_range_y)) {
            if (paths[num_of_path].getPoints().size() < 10) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }

    }




    /*private void cropImageByPath() {
        //closing the path now.
        paths[0].getPath().close();
        //setting the fill type to inverse, so that the outer part of the selected path gets filled.
        paths[0].getPath().setFillType(FillType.INVERSE_WINDING);
        Paint xferPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        xferPaint.setColor(Color.BLACK);
        canvas.drawPath(clipPath, xferPaint);
        xferPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawBitmap(alteredBitmap, 0, 0, xferPaint);
    }*/

    private void doCrop() {
        try {
            sourceBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageCaptureUri);

            // CropActivity.bitmap2=sourceBitmap;

            BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
            bmpFactoryOptions.inJustDecodeBounds = true;
            bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                    mImageCaptureUri), null, bmpFactoryOptions);
            bmpFactoryOptions.inJustDecodeBounds = false;
            bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                    mImageCaptureUri), null, bmpFactoryOptions);
            alteredBitmap = Bitmap.createBitmap(bmp.getWidth(), bmp
                    .getHeight(), bmp.getConfig());


            //*   canvas = new Canvas(alteredBitmap);
            //  canvas.drawColor(Color.BLUE);


            // myview.setLayoutParams(new LinearLayout.LayoutParams(bmp.getWidth(), bmp.getHeight()));

            //  paint = new Paint();
            //  paint.setColor(Color.GREEN);
            // paint.setStrokeWidth(5);
            paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            //     paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(5);
            paint.setColor(Color.GREEN);
            // paint.setPathEffect(new DashPathEffect(new float[] { 10, 20 }, 0));
            matrix = new Matrix();
            //*  canvas.drawBitmap(bmp, matrix, paint);
            //loading the image bitmap in image view





            // myview.setLayoutParams(new LinearLayout.LayoutParams(sourceBitmap.getWidth(), sourceBitmap.getHeight()));

      //    image= new BitmapDrawable(alteredBitmap);
           // drawImageView.setBackground(image);

        //   drawImageView.setImageBitmap(alteredBitmap);
            //myview.draw(canvas);

            // myview.invalidate();
            //setting the touch listener
            f=true;
            //   myview.invalidate();

            drawImageView.setOnTouchListener(this);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }











    class myview extends View implements View.OnTouchListener
    {

        Paint p11=new Paint();
        Canvas c;

        public myview(Context context) {
            super(context);

            p11.setColor(Color.GREEN);
            // this.setOnTouchListener(this);
//           canvas.drawColor(Color.BLACK);
            if(drawImageView!=null)
                drawImageView.setOnTouchListener(this);

            // TODO Auto-generated constructor stub
        }



        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            Rect rect=new Rect(drawImageView.getLeft(),drawImageView.getTop(),drawImageView.getRight(),drawImageView.getBottom());
          //  canvas.drawColor(Color.WHITE);
            if(bmp!=null)
            {
            /*    canvas = new Canvas(alteredBitmap);
                drawImageView.setImageBitmap(alteredBitmap);
               // canvas.drawBitmap (Bitmap bitmap, Rect source, Rect destination, Paint paint)*/

               // canvas.drawBitmap(bmp, 0,0, paint);
                //canvas.drawBitmap(image, 0, 0, paint);
                //canvas = new Canvas(alteredBitmap);
                //canvas=new Canvas();
               // drawImageView.draw(canvas);
              //  drawImageView.setImageBitmap(alteredBitmap);
            /*  canvas = new Canvas(alteredBitmap);
              canvas.drawColor(Color.YELLOW);

                canvas.drawBitmap(bmp, 0, 0, paint);
                drawImageView.setImageBitmap(alteredBitmap);*/

            }

        /*   if(bmp!=null) {

                int w = getWidth(), h = getHeight();
                canvas=new Canvas(alteredBitmap);
                // resize
                Matrix resize = new Matrix();
              //  resize.postScale((float)Math.min(w, h) / (float)bmp.getWidth(), (float)Math.min(w, h) / (float)bmp.getHeight());
                 resize.postScale((float)Math.min(w, h) / (float)bmp.getWidth(), (float)Math.min(w, h) / (float)bmp.getHeight());

                imageScaled = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), resize, false);

                //canvas.drawBitmap(imageScaled, 0,0, paint);
               canvas=new Canvas(imageScaled);

                drawImageView.setImageBitmap(imageScaled);
                if(drawImageView!=null)
                    drawImageView.setOnTouchListener(this);




                //   canvas = new Canvas(alteredBitmap);

                //canvas = new Canvas(bmp.copy(Bitmap.Config.ARGB_8888, true));

                //drawImageView.setImageBitmap(alteredBitmap);

            }


         /*   if(bmp!=null)
              //  canvas.drawBitmap(bmp, matrix, paint);
              // canvas = new Canvas(bmp.copy(Bitmap.Config.ARGB_8888, true));
              // canvas.drawBitmap(bmp, 0, 0, paint);
               // matrix = new Matrix();
            canvas.drawBitmap(bmp, matrix, paint);
            if(alteredBitmap!=null) {
                //canvas = new Canvas(alteredBitmap);
                canvas = new Canvas(alteredBitmap);
                drawImageView.setImageBitmap(alteredBitmap);
                //canvas = new Canvas(bmp.copy(Bitmap.Config.ARGB_8888, true));

             //   drawImageView.setImageBitmap(alteredBitmap);

            }*/




            //  for (int j = 0; j <= num_of_path; j++) {
            boolean first = true;

            Point next = null;
            Point prev = null;

            for (int i = 0; i < paths[num_of_path].getPoints().size(); i += 1) {
                Point point = paths[num_of_path].getPoints().get(i);
                if (first) {
                    first = false;
                    //    paths[num_of_path].getPath().moveTo(point.x, point.y);
                } else if (i < paths[num_of_path].getPoints().size() - 1) {
                    next = paths[num_of_path].getPoints().get(i + 1);
                    //  paths[num_of_path].getPath().quadTo(point.x, point.y, next.x, next.y);
                } else {
                    //  paths[num_of_path].setMlastpoint(paths[num_of_path].getPoints().get(i));
                    //   paths[num_of_path].getPath().lineTo(point.x, point.y);
                }
                canvas.drawCircle(point.x, point.y, 15, paint);

                if (i > 0) {
                    prev = paths[num_of_path].getPoints().get(i - 1);

                }

                if (prev != null) {


                    canvas.drawLine(prev.x, prev.y, point.x, point.y, paint);
                }
            }





            // paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            //  paint.setStyle(Paint.Style.STROKE);
            //  paint.setStrokeWidth(5);
            //  paint.setColor(Color.GREEN);
            // paint.setPathEffect(new DashPathEffect(new float[] { 10, 20 }, 0));
            for(int k=0;k<num_of_path;k++) {
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeWidth(5);

                canvas.drawPath(paths[k].getPath(), paint);
            }
            if(paint!=null)
                paint.setStyle(Paint.Style.FILL);

            //  paint.setStyle(Paint.Style.FILL_AND_STROKE);
        }

        @Override
        public void setOnTouchListener(OnTouchListener l) {
            super.setOnTouchListener(l);
        }



        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            point = new Point();
            Point point_d = new Point();

            point.x = (int) motionEvent.getX();
            point.y = (int) motionEvent.getY();


            switch (motionEvent.getAction())
            {

                case MotionEvent.ACTION_DOWN:
                    point_d.x = (int) motionEvent.getX();
                    point_d.y = (int) motionEvent.getY();
                    check_touch_in_first_point(point_d);
                    f=  check_touch_in_point(point_d);
                    if(!touch_in_point&&!touch_in_first_point)
                    {
                        Log.d("Action down*~~~~~~~>>>>", "not touched");
                        Log.e("Hi  ==>", "Size: " + point_d.x + " " + point_d.y);

                        add_point_to_path(point_d);
                        invalidate();



                    }
                    // check_touch_in_point(point);

                    Log.d("Action down*~~~~~~~>>>>", String.valueOf(f));





                    break;

                case MotionEvent.ACTION_MOVE:
                    Log.d("view move action", "view move action");

                    Point move_point=new Point();

                    move_point.x=(int) motionEvent.getX();
                    move_point.y = (int) motionEvent.getY();
                    if(!touch_in_point)
                        check_touch_in_point(point_d);

                    if(touch_in_point) {
                        Log.d("view move action", "view move action");

                        drag(point);
                        invalidate();
                    }
                    break;

                case MotionEvent.ACTION_UP:
                    //touch_in_first_point=false;

                    touch_in_first_point=false;

                    Log.d("Action up*~~~~~~~>>>>", "called");
                    if(flgPathDraw) {

                        if (num_of_path == crop_num - 1) {
                            flgPathDraw = false;

                        }


                        else if(touch_in_point)
                        {
                            Log.d("Action up*~~~~~~~>>>>", "touched");

                            touch_in_point = false;
                            drag_point=-1;
                        }




                        invalidate();


                    }



                    // drag_point = -1;


                /*    if (flgPathDraw) {

                     //   if(add_polygon_flag) {

                          //  if (!comparepoint(paths[num_of_path-1].getMfirstpoint(), paths[num_of_path-1].getMlastpoint())) {
                                if (num_of_path == crop_num - 1) {
                                    flgPathDraw = false;

                                }
                               // paths[num_of_path-1].getPoints().add(paths[num_of_path-1].getMfirstpoint());
                              //  add_polygon_flag=false;
                         //   }
                       // }
                    }

                      invalidate();*/

                    break;


            }
            return true;
        }
    }

    private void check_touch_in_first_point(Point point_d) {

        if(paths[num_of_path].getMfirstpoint()!=null) {
            Log.e("first point  ==>", "Size: " + paths[num_of_path].getMfirstpoint() + " " + paths[num_of_path].getMfirstpoint());

            double distanceFromCenter = Math.sqrt(((paths[num_of_path].getMfirstpoint().x - point_d.x) * (paths[num_of_path].getMfirstpoint().x - point_d.x)) + ((paths[num_of_path].getMfirstpoint().y - point_d.y) * (paths[num_of_path].getMfirstpoint().y - point_d.y)));

            if (distanceFromCenter < 30) {
                Log.e("p1  ==>", "first point touch");
                touch_in_first_point=true;

                check_if_polygon();

            }
        }

    }





}
