package com.igam.shira.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubmissionDetails extends AppCompatActivity {

    private TextView SubmissionNumber,changeSub;

    String submissionNum;
    final static String SUBMISSION="הגשה מספר";

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private ProgressDialog mProgress;
    private TextView status1;
    private String status;
    private String weekNum = "EE";
    private Button submit;
    Spinner spinner;
    private String gUid;
    private List<File> pictures=new ArrayList<>();

    private String pathweek;

    private String status3;
    private String rate;
    private EditText comment;
    private Spinner statusSpinner;
    private Spinner rateSpinner;
    private String commentReview;
    private String user_guidf;





    //Calendar cal = Calendar.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submission_details);

        user_guidf=getIntent().getStringExtra("user_Uid");


        gUid= getIntent().getStringExtra("Uid");
       weekNum=getIntent().getStringExtra("submission");
       // weekNum="0";

        pathweek="mouth"+weekNum;

        DocumentReference docRef = db.collection("users-data").document(user_guidf);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        //     Log.i("LOGGER","First "+document.getString("first_entry"));

                        Map<String, Object> map = (Map) document.get("mouthReviewSubmissions");
                        Map<String, Object> map1 = (Map) map.get(pathweek);

                        status = (String) map1.get("status");
                        status1.setText(status);
                        Log.e(" status00", String.valueOf(status));

                    } else {
                        Log.d("LOGGER", "No such document");
                    }
                }
                else{
                    Log.d("LOGGER", "get failed with ", task.getException());
                }
            }


        });




        statusSpinner=(Spinner)findViewById(R.id.status1);
        rateSpinner=(Spinner)findViewById(R.id.rate);
       // comment=(EditText) findViewById(R.id.editText);
        status1=(TextView)findViewById(R.id.statusID);
        submit=(Button)findViewById(R.id.apply);
       // submissionNum= getIntent().getStringExtra("submission");
        submissionNum="0";
        SubmissionNumber=(TextView) findViewById(R.id.cameraSecID);
        changeSub=(TextView) findViewById(R.id.changeSub);
        int number= Integer.parseInt(submissionNum)+1;
        SubmissionNumber.setTextColor(Color.BLACK);
        SubmissionNumber.setTextSize(30);
        SubmissionNumber.setText(SUBMISSION+" "+number);
        SubmissionNumber.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
        Log.e(" status88", String.valueOf(status));














    }
public void save(View view)
{
    status3= statusSpinner.getSelectedItem().toString();
    rate=rateSpinner.getSelectedItem().toString();
  //  commentReview = comment.getText().toString();

    DocumentReference docRef = db.collection("users-data").document(user_guidf);
    docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
        @Override
        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document .exists()) {
                    //     Log.i("LOGGER","First "+document.getString("first_entry"));
                    Map<String, Object> map = (Map) document.get("mouthReviewSubmissions");
                    Map<String, Object> map1 = (Map) map.get(pathweek);
                    Map<String,Object>map2=new HashMap<>();
                    map1.put("status",status3);
                    map1.put("rate",rate);
                    map1.put("reviewerComments",commentReview);
                    map.put(pathweek,map1);
                    map2.put("mouthReviewSubmissions",map);
                    db.collection("users-data").document(user_guidf).update(map2);

                } else {
                    Log.d("LOGGER", "No such document");
                }
            } else {
                Log.d("LOGGER", "get failed with ", task.getException());
            }
        }
    });
}


    public void pictures(View view)
    {
        Intent intent = new Intent(SubmissionDetails.this, Images.class);
        intent.putExtra("Uid", gUid);
        intent.putExtra("user_gUid", user_guidf);
        intent.putExtra("weekNum", weekNum);
        startActivity(intent);
    }





}
