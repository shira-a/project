package com.igam.shira.myapplication;

import android.app.ActionBar;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OralHygieneActivity extends AppCompatActivity implements MultiSelectionSpinner.OnMultipleItemsSelectedListener {
    private Map<String, Object> GeneralForm = new HashMap<>();
    private Map<String, Object> healthForm = new HashMap<>();
    private Map<String, Object> ApplicationFormData = new HashMap<>();
    private Map<String, Object> ApplicationFormData1 = new HashMap<>();
    private Map<String, Object> mouthHealthForm= new HashMap<>();
    private FirebaseFirestore db =FirebaseFirestore.getInstance();
    private boolean isBrushingTeeth=false,brushteeth=false;
    private String brushingTeethInterval,suggestedBrushingIntervalInDay,medicalThreadUsage,mouthWashUsage,gUid;
    private String lastVisitToDental,lastVisitToHygienist,recommendedVisitIntervalToDentist_,recommendedVisitIntervalToHygienist_;
    private List<String> lastVisitToDentalReason;
    private EditText recommendedVisitIntervalToDentist,recommendedVisitIntervalToHygienist;
    private TextView frequency;
    private View Divider;
    private LinearLayout BrushFrequency;
    private Spinner suggestedBrushingIntervalInDaySpinner,medicalThreadUsageSpinner,mouthWashUsageSpinner,lastVisitToHygienistSpinner,lastVisitToDentalSpinner,brushingTeethIntervalSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oral_hygiene);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        ((TextView)getSupportActionBar().getCustomView().findViewById(R.id.tvTitle)).setText("שאלון הרשמה");
        brushingTeethIntervalSpinner = (Spinner) findViewById(R.id.brushingTeethInterval);
        BrushFrequency=(LinearLayout)findViewById(R.id.brushfrequence) ;
        BrushFrequency.setVisibility(View.GONE);
        frequency=(TextView)findViewById(R.id.text70);
        Divider=findViewById(R.id.divider700);
        Divider.setVisibility(View.GONE);
        suggestedBrushingIntervalInDaySpinner = (Spinner) findViewById(R.id.suggestedBrushingIntervalInDay);
        medicalThreadUsageSpinner = (Spinner) findViewById(R.id.medicalThreadUsage);
        mouthWashUsageSpinner = (Spinner) findViewById(R.id.mouthWashUsage);
        lastVisitToDentalSpinner = (Spinner) findViewById(R.id.lastVisitToDental);
        lastVisitToHygienistSpinner=(Spinner) findViewById(R.id.lastVisitToHygienist);

        GeneralForm= (Map<String, Object>) getIntent().getSerializableExtra("generalForm");
        healthForm= (Map<String, Object>) getIntent().getSerializableExtra("healthForm");

        Log.d("generalForm", String.valueOf(GeneralForm));
        Log.d("healthForm", String.valueOf(healthForm));
        gUid= getIntent().getStringExtra("Uid");
        recommendedVisitIntervalToDentist=findViewById(R.id.recommendedVisitIntervalToDentist);
        recommendedVisitIntervalToHygienist=findViewById(R.id.recommendedVisitIntervalToHygienist);
        String[] array = {"בדיקה שגרתית תקופתית על ידי רופא שיניים", "טיפול על ידי שיננית", "טיפול כמו סתימה, עקירה, טיפול שורש", "טיפול כמו גשר תותבות יישור שיניים השתלת שיניים"};
        MultiSelectionSpinner multiSelectionSpinner = (MultiSelectionSpinner) findViewById(R.id.mySpinner);
        multiSelectionSpinner.setItems(array);
        multiSelectionSpinner.setListener(this);
    }

    @Override
    public void selectedIndices(List<Integer> indices) {
    }

    @Override
    public void selectedStrings(List<String> strings) {
        lastVisitToDentalReason = strings;
    }

    public void isChecked(View v)
    {
        if(brushteeth==false)
        {
            BrushFrequency.setVisibility(View.VISIBLE);
            Divider.setVisibility(View.VISIBLE);
            brushteeth=true;
        }
        else
        {
            BrushFrequency.setVisibility(View.GONE);
            Divider.setVisibility(View.GONE);
            brushteeth=false;
        }
    }
    public void SaveAndStart(View v)
    {
        final CheckBox BrushingTeeth=(CheckBox)findViewById(R.id.isBrushingTeeth);
        if(BrushingTeeth.isChecked()) {
            isBrushingTeeth=true;
        }

        brushingTeethInterval= brushingTeethIntervalSpinner.getSelectedItem().toString();
        suggestedBrushingIntervalInDay= suggestedBrushingIntervalInDaySpinner.getSelectedItem().toString();
        medicalThreadUsage= medicalThreadUsageSpinner.getSelectedItem().toString();
        mouthWashUsage= mouthWashUsageSpinner.getSelectedItem().toString();
        lastVisitToDental= lastVisitToDentalSpinner.getSelectedItem().toString();
        lastVisitToHygienist=lastVisitToHygienistSpinner.getSelectedItem().toString();
        recommendedVisitIntervalToDentist_ = recommendedVisitIntervalToDentist.getText().toString();
        recommendedVisitIntervalToHygienist_ = recommendedVisitIntervalToHygienist.getText().toString();

        if(validation()) {
            save();
            Intent intent = new Intent(OralHygieneActivity.this, Treatment_progress.class);
            intent.putExtra("Uid", gUid);
            startActivity(intent);
        }
        else
            Toast.makeText(OralHygieneActivity.this, "נא הכנס פרטים",
                    Toast.LENGTH_SHORT).show();
    }
    public boolean validation()//checks if all fields are filled
    {
        if( brushteeth==true&&(brushingTeethInterval==null||brushingTeethInterval.isEmpty()))
        {
            return false;
        }
        if(suggestedBrushingIntervalInDay==null||suggestedBrushingIntervalInDay.isEmpty())
        {
            return false;
        }
        if(medicalThreadUsage==null||medicalThreadUsage.isEmpty())
        {
            return false;
        }
        if(mouthWashUsage==null||mouthWashUsage.isEmpty())
        {
            return false;
        }
        if(lastVisitToDental==null||lastVisitToDental.isEmpty())
        {
            return false;
        }
        if(lastVisitToHygienist==null||lastVisitToHygienist.isEmpty())
        {
            return false;
        }
        if(recommendedVisitIntervalToHygienist_.isEmpty())
        {
            return false;
        }
        if(recommendedVisitIntervalToDentist_.isEmpty())
        {
            return false;
        }
        if(lastVisitToDentalReason==null||lastVisitToDentalReason.isEmpty())
        {
            return false;
        }
        return true;
    }

    private void save() {
        mouthHealthForm.put("isBrushingTeeth", isBrushingTeeth);
        mouthHealthForm.put("brushingTeethInterval", brushingTeethInterval);
        mouthHealthForm.put("suggestedBrushingIntervalInDay", suggestedBrushingIntervalInDay);
        mouthHealthForm.put("medicalThreadUsage", medicalThreadUsage);
        mouthHealthForm.put("mouthWashUsage", mouthWashUsage);
        mouthHealthForm.put("lastVisitToDental", lastVisitToDental);
        mouthHealthForm.put("lastVisitToDentalReason", lastVisitToDentalReason.toString());
        mouthHealthForm.put("recommendedVisitIntervalToDentist",recommendedVisitIntervalToDentist_);
        mouthHealthForm.put("lastVisitToHygienist",lastVisitToHygienist);
        mouthHealthForm.put("recommendedVisitIntervalToHygienist",recommendedVisitIntervalToHygienist_);
        ApplicationFormData.put("healthForm", healthForm);
        ApplicationFormData.put("generalForm", GeneralForm);
        ApplicationFormData.put("mouthHealthForm", mouthHealthForm);
        ApplicationFormData1.put("applicationFormData",ApplicationFormData);
        db.collection("users-data").document(gUid).update(ApplicationFormData1)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e(" successfully written!", "o");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("Error writing document", "o");
                    }
                });

        DocumentReference docRef = db.collection("users-data").document(gUid);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null) {
                        Log.i("LOGGER","First "+document.getString("role"));
                        Log.i("LOGGER","second "+document.getString("role"));

                    } else {
                        Log.d("LOGGER", "No such document");
                    }
                } else {
                    Log.d("LOGGER", "get failed with ", task.getException());
                }
            }
        });
    }
}
