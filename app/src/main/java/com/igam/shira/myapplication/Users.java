package com.igam.shira.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class Users extends AppCompatActivity implements ListView.OnClickListener {

    private FirebaseFirestore db =FirebaseFirestore.getInstance();
   private List<String> list;
    LinearLayout linearLayout;
    RelativeLayout relativeLayout;
    TextView countOfUseres;
    ArrayAdapter adapter;
    ListView listView;
    Button add;
    SparseBooleanArray mCheckStates ;
    private static final String SUM= "סך הכל:";
    private ProgressDialog mProgress;
    int count=0;
    private String gUid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        setTitle("משתמשים");
       // add = (Button)findViewById(R.id.button2);
        gUid = getIntent().getStringExtra("Uid");
        mProgress = new ProgressDialog(this);
        list = new ArrayList<>();

         linearLayout = new LinearLayout(this);
       // setContentView(linearLayout);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        countOfUseres = (TextView)findViewById(R.id.count_of_users);
        EditText filter=(EditText)findViewById(R.id.searchFilter);
         //list.add("dddd");
       /* add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Users.this, Details.class);


                // ---------------------------------------------------------------------
                //    After successful login. move to TakeAPic
                // ---------------------------------------------------------------------
                startActivity(intent);
            }
        });*/






  mProgress.setMessage("Loading...");
  mProgress.show();

        db.collection("users-data").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    String size= String.valueOf(task.getResult().size());

                    countOfUseres.setText(SUM+""+size);
                  //  linearLayout.addView(countOfUseres);
                    countOfUseres.setTextSize(20);
                    countOfUseres.setTextColor(Color.BLACK);
                    countOfUseres.setGravity(Gravity.CENTER);



                    for (QueryDocumentSnapshot document : task.getResult()) {
                        String s=document.getId();
                        list.add(s);
                        Log.d("id",document.getId());
                       // display(document.getId());
                        Log.d("size", String.valueOf(list.size()));


                    }
                     listView=(ListView)findViewById(R.id.theList);
                    adapter=new ArrayAdapter(Users.this,android.R.layout.simple_list_item_1,list);
                    listView.setAdapter(adapter);
                    mProgress.dismiss();
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(Users.this, User_Detailes.class);

                            intent.putExtra("user_Uid", ((TextView) view).getText());
                            intent.putExtra("Uid",gUid);
                            Log.d("the user", String.valueOf(((TextView) view).getText()));
                            startActivity(intent);
                        }
                    });



                    Log.d("TAG", list.toString());
                } else {
                    Log.d("TAG", "Error getting documents: ", task.getException());
                }
            }
        });

        // count_();

        filter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu( Menu menu) {
        // Inflate the main_menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.admin_menue, menu);
        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.item1:
                Intent intent = new Intent(Users.this, Treatment_progress.class);
                startActivity(intent);
                break;
            case R.id.item2:
                Intent intent1 = new Intent(Users.this, Tutorials.class);
                startActivity(intent1);
                break;
            case R.id.item4:
                Intent intent2 = new Intent(Users.this, Users.class);
                startActivity(intent2);
                break;
            case R.id.edit_person:
                Intent intent3= new Intent(Users.this, User_Detailes.class);
                intent3.putExtra("new_user", "true");

                startActivity(intent3);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

        @Override
    public void onClick(View view) {

    }







}
