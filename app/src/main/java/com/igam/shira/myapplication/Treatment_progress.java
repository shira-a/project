package com.igam.shira.myapplication;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Treatment_progress extends AppCompatActivity {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    private StorageReference mStorageRef;
    static final int REQUEST_IMAGE_CAPTURE = 111;
    private ProgressDialog mProgress;
    private String weekNum = "EE",gUid,role,timeLeft;

    private Map<String, Object> mouthReviewSubmissions1 = new HashMap<>();
    private Map<String, Object> mouthReviewSubmissions = new HashMap<>();
    private Map<String, Object> mouth1 = new HashMap<>();
    private Map<String, Object> mouth2 = new HashMap<>();
    private Map<String, Object> mouth3 = new HashMap<>();
    private Map<String, Object> mouth4 = new HashMap<>();
    private Map<String, Object> mouth5 = new HashMap<>();
    private Map<String, Object> mouth6= new HashMap<>();
    private Map<String, Object> mouth7= new HashMap<>();
    private Map<String, Object> mouth8= new HashMap<>();

    private Date unlockDate,updateDate;
    private ImageButton week0,week1,week2,week3,week4,week5,week6,week7;
    private boolean w0=true,w1=true,w2=true,w3=true,w4=true,wl=true,w5=true,w6=true,w7=true;
    private boolean p0=true,p1=true,p2=true,p3=true,p4=true,p5=true,p6=true,p7=true,pl=true;
    private Date d1,d2,d3,d4,d5,d6,d7,d8;
    private String y1,y2,y3,y4,y5,y6,y7,y8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        ((TextView)getSupportActionBar().getCustomView().findViewById(R.id.tvTitle)).setText("לוח מעקב");
        gUid = getIntent().getStringExtra("Uid");
        setContentView(R.layout.activity_treatment_progress);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mProgress = new ProgressDialog(this);
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            //  takePictureButton.setEnabled(false);
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }



        final DocumentReference docRef = db.collection("users-data").document(gUid);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document .exists()) {
                        //     Log.i("LOGGER","First "+document.getString("first_entry"));
                        Boolean b;
                        b = (Boolean) document.get("isCompleted");
                        if(b==null)
                            b=false;
                        //if the field is Boolean

                        if (b) {//if its not a new user
                            updateDate=  Calendar.getInstance().getTime();//current date and time
                            final DocumentReference docRef1 = db.collection("users-data").document(gUid);
                            docRef1.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        DocumentSnapshot document1 = task.getResult();
                                        if (document1 .exists()) {
                                            Log.d("LOGGER", " such document");
                                            Map<String, Object> map = (Map) document1.get("mouthReviewSubmissions");
                                            Map<String, Object> map1 = (Map) map.get("mouth0");//one for each week
                                            Map<String, Object> map2 = (Map) map.get("mouth1");
                                            Map<String, Object> map3 = (Map) map.get("mouth2");
                                            Map<String, Object> map4 = (Map) map.get("mouth3");
                                            Map<String, Object> map5 = (Map) map.get("mouth4");
                                            Map<String, Object> map6 = (Map) map.get("mouth5");
                                            Map<String, Object> map7 = (Map) map.get("mouth6");
                                            Map<String, Object> map8 = (Map) map.get("mouth7");

                                            d1=(Date)map1.get("unlockDate");
                                            timeLeft=ReturnTimeLeft(d1,updateDate);
                                            TextView tx=(TextView)findViewById(R.id.timeLeft1);

                                            if(timeLeft!="לא הוגש")
                                            {
                                                tx.setText(timeLeft);
                                                w0=false;
                                            }
                                            else
                                            {
                                                y1 = (String) map1.get("status");
                                                tx.setText(y1);
                                                week0=(ImageButton)findViewById(R.id.imageButtonWeek0);
                                                if (y1.equals("לא הוגש"))
                                                    week0.setBackgroundColor(Color.RED);
                                                if (y1.equals("נדרש תיקון"))
                                                    week0.setBackgroundColor(Color.YELLOW);
                                                if(y1.equals("ממתין לבדיקה"))
                                                {
                                                    p0=false;
                                                    week0.setBackgroundColor(Color.parseColor("#ffff8800"));
                                                }
                                                if(y1.equals("נבדק"))
                                                {
                                                    p0=false;
                                                    week0.setBackgroundColor(Color.GREEN);
                                                }
                                            }

                                            d2=(Date)map2.get("unlockDate");
                                            timeLeft=ReturnTimeLeft(d2,updateDate);
                                            TextView to=(TextView)findViewById(R.id.timeLeft2);

                                            if(timeLeft!="לא הוגש")
                                            {
                                                to.setText(timeLeft);
                                                w1=false;
                                            }
                                            else
                                            {
                                                y2=(String)map2.get("status");
                                                to.setText(y2);
                                                week1=(ImageButton)findViewById(R.id.imageButtonWeek1);
                                                if (y2.equals("לא הוגש"))
                                                    week1.setBackgroundColor(Color.RED);
                                                if (y2.equals("נדרש תיקון"))
                                                    week1.setBackgroundColor(Color.YELLOW);

                                                if(y2.equals("ממתין לבדיקה"))
                                                {
                                                    p1=false;
                                                    week1.setBackgroundColor(Color.parseColor("#ffff8800"));
                                                }
                                                if(y2.equals("נבדק"))
                                                {
                                                    p1=false;
                                                    week1.setBackgroundColor(Color.GREEN);
                                                }
                                            }

                                            d3=(Date)map3.get("unlockDate");
                                            timeLeft=ReturnTimeLeft(d3,updateDate);
                                            TextView tt=(TextView)findViewById(R.id.timeLeft3);

                                            if(timeLeft!="לא הוגש")
                                            {
                                                tt.setText(timeLeft);
                                                w2=false;
                                            }
                                            else
                                            {
                                                y3=(String)map3.get("status");
                                                tt.setText(y3);
                                                week2=(ImageButton)findViewById(R.id.imageButtonWeek2);
                                                if (y3.equals("לא הוגש"))
                                                    week2.setBackgroundColor(Color.RED);
                                                if (y3.equals("נדרש תיקון"))
                                                    week2.setBackgroundColor(Color.YELLOW);

                                                if(y3.equals("ממתין לבדיקה"))
                                                {
                                                    p2=false;
                                                    week2.setBackgroundColor(Color.parseColor("#ffff8800"));
                                                }
                                                if(y3.equals("נבדק"))
                                                {
                                                week2.setBackgroundColor(Color.GREEN);
                                                p2 = false;
                                                }
                                            }

                                            d4=(Date)map4.get("unlockDate");
                                            timeLeft=ReturnTimeLeft(d4,updateDate);
                                            TextView tr=(TextView)findViewById(R.id.timeLeft4);

                                            if(timeLeft!="לא הוגש")
                                            {
                                                tr.setText(timeLeft);
                                                w3=false;
                                            }
                                            else
                                            {
                                                y4=(String)map4.get("status");
                                                tr.setText(y4);
                                                week3=(ImageButton)findViewById(R.id.imageButtonWeek3);
                                                if (y4.equals("לא הוגש"))
                                                    week3.setBackgroundColor(Color.RED);
                                                if (y4.equals("נדרש תיקון"))
                                                    week3.setBackgroundColor(Color.YELLOW);
                                                if(y4.equals("ממתין לבדיקה"))
                                                {
                                                    p3=false;
                                                    week3.setBackgroundColor(Color.parseColor("#ffff8800"));
                                                }
                                                if(y4.equals("נבדק"))
                                                {
                                                week3.setBackgroundColor(Color.GREEN);
                                                p3 = false;
                                                }
                                            }

                                            d5=(Date)map5.get("unlockDate");
                                            timeLeft=ReturnTimeLeft(d5,updateDate);
                                            TextView th=(TextView)findViewById(R.id.timeLeft5);

                                            if(timeLeft!="לא הוגש")
                                            {
                                                th.setText(timeLeft);
                                                w4=false;
                                            }
                                            else
                                            {
                                                y5=(String)map5.get("status");
                                                th.setText(y5);
                                                week4=(ImageButton)findViewById(R.id.imageButtonWeek4);
                                                if (y5.equals("לא הוגש"))
                                                    week4.setBackgroundColor(Color.RED);
                                                if (y5.equals("נדרש תיקון"))
                                                    week4.setBackgroundColor(Color.YELLOW);
                                                if(y5.equals("ממתין לבדיקה"))
                                                {
                                                    p4=false;
                                                    week4.setBackgroundColor(Color.parseColor("#ffff8800"));
                                                }
                                                if(y5.equals("נבדק"))
                                                {
                                                    week4.setBackgroundColor(Color.GREEN);
                                                    p4 = false;
                                                }
                                            }

                                            d6=(Date)map6.get("unlockDate");
                                            timeLeft=ReturnTimeLeft(d6,updateDate);
                                            TextView tp=(TextView)findViewById(R.id.timeLeft6);

                                            if(timeLeft!="לא הוגש")
                                            {
                                                tp.setText(timeLeft);
                                                w5=false;
                                            }
                                            else
                                            {
                                                y6=(String)map6.get("status");
                                                tp.setText(y6);
                                                week5=(ImageButton)findViewById(R.id.imageButtonWeek5);
                                                if (y6.equals("לא הוגש"))
                                                    week5.setBackgroundColor(Color.RED);
                                                if (y6.equals("נדרש תיקון"))
                                                    week5.setBackgroundColor(Color.YELLOW);
                                                if(y6.equals("ממתין לבדיקה"))
                                                {
                                                    p5=false;
                                                    week5.setBackgroundColor(Color.parseColor("#ffff8800"));
                                                }
                                                if(y6.equals("נבדק"))
                                                {
                                                    week5.setBackgroundColor(Color.GREEN);
                                                    p5 = false;
                                                }
                                            }

                                            d7=(Date)map7.get("unlockDate");
                                            timeLeft=ReturnTimeLeft(d7,updateDate);
                                            TextView tl=(TextView)findViewById(R.id.timeLeft7);

                                            if(timeLeft!="לא הוגש")
                                            {
                                                tl.setText(timeLeft);
                                                w6=false;
                                            }
                                            else
                                            {
                                                y7=(String)map7.get("status");
                                                tp.setText(y7);
                                                week6=(ImageButton)findViewById(R.id.imageButtonWeek6);
                                                if (y7.equals("לא הוגש"))
                                                    week6.setBackgroundColor(Color.RED);
                                                if (y7.equals("נדרש תיקון"))
                                                    week6.setBackgroundColor(Color.YELLOW);
                                                if(y7.equals("ממתין לבדיקה"))
                                                {
                                                    p6=false;
                                                    week6.setBackgroundColor(Color.parseColor("#ffff8800"));
                                                }
                                                if(y7.equals("נבדק"))
                                                {
                                                    week6.setBackgroundColor(Color.GREEN);
                                                    p6 = false;
                                                }
                                            }

                                            d8=(Date)map8.get("unlockDate");
                                            timeLeft=ReturnTimeLeft(d8,updateDate);
                                            TextView tk=(TextView)findViewById(R.id.timeLeft8);

                                            if(timeLeft!="לא הוגש")
                                            {
                                                tk.setText(timeLeft);
                                                w7=false;
                                            }
                                            else
                                            {
                                                y8=(String)map8.get("status");
                                                tk.setText(y8);
                                                week7=(ImageButton)findViewById(R.id.imageButtonWeek7);
                                                if (y8.equals("לא הוגש"))
                                                    week7.setBackgroundColor(Color.RED);
                                                if (y8.equals("נדרש תיקון"))
                                                    week7.setBackgroundColor(Color.YELLOW);
                                                if(y8.equals("ממתין לבדיקה"))
                                                {
                                                    p7=false;
                                                    week7.setBackgroundColor(Color.parseColor("#ffff8800"));
                                                }
                                                if(y8.equals("נבדק"))
                                                {
                                                    week7.setBackgroundColor(Color.GREEN);
                                                    p7 = false;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Log.d("LOGGER", "No such document");
                                        }
                                    }
                                    else
                                    {
                                        Log.d("LOGGER", "get failed with ", task.getException());
                                    }
                                }
                            });
                        }
                        else if (!b) {//if its the first time logging in - new user

                            db.collection("users-data").document(gUid).update("isCompleted", true)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.e(" successfully written!", "o");
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.e("Error writing document", "o");
                                        }
                                    });

                            updateDate=  Calendar.getInstance().getTime();
                            timeLeft =ReturnTimeLeft(updateDate,updateDate);
                            TextView tx=(TextView)findViewById(R.id.timeLeft1);
                            tx.setText(timeLeft);

                            week0=(ImageButton)findViewById(R.id.imageButtonWeek0);
                            if (timeLeft.equals("לא הוגש"))
                                week0.setBackgroundColor(Color.RED);
                            if(timeLeft!="לא הוגש")
                                w0=false;

                            mouth1.put("updateDate",updateDate);
                            mouth1.put("unlockDate",updateDate);
                            mouth1.put("status","לא הוגש");
                            mouth1.put("sequenceNumber",1);

                            unlockDate=new Date(updateDate.getYear(),updateDate.getMonth(),updateDate.getDate()+7,updateDate.getHours(),updateDate.getMinutes(),updateDate.getSeconds());
                            timeLeft =ReturnTimeLeft(unlockDate,updateDate);
                            TextView tv=(TextView)findViewById(R.id.timeLeft2);
                            tv.setText(timeLeft);

                            if(timeLeft!="לא הוגש")
                                w1=false;

                            mouth2.put("updateDate",updateDate);
                            mouth2.put("unlockDate",unlockDate);
                            mouth2.put("status","לא הוגש");
                            mouth2.put("sequenceNumber",2);

                            unlockDate=new Date(updateDate.getYear(),updateDate.getMonth(),updateDate.getDate()+14,updateDate.getHours(),updateDate.getMinutes(),updateDate.getSeconds());
                            timeLeft =ReturnTimeLeft(unlockDate,updateDate);
                            TextView tz=(TextView)findViewById(R.id.timeLeft3);
                            tz.setText(timeLeft);

                            if(timeLeft!="לא הוגש")
                                w2=false;

                            mouth3.put("updateDate",updateDate);
                            mouth3.put("unlockDate",unlockDate);
                            mouth3.put("status","לא הוגש");
                            mouth3.put("sequenceNumber",3);

                            unlockDate=new Date(updateDate.getYear(),updateDate.getMonth(),updateDate.getDate()+21,updateDate.getHours(),updateDate.getMinutes(),updateDate.getSeconds());
                            timeLeft =ReturnTimeLeft(unlockDate,updateDate);
                            TextView ts=(TextView)findViewById(R.id.timeLeft4);
                            ts.setText(timeLeft);

                            if(timeLeft!="לא הוגש")
                                w3=false;

                            mouth4.put("updateDate",updateDate);
                            mouth4.put("unlockDate",unlockDate);
                            mouth4.put("status","לא הוגש");
                            mouth4.put("sequenceNumber",4);

                            unlockDate=new Date(updateDate.getYear(),updateDate.getMonth(),updateDate.getDate()+28,updateDate.getHours(),updateDate.getMinutes(),updateDate.getSeconds());
                            timeLeft =ReturnTimeLeft(unlockDate,updateDate);
                            TextView tg=(TextView)findViewById(R.id.timeLeft5);
                            tg.setText(timeLeft);

                            if(timeLeft!="לא הוגש")
                                w4=false;

                            mouth5.put("updateDate",updateDate);
                            mouth5.put("unlockDate",unlockDate);
                            mouth5.put("status","לא הוגש");
                            mouth5.put("sequenceNumber",5);

                            unlockDate=new Date(updateDate.getYear(),updateDate.getMonth(),updateDate.getDate()+35,updateDate.getHours(),updateDate.getMinutes(),updateDate.getSeconds());
                            timeLeft =ReturnTimeLeft(unlockDate,updateDate);
                            TextView tr=(TextView)findViewById(R.id.timeLeft6);
                            tr.setText(timeLeft);

                            if(timeLeft!="לא הוגש")
                                w5=false;

                            mouth6.put("updateDate",updateDate);
                            mouth6.put("unlockDate",unlockDate);
                            mouth6.put("status","לא הוגש");
                            mouth6.put("sequenceNumber",6);

                            unlockDate=new Date(updateDate.getYear(),updateDate.getMonth(),updateDate.getDate()+42,updateDate.getHours(),updateDate.getMinutes(),updateDate.getSeconds());
                            timeLeft =ReturnTimeLeft(unlockDate,updateDate);
                            TextView tm=(TextView)findViewById(R.id.timeLeft7);
                            tm.setText(timeLeft);

                            if(timeLeft!="לא הוגש")
                                w6=false;

                            mouth7.put("updateDate",updateDate);
                            mouth7.put("unlockDate",unlockDate);
                            mouth7.put("status","לא הוגש");
                            mouth7.put("sequenceNumber",7);

                            unlockDate=new Date(updateDate.getYear(),updateDate.getMonth(),updateDate.getDate()+49,updateDate.getHours(),updateDate.getMinutes(),updateDate.getSeconds());
                            timeLeft =ReturnTimeLeft(unlockDate,updateDate);
                            TextView tq=(TextView)findViewById(R.id.timeLeft8);
                            tq.setText(timeLeft);

                            if(timeLeft!="לא הוגש")
                                w7=false;

                            mouth8.put("updateDate",updateDate);
                            mouth8.put("unlockDate",unlockDate);
                            mouth8.put("status","לא הוגש");
                            mouth8.put("sequenceNumber",8);

                            mouthReviewSubmissions1.put("mouth0",mouth1);
                            mouthReviewSubmissions1.put("mouth1",mouth2);
                            mouthReviewSubmissions1.put("mouth2",mouth3);
                            mouthReviewSubmissions1.put("mouth3",mouth4);
                            mouthReviewSubmissions1.put("mouth4",mouth5);
                            mouthReviewSubmissions1.put("mouth5",mouth6);
                            mouthReviewSubmissions1.put("mouth6",mouth7);
                            mouthReviewSubmissions1.put("mouth7",mouth8);

                            mouthReviewSubmissions.put("mouthReviewSubmissions",mouthReviewSubmissions1);

                            db.collection("users-data").document(gUid).update(mouthReviewSubmissions)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.e(" successfully written!", "o");
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.e("Error writing document", "o");
                                        }
                                    });
                        }
                    } else {
                        Log.d("LOGGER", "No such document");
                    }
                } else {
                    Log.d("LOGGER", "get failed with ", task.getException());
                }
            }
        });
    }

    public String ReturnTimeLeft(Date finalDate,Date firstDate){//return the difference between 2 given dates(from the date of now and the date that the user can upload an image)

        long different = finalDate.getTime() - firstDate.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;
        if(elapsedDays>=7)
        {
            long elapsedWeeks=elapsedDays/7;
            long elaspedDays2=elapsedDays-(elapsedWeeks*7);
            return ("נותר,"+ elapsedWeeks +" שבועות, " +elaspedDays2+ " ימים, "+ elapsedHours+" שעות, "+elapsedMinutes+" דקות" );
        }
        if(elapsedDays<=0&&elapsedHours<=0&&elapsedMinutes<=0)
            return("לא הוגש");
        else
        if (elapsedDays == 0 && elapsedHours == 0) {
            return ("נותר," + elapsedMinutes + " דקות");

        }
        else
        if(elapsedDays==0){
            return("נותר,"+ elapsedHours+" שעות, "+elapsedMinutes+" דקות" );
        }
        return("נותר,"+ elapsedDays + " ימים "+ elapsedHours+" שעות, "+elapsedMinutes+" דקות" );
    }






    // ============================================================================================
    //  Take Picture.
    // ============================================================================================
    public void takePicture(View view) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        switch (view.getId()) {//checks wich week the user clicked
            case R.id.imageButtonWeek0:
                weekNum = "0";
                if (w0 == false) {
                    wl = false;
                    Toast.makeText(Treatment_progress.this, "לא ניתן להגיש, נא לחכות למועד ההגשה", Toast.LENGTH_SHORT).show();
                } else
                    wl = true;
                if (p0 == false) {
                    Toast.makeText(Treatment_progress.this, "כבר הוגש, לא ניתן להגיש שוב.", Toast.LENGTH_SHORT).show();
                    pl = false;
                } else
                    pl = true;
                break;
            case R.id.imageButtonWeek1:
                weekNum = "1";
                if (w1 == false) {
                    wl = false;
                    Toast.makeText(Treatment_progress.this, "לא ניתן להגיש, נא לחכות למועד ההגשה", Toast.LENGTH_SHORT).show();

                } else
                    wl = true;
                if (p1 == false)
                {
        pl = false;
        Toast.makeText(Treatment_progress.this, "כבר הוגש, לא ניתן להגיש שוב.", Toast.LENGTH_SHORT).show();
    }
                else
                    pl=true;

                break;
            case R.id.imageButtonWeek2:
                weekNum = "2";
                if(w2==false) {
                    wl=false;
                    Toast.makeText(Treatment_progress.this, "לא ניתן להגיש, נא לחכות למועד ההגשה", Toast.LENGTH_SHORT).show();
                }
                else
                    wl=true;
                if(p2==false) {
                    pl = false;
                    Toast.makeText(Treatment_progress.this, "כבר הוגש, לא ניתן להגיש שוב.", Toast.LENGTH_SHORT).show();
                }
                else
                    pl=true;
                break;
            case R.id.imageButtonWeek3:
                weekNum = "3";
                if(w3==false) {
                    wl=false;
                    Toast.makeText(Treatment_progress.this, "לא ניתן להגיש, נא לחכות למועד ההגשה", Toast.LENGTH_SHORT).show();
                }
                else
                    wl=true;
                if(p3==false) {
                    pl = false;
                    Toast.makeText(Treatment_progress.this, "כבר הוגש, לא ניתן להגיש שוב.", Toast.LENGTH_SHORT).show();
                }
                else
                    pl=true;
                break;
            case R.id.imageButtonWeek4:
                weekNum = "4";
                if(w4==false) {
                    wl=false;
                    Toast.makeText(Treatment_progress.this, "לא ניתן להגיש, נא לחכות למועד ההגשה", Toast.LENGTH_SHORT).show();
                }
                else
                    wl=true;
                if(p4==false) {
                    Toast.makeText(Treatment_progress.this, "כבר הוגש, לא ניתן להגיש שוב.", Toast.LENGTH_SHORT).show();
                    pl = false;
                }
                else
                    pl=true;
                break;

            case R.id.imageButtonWeek5:
                weekNum = "5";
                if(w5==false) {
                    wl=false;
                    Toast.makeText(Treatment_progress.this, "לא ניתן להגיש, נא לחכות למועד ההגשה", Toast.LENGTH_SHORT).show();
                }
                else
                    wl=true;
                if(p5==false) {
                    Toast.makeText(Treatment_progress.this, "כבר הוגש, לא ניתן להגיש שוב.", Toast.LENGTH_SHORT).show();
                    pl = false;
                }
                else
                    pl=true;
                break;

            case R.id.imageButtonWeek6:
                weekNum = "6";
                if(w6==false) {
                    wl=false;
                    Toast.makeText(Treatment_progress.this, "לא ניתן להגיש, נא לחכות למועד ההגשה", Toast.LENGTH_SHORT).show();
                }
                else
                    wl=true;
                if(p6==false) {
                    pl = false;
                    Toast.makeText(Treatment_progress.this, "כבר הוגש, לא ניתן להגיש שוב.", Toast.LENGTH_SHORT).show();
                }
                else
                    pl=true;
                break;

            case R.id.imageButtonWeek7:
                weekNum = "7";
                if(w7==false) {
                    wl=false;
                    Toast.makeText(Treatment_progress.this, "לא ניתן להגיש, נא לחכות למועד ההגשה", Toast.LENGTH_SHORT).show();
                }
                else
                    wl=true;
                if(p7==false) {
                    pl = false;
                    Toast.makeText(Treatment_progress.this, "כבר הוגש, לא ניתן להגיש שוב.", Toast.LENGTH_SHORT).show();
                } else
                    pl=true;
                break;
        }

        if(wl==true&&pl==true) {//if button was clicked goes to take a picture activity

            setResult(RESULT_OK, intent);
            Intent in = new Intent(Treatment_progress.this, TakeApicture.class);
            in.putExtra("submission", weekNum);
            in.putExtra("Uid", gUid);
            startActivity(in);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {//menu bar
        // Inflate the main_menu; this adds items to the action bar if it is present.
        DocumentReference docRef = db.collection("users-data").document(gUid);

        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document .exists()) {
                        //     Log.i("LOGGER","First "+document.getString("first_entry"));
                        Map<String, Object> map = document.getData();
                        role = (String) document.get("role");

                        //if the field is Boolean
                        if(document.get("role")!=null&&role.equals("user"))
                        {
                            getMenuInflater().inflate(R.menu.menu, menu);

                        }
                        else if(document.get("role")!=null&&role.equals("admin"))
                        {
                            getMenuInflater().inflate(R.menu.admin, menu);

                        }
                    } else {
                        Log.d("LOGGER", "No such document");
                    }
                } else {
                    Log.d("LOGGER", "get failed with ", task.getException());
                }
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.item1:
                Intent intent = new Intent(Treatment_progress.this, Treatment_progress.class);
                intent.putExtra("Uid", gUid);
                startActivity(intent);
                break;
            case R.id.item2:
                Intent intent1 = new Intent(Treatment_progress.this, Tutorials.class);
                intent1.putExtra("Uid", gUid);
                startActivity(intent1);
                break;
            case R.id.item4:
                Intent intent2 = new Intent(Treatment_progress.this, Users.class);
                intent2.putExtra("Uid", gUid);
                startActivity(intent2);
                break;
            case R.id.item5:
                Intent intent3 = new Intent(Treatment_progress.this, Checkpic.class);
                intent3.putExtra("Uid", gUid);
                startActivity(intent3);
                break;
            case R.id.item3:
                FirebaseAuth.getInstance().signOut();
                Intent intent4 = new Intent(Treatment_progress.this, login.class);
                startActivity(intent4);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }




    // ============================================================================================
    //  upload image to Firebase .
    // ============================================================================================
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] dataBAOS = baos.toByteArray();

            mProgress.setMessage("Uploading Image...");
            mProgress.show();

//            StorageReference imagesRef = mStorageRef.child("images").child("IMG_" + userName + "_" +  new Date().getTime());
            StorageReference imagesRef = mStorageRef.child("images").child("IMG__userID_" + gUid + "_week_"+ weekNum +"_" + new Date().getTime());

            imagesRef.putBytes(dataBAOS).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(Treatment_progress.this, "Successfully uploading the image file!", Toast.LENGTH_LONG).show();
                    mProgress.dismiss();

                }

            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    mProgress.dismiss();
                    Toast.makeText(Treatment_progress.this, "Error uploading file... ", Toast.LENGTH_LONG).show();

                }
            });
        }
    }

}
