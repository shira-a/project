package com.igam.shira.myapplication;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter <ImageAdapter.ImageViewHolder>{
    private Context mcontext;
    private List<Upload> mUpload;
    private OnItemClickListener mListener;
    private ProgressDialog mProgress;
    public ImageAdapter(Context context,List<Upload>uploads)
    {
        mcontext=context;
        mUpload=uploads;
        mProgress = new ProgressDialog(context);
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(mcontext).inflate(R.layout.item_card,parent,false);
        return new ImageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
       mProgress.setMessage("Loading...");
      mProgress.show();
        Upload currentUpload=mUpload.get(position);

        Glide.with(mcontext)
                .load(currentUpload.getmImageUrl())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                       mProgress.dismiss();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                       mProgress.dismiss();
                        return false;
                    }
                })


                .into(holder.imageView);



        /*Picasso.with(mcontext)
                .load(currentUpload.getmImageUrl())
                .centerCrop()
                .fit()
                .into(holder.imageView, new Callback(){
                    @Override
                    public void onSuccess() {
                        //mProgress.dismiss();

                    }

                    @Override
                    public void onError() {
                    }
                } );*/
    }

    @Override
    public int getItemCount() {
        return mUpload.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        ImageView imageView;
        public ImageViewHolder(View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.image_view);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(mListener!=null)
            {
                int position=getAdapterPosition();
                if(position!=RecyclerView.NO_POSITION)
                {
                    mListener.onItemClicked(position);


                }
            }
        }
    }


    public interface OnItemClickListener
    {
        void onItemClicked(int position);

    }


    public void setOnItemClickListener(OnItemClickListener listener)
    {
        mListener=listener;
    }


    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }
}
