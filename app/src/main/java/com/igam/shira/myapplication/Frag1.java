package com.igam.shira.myapplication;

import android.app.ActionBar;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;

public class Frag1 extends Fragment {
    String code,phone,role,password;
    private FirebaseFirestore db =FirebaseFirestore.getInstance();
    private String gUid=null,user="fff";
    private String user_guidf=null;
    EditText user_code;
    EditText user_role;
    EditText user_phone;
    EditText user_password;
    boolean new_user=true;
    Button edit;
    View view;
    private FirebaseAuth mAuth;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ActionBar actionBar = getActivity().getActionBar();
//        actionBar.setTitle("טופס הרשמה");
        //user=getArguments().getString("new_user");

       // Log.d("new user?2",user);

             view = inflater.inflate(R.layout.activity_user_detailes, container, false);

            user_code=(EditText)view.findViewById(R.id.user_code);
            user_phone=(EditText)view.findViewById(R.id.user_phone);
             user_password=(EditText)view.findViewById(R.id.user_password);
             user_phone=(EditText)view.findViewById(R.id.user_phone);
            edit=(Button)view.findViewById(R.id.edit_button);
            user_role=(EditText)view.findViewById(R.id.user_role);
            gUid= getArguments().getString("Uid");
            user_guidf=getArguments().getString("user_gUid");
//         Log.d("guid",gUid);



        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("clicked", "button clicked");


                Frag4 fragment = new Frag4();
                // ((User_Detailes) getActivity()).replaceFragments(fragment);

              if (fragment != null) {

                    Bundle arguments = new Bundle();
                    arguments.putString("Uid", gUid);
                  arguments.putString("user_gUid", user_guidf);
                    fragment.setArguments(arguments);


                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.addToBackStack(null);
                    Log.d("container", String.valueOf(((ViewGroup) getView().getParent()).getId()));
                    //transaction.hide(NewUser.this);
                    transaction.replace(R.id.root_frame1, fragment).commit();
                    getChildFragmentManager().executePendingTransactions();


                }
            }
        });


        if(user_guidf!=null) {


            DocumentReference docRef = db.collection("users-data").document(user_guidf);


            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            //     Log.i("LOGGER","First "+document.getString("first_entry"));
                            Map<String, Object> map = document.getData();
                            role = (String) document.get("role");
                            user_role.setText(role);
                            String str=user_guidf;
                            str=str.substring(0, str.indexOf("@"));
                            user_code.setText(str);
                            phone=(String)document.get("phone");
                            user_phone.setText(phone);

                            //if the field is Boolean


                            // Log.i("LOGGER","second "+document.getString("role"));

                        } else {
                            Log.d("LOGGER", "No such document");
                        }
                    } else {
                        Log.d("LOGGER", "get failed with ", task.getException());
                    }
                }
            });


            user_role.setTextColor(Color.BLACK);
            user_code.setEnabled(false);
     user_password.setEnabled(false);
            user_role.setEnabled(false);
            user_phone.setEnabled(false);
            //user_phone.setEnabled(false);
        }

        return view;
    }


}
